-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 19 Φεβ 2017 στις 20:41:38
-- Έκδοση διακομιστή: 10.1.9-MariaDB
-- Έκδοση PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `tiv`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `photos`
--

CREATE TABLE `photos` (
  `photoID` int(11) NOT NULL,
  `userName` text NOT NULL,
  `date` text NOT NULL,
  `title` text NOT NULL,
  `numberOfRatings` int(11) NOT NULL,
  `contentType` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `user`
--

CREATE TABLE `user` (
  `userName` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `birthDate` text NOT NULL,
  `gender` enum('Male','Female','Unknown') NOT NULL,
  `country` text NOT NULL,
  `town` text NOT NULL,
  `additional` text NOT NULL,
  `REGISTEREDSINCE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `user`
--

INSERT INTO `user` (`userName`, `email`, `password`, `firstName`, `lastName`, `birthDate`, `gender`, `country`, `town`, `additional`, `REGISTEREDSINCE`) VALUES
('com', '', 'com', '', '', '', '', '', '', '', '0000-00-00'),
('Voldemort', 'nickamorg0@gmail.com', 'NN1N112098aN8aN', 'asg', 'asdhf', '11/1/1999', 'Male', 'Greece', 'sdgfds', '', '2017-02-19');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
