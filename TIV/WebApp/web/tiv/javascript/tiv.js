function sendOnGetImageCollectionAjaxGET(num, opt) {
    var xhr = new XMLHttpRequest();
	var response;
	
	if(opt === "signed") {
		var value = document.cookie;
		var parts = value.split("; ");
		var userName = parts[0].substring(9, parts[0].length);
		xhr.open('GET', 'GetImageCollection?userName=' + userName);
	} else {
		xhr.open('GET', 'GetImageCollection?number=' + num); 
	}
	
    xhr.onload = function () {
		if (xhr.status === 200) {
			response = xhr.responseText;
			if(response === "nophotos") {
				if(opt === "signed") {
					document.getElementById("main_content").innerHTML = "<h2 id=\"response\" class=\"info\">"
					+ "You haven't uploaded any image</h2>";
				} else {
					document.getElementById("nophotomessage").innerHTML = "<h2 id=\"response\" class=\"info\">"
					+ "There isn't any uploaded image</h2>";
				}
			}else {
				if(document.getElementById("nophotomessage") !== null) {
					document.getElementById("nophotomessage").innerHTML = "";
				}
				var json = JSON.parse(xhr.responseText);
				for (var i = 0; i < json.ids.length; i++) {
					sendGetImageAjaxGET(json.ids[i].id, "false", opt);
				}
			}
		} else {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
    };
    xhr.send();
   
}

function sendOnUploadImageAjaxGET(image) {
    var formData = new FormData();
	var value = document.cookie;
    var parts = value.split("; ");
    var userName = parts[0].substring(9, parts[0].length);
	var xhr = new XMLHttpRequest();
	
	 xhr.onload = function () {
		if (xhr.status === 200) {
			sendOnGetImageCollectionAjaxGET("20", "signed");
		} else {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
    };
	
	formData.append('photo', image);
	xhr.open("POST", 'UploadImage?userName=' + userName + '&title=' + image.name + '&contentType=' + image.type);
	xhr.send(formData);
	
}

function sendGetImageAjaxGET(imageID, metadata, opt) {
    var xhr = new XMLHttpRequest();
	
    xhr.open('GET', 'GetImage?image=' + imageID + '&metadata=' + metadata);
    xhr.onload = function () {
        if (xhr.status === 200) {
			if(metadata === "true") {
				document.getElementById("grid").innerHTML += xhr.responseText;
			} else {
				loadImage(imageID, xhr.responseText, document.getElementById("grid"), opt);
            }
        }
    };
    xhr.send();
   
}

function loadImage(id, str, elem, opt) {
	// Render thumbnail.
	tile = document.createElement("div");
	tile.className = "tile";

	img = document.createElement("img");
	img.id = "img" + id;
	img.src = str;
	img.alt = "image" + id;
	img.title = "image" + id;
	img.addEventListener("click", function (e) {
		showImage(id, str, document.body, opt);
	});
	tile.appendChild(img);
	elem.insertBefore(tile, null);
	
}

function uploadImage() {
	loadedImage = document.getElementById("input_images").files;
	
	if(loadedImage.length !== 0 && loadedImage[0].name.match(/\.(jpg|jpeg|png|gif|JPG)$/)) {
		sendOnUploadImageAjaxGET(loadedImage[0]);
	}
	
}	
		
function showImage(id, str, elem, opt) {
	var modal = document.createElement("div");
	
	modal.id = "modal" + id;
	modal.className = "modal";

	var x = document.createElement("span");
	x.className = "close";
	x.textContent = "×";
	x.onclick = function () {
		document.location.hash = "";
		modal.style.display = "none";
	};
	modal.appendChild(x);
	
	if(opt === "signed") {
		var dltPhoto = document.createElement("button");
		dltPhoto.className = "edit dltButton";
		dltPhoto.textContent = "Delete Photo";
		dltPhoto.onclick = function () {
			deleteImage(id);
			document.location.hash = "";
			modal.style.display = "none";
		};
		modal.appendChild(dltPhoto);
	}
	
	var nimg = document.createElement("img");
	nimg.className = "modal-content";
	nimg.id = id;
	nimg.src = str;
	nimg.alt = "image" + id;
	nimg.title = "image" + id;
	modal.appendChild(nimg);

	var map = document.createElement("div");
	map.className = "map";
	modal.appendChild(map);
	showImageDetailedExifWithMap(str, map);

	var caption = document.createElement("div");
	caption.className = "caption";
	commentSectionAjaxGET(id, caption);
	
	modal.style.display = "block";
	elem.appendChild(modal);

	document.location.hash = "inage" +id;

}

function showImageDetailedExifInfo(str, elem) {

	var tmpimg = document.createElement("img");
	tmpimg.src = str;

	window.EXIF.getData(tmpimg, function () {
		var allMetaData = window.EXIF.getAllTags(this);
		if (!JSON.stringify(allMetaData, null, "\t").match("{}")) {
			elem.innerHTML = JSON.stringify(allMetaData, null, "\t");
		}
	});

};

function showImageDetailedExifWithMap(str, elem) {

	var tmpimg = document.createElement("img");
	tmpimg.src = str;

	window.EXIF.getData(tmpimg, function () {
		var lon = window.EXIF.getTag(this, "GPSLongitude");
		var lat = window.EXIF.getTag(this, "GPSLatitude");

		if (lon === null || lon === undefined || lat === null || lat === undefined) {
			return;
		}

		var lonRef = window.EXIF.getTag(this, "GPSLongitudeRef") || "N";
		var latRef = window.EXIF.getTag(this, "GPSLatitudeRef") || "W";

		lat = (lat[0] + lat[1] / 60 + lat[2] / 3600) * (latRef === "N" ? 1 : -1);
		lon = (lon[0] + lon[1] / 60 + lon[2] / 3600) * (lonRef === "W" ? -1 : 1);


		var uluru = {
			lat: lat,
			lng: lon
		};
		var map = new window.google.maps.Map(elem, {
			zoom: 4,
			center: uluru
		});
		var marker = new window.google.maps.Marker({
			position: uluru,
			map: map
		});

	});

}

function loadPhotos() {

	var users = document.getElementById("checkusers");
	var info = document.getElementById("myinfo");
	var photos = document.getElementById("showphotos");

	photos.style.display = "none";
	users.style.display = "block";
	info.style.display = "block";

	document.getElementById("main_content").innerHTML = 
			"<div class=\"input_field\">"
			+		"<label id='imagesshownlabel'>"
			+			"Select numebr of images shown"
			+			"<input id=\"images_shown\" type=\"number\" name=\"images_shown\" onchange=\"showLimitedPhotos();\">"
			+		"</label>"
			+	"</div>"
			+ "<div id=\"grid\"></div>";
	shownPhotosNumber();

	uploadImage();

}

function showMyPhotos() {
	var users = document.getElementById("checkusers");
	var info = document.getElementById("myinfo");
	var photos = document.getElementById("showphotos");
	users.style.display = "block";
	info.style.display = "block";
	photos.style.display = "none";
	document.getElementById("main_content").innerHTML = 
			"<div class=\"input_field\">"
			+		"<label id='imagesshownlabel'>"
			+			"Select numebr of images shown"
			+			"<input id=\"images_shown\" type=\"number\" name=\"images_shown\" onchange=\"showLimitedPhotos();\">"
			+		"</label>"
			+	"</div>"
			+ "<div id=\"grid\"></div>";
	shownPhotosNumber();
	sendOnGetImageCollectionAjaxGET("20", "signed");
}

function showLatestPhotos() {
	sendOnGetImageCollectionAjaxGET("20", "unsigned");
}