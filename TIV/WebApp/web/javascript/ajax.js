/* jshint browser: true */
"use strict";

function daysInMonth(m, y) { // m is 0 indexed: 0-11
	switch (m) {
		case 1 :
			return (y % 4 === 0 && y % 100) || y % 400 === 0 ? 29 : 28;
		case 8 :
		case 3 :
		case 5 :
		case 10 :
			return 30;
		default :
			return 31;
	}
}

function isValid(d, m, y) {
	var year = new Date().getFullYear();
	var days = daysInMonth(m, y);
	return m >= 0 && m < 12 && d > 0 && d <= days && y > 1900 && y <= year;
}

function checkUsername() {

	var username = document.getElementsByName("username")[0];
	var username_error = document.getElementById("username_error");
	var empty_username = document.getElementById("empty_username");

	if (username.value.length === 0) {
		username_error.style.display = "none";
		empty_username.style.display = "block";
	} else {
		empty_username.style.display = "none";
		if (username.value.length < 8) {
			username_error.style.display = "block";
		} else {
			username_error.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function checkEmail() {

	var email = document.getElementsByName("email")[0];
	var email_error = document.getElementById("email_error");
	var empty_email = document.getElementById("empty_email");

	if (email.value.length === 0) {
		email_error.style.display = "none";
		empty_email.style.display = "block";
	} else {
		empty_email.style.display = "none";
		if (email.value.replace(/[^@]/g, "").length !== 1 || email.value.replace(/[^.]/g, "").length < 1) {
			email_error.style.display = "block";
		} else {
			email_error.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function checkPassword() {

	var password1 = document.getElementsByName("password")[0];
	var password2 = document.getElementsByName("password")[1];
	var password_error_short = document.getElementById("password_error_short");
	var password_error_simple = document.getElementById("password_error_simple");
	var empty_password = document.getElementById("empty_password");
	var password_error_match = document.getElementById("password_error_match");
	var password_confirm = document.getElementById("password_confirm");

	if (password1.value.length === 0) {
		password_confirm.style.display = "none";
		password_error_match.style.display = "none";
		password_error_short.style.display = "none";
		password_error_simple.style.display = "none";
		empty_password.style.display = "block";
	} else {
		empty_password.style.display = "none";
		password_error_match.style.display = "none";
		password_confirm.style.display = "none";
		if (password1.value.length < 6) {
			password_error_simple.style.display = "none";
			password_error_short.style.display = "block";
		} else if (!password1.value.match(/[a-z]/i) ||
				!password1.value.match(/[0-9]/) ||
				password1.value.replace(/[a-z0-9]/gi, "").length < 1) {
			password_error_short.style.display = "none";
			password_error_simple.style.display = "block";
		} else {
			password_error_short.style.display = "none";
			password_error_simple.style.display = "none";
			if (password2.value.length === 0) {
				password_error_match.style.display = "none";
				password_confirm.style.display = "block";
			} else {
				if (password2.value !== password1.value) {
					password_error_match.style.display = "block";
				} else {
					password_error_match.style.display = "none";
					return 1;
				}
			}
		}
	}

	return 0;

}

function checkFirstName() {

	var first_name = document.getElementsByName("first_name")[0];
	var first_name_error_short = document.getElementById("first_name_error_short");
	var first_name_error_invalid = document.getElementById("first_name_error_invalid");
	var empty_first_name = document.getElementById("empty_first_name");

	if (first_name.value.length === 0) {
		first_name_error_short.style.display = "none";
		first_name_error_invalid.style.display = "none";
		empty_first_name.style.display = "block";
	} else {
		empty_first_name.style.display = "none";
		if (first_name.value.length < 3) {
			first_name_error_invalid.style.display = "none";
			first_name_error_short.style.display = "block";
		} else if (first_name.value
				.replace(/[ςερτυθιοπασδφγηξκλζχψωβνμΕΡΤΥΘΙΟΠΑΣΔΦΓΗΞΚΛΖΧΨΩΒΝΜΆΈΊΎΌάέύίόa-zA-Z]/g, "").length !== 0) {
			first_name_error_short.style.display = "none";
			first_name_error_invalid.style.display = "block";
		} else {
			first_name_error_short.style.display = "none";
			first_name_error_invalid.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function checkLastName() {

	var last_name = document.getElementsByName("last_name")[0];
	var last_name_error_short = document.getElementById("last_name_error_short");
	var last_name_error_invalid = document.getElementById("last_name_error_invalid");
	var empty_last_name = document.getElementById("empty_last_name");

	if (last_name.value.length === 0) {
		last_name_error_short.style.display = "none";
		last_name_error_invalid.style.display = "none";
		empty_last_name.style.display = "block";
	} else {
		empty_last_name.style.display = "none";
		if (last_name.value.length < 4) {
			last_name_error_invalid.style.display = "none";
			last_name_error_short.style.display = "block";
		} else if (last_name.value
				.replace(/[ςερτυθιοπασδφγηξκλζχψωβνμΕΡΤΥΘΙΟΠΑΣΔΦΓΗΞΚΛΖΧΨΩΒΝΜΆΈΊΎΌάέύίόa-zA-Z]/g, "").length !== 0) {
			last_name_error_short.style.display = "none";
			last_name_error_invalid.style.display = "block";
		} else {
			last_name_error_short.style.display = "none";
			last_name_error_invalid.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function checkBirthday(scrunity) {

	var day = document.getElementsByName("day")[0];
	var month = document.getElementsByName("month")[0];
	var year = document.getElementsByName("year")[0];
	var birthday_error = document.getElementById("birthday_error");
	var empty_birthday = document.getElementById("empty_birthday");

	if (month.value.length === 0 && day.value.length === 0 && year.value.length === 0) {
		birthday_error.style.display = "none";
		empty_birthday.style.display = "block";
	} else {
		empty_birthday.style.display = "none";
		if (scrunity === true || (month.value.length !== 0 && day.value.length !== 0 && year.value.length !== 0)) {
			if (!isValid(day.value, month.value - 1, year.value)) {
				birthday_error.style.display = "block";
			} else {
				birthday_error.style.display = "none";
				return 1;
			}
		}
	}

	return 0;

}

function checkCity() {

	var city = document.getElementsByName("city")[0];
	var empty_city = document.getElementById("empty_city");
	var city_error = document.getElementById("city_error");

	if (city.value.length === 0) {
		city_error.style.display = "none";
		empty_city.style.display = "block";
	} else {
		empty_city.style.display = "none";
		if (city.value.length < 2) {
			city_error.style.display = "block";
		} else {
			city_error.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function sendSignUpPOST() {

	var response;
	var username = document.getElementsByName("username")[0].value;
	var email = document.getElementsByName("email")[0].value;
	var first_name = document.getElementsByName("first_name")[0].value;
	var last_name = document.getElementsByName("last_name")[0].value;
	var password = document.getElementsByName("password")[0].value;
	var birthday = document.getElementsByName("day")[0].value +
			"/" + document.getElementsByName("month")[0].value +
			"/" + document.getElementsByName("year")[0].value;
	var gender = document.querySelector('input[name="gender"]:checked').value;
	var country = document.getElementsByName("country")[0].value;
	var city = document.getElementsByName("city")[0].value;
	var comments = document.getElementsByName("comments")[0].value;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "WebAppServlet", true);
	xhr.onload = function () {
		if (xhr.status === 200) {
			response = xhr.responseText;
			switch (response) {
				case "1":
					document.getElementById("response3").style.display = "none";
					document.getElementById("response2").style.display = "none";
					document.getElementById("response1").style.display = "block";
					break;
				case "2":
					document.getElementById("response3").style.display = "none";
					document.getElementById("response1").style.display = "none";
					document.getElementById("response2").style.display = "block";
					break;
				case "3":
					document.getElementById("response2").style.display = "none";
					document.getElementById("response1").style.display = "none";
					document.getElementById("response3").style.display = "block";
					break;
				default:
					document.body.innerHTML = response;
					break;
			}
		} else if (xhr.status !== 200) {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("action=signup&username=" + username + "&email=" + email + "&first_name=" + first_name +
			"&last_name=" + last_name + "&password=" + encryption(password) +
			"&birthday=" + birthday.toString() + "&gender=" + gender + "&country=" + country +
			"&city=" + city + "&comments=" + comments);

}

function encryption(pass) {
	var key = 10 + pass.length;
	var newPass = pass;
	for(var i = 0; i < pass.length; i++) {
		newPass = newPass.substr(0, i) + (parseInt(pass.charAt(i)) + key) + newPass.substr(i, pass.length);
	}
	
	return newPass;
}

function signUp() {

	if ((checkUsername() + checkEmail() + checkPassword() + checkFirstName() +
			checkLastName() + checkBirthday(true) + checkCity()) === 7) {
		sendSignUpPOST();
	}

}

function isUsernameFilled(scrunity) {

	var username = document.getElementsByName("username")[0];
	var empty_username = document.getElementById("empty_username");


	if (scrunity === true && username.value.length === 0) {
		empty_username.style.display = "block";
	} else {
		empty_username.style.display = "none";
		return 1;
	}

	return 0;

}

function isPasswordFilled(scrunity) {

	var password = document.getElementsByName("password")[0];
	var empty_password = document.getElementById("empty_password");

	if (scrunity === true && password.value.length === 0) {
		empty_password.style.display = "block";
	} else {
		empty_password.style.display = "none";
		return 1;
	}

	return 0;

}

function sendSignInPOST() {

	var response;
	var username = document.getElementsByName("username")[0].value;
	var password = document.getElementsByName("password")[0].value;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "WebAppServlet", true);
	xhr.onload = function () {
		if (xhr.status === 200) {
			response = xhr.responseText;
			switch (response) {
				case "1":
					document.getElementById("response2").style.display = "none";
					document.getElementById("response1").style.display = "block";
					break;
				case "2":
					document.getElementById("response1").style.display = "none";
					document.getElementById("response2").style.display = "block";
					break;
				default:
					document.body.innerHTML = response;
					break;
			}
		} else if (xhr.status !== 200) {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("action=signin&username=" + username + "&password=" + encryption(password));

}

function signIn() {

	if ((isUsernameFilled(true) + isPasswordFilled(true)) === 2) {
		sendSignInPOST();
	}

}

function sendPOST(action) {

	var response;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "WebAppServlet", true);
	xhr.onload = function () {
		response = xhr.responseText;
		if (xhr.status === 200) {
			if (response !== null && response !== undefined) {
				document.body.innerHTML = response;
			}
		} else if (xhr.status !== 200) {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("action=" + action);

}

function showUsers() {

	sendPOST("showusers");

}

function showMyInfo() {

	sendPOST("showmyinfo");

}

function signOut() {

	sendPOST("signout");

}

function checkPasswordEdit() {

	var password = document.getElementById("password");
	var password_error_short = document.getElementById("password_error_short");
	var password_error_simple = document.getElementById("password_error_simple");
	var empty_password = document.getElementById("empty_password");

	if (password.value.length === 0) {
		password_error_short.style.display = "none";
		password_error_simple.style.display = "none";
		empty_password.style.display = "block";
	} else {
		empty_password.style.display = "none";
		if (password.value.length < 6) {
			password_error_simple.style.display = "none";
			password_error_short.style.display = "block";
		} else if (!password.value.match(/[a-z]/i) ||
				!password.value.match(/[0-9]/) ||
				password.value.replace(/[a-z0-9]/gi, "").length < 1) {
			password_error_short.style.display = "none";
			password_error_simple.style.display = "block";
		} else {
			password_error_short.style.display = "none";
			password_error_simple.style.display = "none";
			return 1;
		}
	}

	return 0;

}

function checkEmailEdit() {

	document.getElementById("email_response_error").style.display = "none";

	return checkEmail();

}

function saveName() {

	var info, edit, value1, value2, response;
	info = document.getElementById("name_info");
	edit = document.getElementById("edit_name");
	value1 = document.getElementById("first_name").value;
	value2 = document.getElementById("last_name").value;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "WebAppServlet", true);
	xhr.onload = function () {
		response = xhr.responseText;
		if (xhr.status === 200) {
			info.innerText = response;
			edit.innerText = "Edit";
			edit.setAttribute("onclick", "edit(\"name\");");
		} else if (xhr.status !== 200) {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("action=change&value=name&first_name=" + value1 + "&last_name=" + value2);

}

function save(key) {

	var info, edit, value, response;
	info = document.getElementById(key + "_info");
	edit = document.getElementById("edit_" + key);
	value = document.getElementById(key).value;

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "WebAppServlet", true);
	xhr.onload = function () {
		response = xhr.responseText;
		if (xhr.status === 200) {
			if (response !== "error") {
				info.innerText= response;
				edit.innerText = "Edit";
				edit.setAttribute("onclick", "edit(\"" + key + "\");");
			} else {
				document.getElementById(key+"_response_error").style.display = "block";
			}
		} else if (xhr.status !== 200) {
			document.body.innerHTML = "Request failed. Returned status : " + xhr.status;
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("action=change&value=" + key + "&" + key + "=" + value);

}

function sendSaveRequest(value) {

	switch (value) {
		case "name":
			if((checkFirstName() + checkLastName()) === 2) {
				saveName();
			}
			break;
		case "email":
			if(checkEmail() === 1) {
				save("email");
			}
			break;
		case "password":
			if(checkPasswordEdit() === 1) {
				save("password");
			}
			break;
		case "comments":
			save("comments");
			break;
		default:
			break;
	}

}

function edit(value) {
	var info, edit;
	if (value.match("email")) {
		info = document.getElementById("email_info");
		edit = document.getElementById("edit_email");
		info.innerHTML = "<div class=\"input_field\"><label>" +
				"<input id=\"email\" type=\"text\" name=\"email\" onchange=\"checkEmailEdit();\">" +
				"<span id=\"empty_email\" class=\"error hidden\" role=\"alert\">You can't leave this empty.</span>" +
				"<span id=\"email_error\" class=\"error hidden\" role=\"alert\">Email address not valid.</span>" +
				"<span id=\"email_response_error\" class=\"error hidden\" role=\"alert\">The given email address is already in use.</span>" +
				"</label></div>";
		edit.innerText = "Save";
		edit.setAttribute("onclick", "sendSaveRequest(\"email\");");
	} else if (value.match("password")) {
		info = document.getElementById("password_info");
		edit = document.getElementById("edit_password");
		info.innerHTML = "<div class=\"input_field\"><label>" +
				"<input id=\"password\" type=\"password\" maxlength=\"10\" name=\"password\" onchange=\"checkPasswordEdit();\">" +
				"<span id=\"empty_password\" class=\"error hidden\" role=\"alert\">You can't leave this empty.</span>" +
				"<span id=\"password_error_short\" class=\"error hidden\" role=\"alert\">Password is too short.Try one with at least 6 characters.</span>" +
				"<span id=\"password_error_simple\" class=\"error hidden\" role=\"alert\">Password must contain at least one character:[a-z/A-z],at least one number:[0-9] and at least one symbol: ‘#’, ‘$’, ‘%’,etc.</span>" +
				"</label></div>";
		edit.innerText = "Save";
		edit.setAttribute("onclick", "sendSaveRequest(\"password\");");
	} else if (value.match("name")) {
		info = document.getElementById("name_info");
		edit = document.getElementById("edit_name");
		info.innerHTML = "<div class=\"input_field\"><div class=\"multinput\">" +
				"<label class=\"first_label\">" +
				"<input id=\"first_name\" type=\"text\" placeholder=\"First\" maxlength=\"20\" name=\"first_name\" onchange=\"checkFirstName();\">" +
				"<span id=\"empty_first_name\" class=\"error hidden\" role=\"alert\">You can't leave this empty.</span>" +
				"<span id=\"first_name_error_short\" class=\"error hidden\" role=\"alert\">First name is too short.Try one with at least 3 characters.</span>" +
				"<span id=\"first_name_error_invalid\" class=\"error hidden\" role=\"alert\">Acceptable characters are only those of the Greek and English alphabet.</span>" +
				"</label>" +
				"<label class=\"after_label\">" +
				"<input id=\"last_name\" type=\"text\" placeholder=\"Last\" maxlength=\"20\" name=\"last_name\" onchange=\"checkLastName();\">" +
				"<span id=\"empty_last_name\" class=\"error hidden\" role=\"alert\">You can't leave this empty.</span>" +
				"<span id=\"last_name_error_short\" class=\"error hidden\" role=\"alert\">Last name is too short.Try one with at least 4 characters</span>" +
				"<span id=\"last_name_error_invalid\" class=\"error hidden\" role=\"alert\">Acceptable characters are only those of the Greek and English alphabet</span>" +
				"</label>" +
				"</div></div>";
		edit.innerText = "Save";
		edit.setAttribute("onclick", "sendSaveRequest(\"name\");");
	} else if (value.match("comments")) {
		info = document.getElementById("comments_info");
		edit = document.getElementById("edit_comments");
		info.innerHTML = "<div class=\"input_field\"><label>" +
				"<textarea id=\"comments\" name=\"comments\" maxlength=\"500\" rows=7 cols=70></textarea>" +
				"</label></div>";
		edit.innerText = "Save";
		edit.setAttribute("onclick", "sendSaveRequest(\"comments\");");
	}
}

function signUpOpt() {
    document.getElementById("contentbox").style.display = "block";
    document.getElementById("home").style.display = "block";
    document.getElementById("signUp").style.display = "none";
	document.getElementById("grid").style.display = "none";
    document.getElementById("grid").innerHTML = "";
	
	if(document.getElementById("nophotomessage") !== null) {
		document.getElementById("nophotomessage").style.display = "block";
	}
			
	if(document.getElementById("nophotomessage") !== null) {
		document.getElementById("nophotomessage").style.display = "none";
	}
	
    country();
    month();
}

function homeOpt() {
    document.getElementById("contentbox").style.display = "none";
    document.getElementById("signUp").style.display = "block";
    document.getElementById("home").style.display = "none";
	document.getElementById("grid").style.display = "block";
	
	if(document.getElementById("nophotomessage") !== null) {
		document.getElementById("nophotomessage").style.display = "block";
	}
				
}

function country() {
    var country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
    var select = document.getElementById('country');

    for (var i = 0; i < country_list.length; i++){
        var opt = document.createElement('option');
		
        if(country_list[i] === "Greece") {
            opt.selected = "selected";
        }
		
        opt.value = country_list[i];
        opt.innerHTML = country_list[i];
        
        select.append(opt);
    }
}

function month() {
	
    var month_list = ["Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var select = document.getElementById('month');

    for (var i = 0; i < month_list.length; i++){
        var opt = document.createElement('option');
        
        opt.value = i;
        opt.innerHTML = month_list[i];
        
        if(month_list[i] === "Month") {
            opt.selected = "selected";
            opt.value = "";
        }
        
        select.append(opt);
    }
	
}

function deleteUser() {
	document.body.innerHTML = "<h2 id=\"response\" class=\"info centered\">"
		+ "Your acount has been deleted</h2>";
			
	setTimeout(function() { window.location.href = "index.html"; }, 3000);
	
	sendOnDeleteUserAjaxGET();
}

function sendOnDeleteUserAjaxGET() {
	var value = document.cookie;
    var parts = value.split("; ");
    var userName = parts[0].substring(9, parts[0].length);
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'DeleteUser?userName=' + userName);
    xhr.onload = function () {
        if (xhr.status === 200) {
			//document.getElementById("grid").innerHTML += xhr.responseText;
        }
    };
    xhr.send();
}

function deleteImage(imageID) {
	//document.body.innerHTML = "<h2 id=\"response\" class=\"info centered\">"
	//	+ "Your acount has been deleted</h2>";
			
	//setTimeout(function() { window.location.href = "index.html"; }, 3000);
	sendOnDeleteImageAjaxGET(imageID);
	document.getElementById("grid").innerHTML = "";
	sendOnGetImageCollectionAjaxGET("20", "signed");
}

function sendOnDeleteImageAjaxGET(imageID) {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'DeleteImage?id=' + imageID);
    xhr.onload = function () {
        if (xhr.status === 200) {
			//document.getElementById("grid").innerHTML += xhr.responseText;
        }
    };
    xhr.send();
}

function clickUserInfo(userName) {
	sendOnClickUserInfoAjaxGET(userName);
}

function sendOnClickUserInfoAjaxGET(userName) {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'clickUserInfo?userName=' + userName);
    xhr.onload = function () {
        if (xhr.status === 200) {
			//document.getElementById("grid").innerHTML += xhr.responseText;
        }
    };
    xhr.send();
}

function showLimitedPhotos() {
	var num = document.getElementById("images_shown").value;
	document.cookie = "number=" + num;
	document.getElementById("grid").innerHTML = "";
	sendOnGetImageCollectionAjaxGET(num, "signed");
}

function shownPhotosNumber() {
	var value = document.cookie;
	var parts = value.split("; ");
	
	if(parts.length === 2) {
		var num = parts[1].substr(7, parts[1].length);
		document.getElementById("images_shown").value = parseInt(num);
	} else {
		document.getElementById("images_shown").value = 20;
	}
}

function commentSectionAjaxGET(imageID, elem) {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'GetCommentSection?id=' + imageID);
    xhr.onload = function () {
        if (xhr.status === 200) {
			elem.innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}

function deleteComment(commentID) {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'DeleteComment?id=' + commentID);
    xhr.onload = function () {
        if (xhr.status === 200) {
			//document.getElementById("grid").innerHTML += xhr.responseText;
        }
    };
    xhr.send();
	commentSectionAjaxGET(commentID);
}

function modifyComment(commentID) {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'ModifyComment?id=' + commentID);
    xhr.onload = function () {
        if (xhr.status === 200) {
			//document.getElementById("grid").innerHTML += xhr.responseText;
        }
    };
    xhr.send();
}

function showAllPhotos() {
	document.getElementById("main_content").innerHTML = "<div id='grid'></div>";
	sendOnGetImageCollectionAjaxGET("100", "unsigned");
}

/*
 * var value = document.cookie;
			if(value === "") {
				
				var parts = value.split("; ");
				Cookie cookie;
		cookie = new Cookie("username",username);
		response.addCookie(cookie);
				var userName = parts[0].substring(9, parts[0].length);
			}
 */