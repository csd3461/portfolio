/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cs359db.db.PhotosDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author skerdbash
 */
@WebServlet(name = "GetImageCollection", urlPatterns = {"/GetImageCollection"})
public class GetImageCollection extends HttpServlet {

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws java.lang.ClassNotFoundException
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ClassNotFoundException {
		response.setContentType("application/json");
		
		String username = request.getParameter("userName");
		String num_str = request.getParameter("number");
		
		int last, number;
		List<Integer> idlist;
		if(num_str == null) {
			Cookie cookies[] = request.getCookies();
			number = Integer.parseInt(cookies[1].getValue());
		}else {
			try {
				number = Integer.parseInt(num_str);	
			} catch (NumberFormatException e) {
				number = 10;
			}
		}
		
		if(username != null) {
			idlist = PhotosDB.getPhotoIDs(number, username);
		}else {
			idlist = PhotosDB.getPhotoIDs(number);
		}
		
		if(!idlist.isEmpty()) {
			last = idlist.get(idlist.size() - 1);
		
			try (PrintWriter out = response.getWriter()) {
				out.print("{\"ids\":[");
				idlist.forEach((id) -> {
					if(id != last) {
						out.print("{\"id\":\""+id.toString()+"\"},");
					}else {
						out.print("{\"id\":\""+id.toString()+"\"}");
					}
				});
				out.print("]}");
			}
		}else {
			try (PrintWriter out = response.getWriter()) {
				out.print("nophotos");
			}
		}
		
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(GetImageCollection.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(GetImageCollection.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "GetImageCollection";
	}

}
