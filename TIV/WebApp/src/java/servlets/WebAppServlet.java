package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cs359db.db.UserDB;
import cs359db.model.User;
import java.util.List;

/**
 *
 * @author skerdbash
 */
@WebServlet(name = "WebAppServlet", urlPatterns = {"/WebAppServlet"})
public class WebAppServlet extends HttpServlet {
	
	public String getBulletPoints(String str) {
		String bullets = "";
		while(bullets.length() < str.length()) {
			bullets = bullets + "\u2022";
		}
		return bullets;
	}
	
	public static String filtered(String input) {
		StringBuilder filtered = new StringBuilder(input.length());
		char c;
		int i = 0;
		for(i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			switch (c) {
				case '<':
					filtered.append("&lt;");
					break;
				case '>':
					filtered.append("&gt;");
					break;
				case '"':
					filtered.append("&quot;");
					break;
				case '&':
					filtered.append("&amp;");
					break;
				default:
					filtered.append(c);
			}
		}
		return (filtered.toString());
	}
	
	public String getMyInfo(User user, boolean first_time) {
		String myinfo;
		myinfo = "<ul><li id=\"checkusers\" class=\"left\">"
					+ "<a href=\"#\" onclick=\"showUsers();\">See Users</a></li>"
					+ "<li id=\"myinfo\" class=\"left hidden\">"
					+ "<a href=\"#\" onclick=\"showMyInfo();\">My Info</a></li>"
					+ "<li id=\"showphotos\" class=\"left\">"
					+ "<a href=\"#\" onclick=\"showMyPhotos();\";\">My Images</a></li>"
					+ "<li id=\"showallphotos\" class=\"left\">"
					+ "<a href=\"#\" onclick=\"showAllPhotos();\";\">Latest Images</a></li>"
					+ "<li id=\"loadphotos\" class=\"left\">"
					+ "<input id=\"input_images\" type=\"file\" onchange=\"loadPhotos();\" hidden/>"
					+ "<label id=\"input_images_label\" for=\"input_images\" class=\"inputbutton\">Upload Image</label></li>"
					+ "<li id=\"signout\" class=\"right\">"
					+ "<a href=\"signin.html\" onclick=\"signOut();\">Sign out</a></li>"
					+ "</ul>"
					+ "<main id=\"main_content\">";
		if(first_time == true) {
			myinfo = myinfo + "<h2 id=\"response\" class=\"info\">"
					+ "Your account was created successfully</h2>";
		}
		myinfo = myinfo	+ "<div id=\"contentbox\" class=\"centered\"><table>"
					+ "<tr class=\"noborder\"><td>Username</td><td id=\"username_info\">" + user.getUserName() + "</td></tr>"
					+ "<tr><td>Email</td><td id=\"email_info\">" + user.getEmail() + "</td>"
					+ "<td><button class=\"edit\" id=\"edit_email\" onclick=\"edit('email');\">Edit</button></td></tr>"
					+ "<tr><td>Password</td><td id=\"password_info\">" + getBulletPoints(user.getPassword())  + "</td>"
					+ "<td><button class=\"edit\" id=\"edit_password\" onclick=\"edit('password');\">Edit</button></td></tr>"
					+ "<tr><td>Name</td><td id=\"name_info\">" + user.getFirstName() + " " + user.getLastName() + "</td>"
					+ "<td><button class=\"edit\" id=\"edit_name\" onclick=\"edit('name');\">Edit</button></td></tr>"
					+ "<tr><td>Birthday</td><td id=\"birthday_info\">" + user.getBirthDate()  + "</td></tr>"
					+ "<tr><td>Gender</td><td id=\"gender_info\">" + user.getGender()  + "</td></tr>"
					+ "<tr><td>Country</td><td id=\"country_info\">" + user.getCountry()  + "</td></tr>"
					+ "<tr><td>City</td><td id=\"city_info\">" + user.getTown()  + "</td></tr>"
					+ "<tr><td>Comments about you</td><td id=\"comments_info\">" + user.getInfo()  + "</td>"
					+ "<td><button class=\"edit\" id=\"edit_comments\" onclick=\"edit('comments');\">Edit</button></td></tr>"
				
					+ "<tr class=\"noborder\"><td><button class=\"edit\" id=\"delete_account\" onclick=\"deleteUser();\">Delete Account</button></td></tr>"
				
					+ "</table></div>"
					+ "</main>";
		return myinfo;
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws java.lang.ClassNotFoundException
	 */
	protected void processSignUpRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, ClassNotFoundException{
		
		response.setContentType("text/html;charset=UTF-8");
		
		String username = request.getParameter("username");
		String email  = request.getParameter("email");
		String password = request.getParameter("password");
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");
		String birthday = request.getParameter("birthday");
		String gender = request.getParameter("gender");
		String country = request.getParameter("country");
		String city = request.getParameter("city");
		String comments = request.getParameter("comments");
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -15);
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
		Date mindate = cal.getTime();
		Date birthdate = new Date();
		try {
			birthdate = dateformat.parse(birthday);
		} catch (ParseException ex) {
			Logger.getLogger(WebAppServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		if(birthdate.after(mindate)) {
			try (PrintWriter out = response.getWriter()) {
				out.print("1");
			}
			return;
		}else if(!UserDB.checkValidUserName(username)) {
			try (PrintWriter out = response.getWriter()) {
				out.print("2");
			}
			return;
		}else if(!UserDB.checkValidEmail(email)){
			try (PrintWriter out = response.getWriter()) {
				out.print("3");
			}
		}
		
		User user = new User(filtered(username), filtered(email), filtered(password), 
			filtered(first_name), filtered(last_name), filtered(birthday), 
			filtered(country), filtered(city), filtered(comments), filtered(gender));
		
		UserDB.addUser(user);
		
		Cookie cookie; 
		cookie = new Cookie("username",username);
		response.addCookie(cookie);
		cookie = new Cookie("number","20");
		response.addCookie(cookie);
		
		try (PrintWriter out = response.getWriter()) {
			out.print(getMyInfo(user, true));
		}
	
	}
	
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws java.lang.ClassNotFoundException
	 */
	protected void processSignInRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, ClassNotFoundException {
		
		response.setContentType("text/html;charset=UTF-8");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		User user;
		
		if(UserDB.getUser(username) == null) {
			try (PrintWriter out = response.getWriter()) {
				out.print("1");
			}
			return;
		}else {
			user = UserDB.getUser(username);
			if(!UserDB.getUser(username).getPassword().equals(password)){
				try (PrintWriter out = response.getWriter()) {
					out.print("2");
				}
				return;
			}
		}
		
		Cookie cookie;
		cookie = new Cookie("username",username);
		response.addCookie(cookie);
		cookie = new Cookie("number","20");
		response.addCookie(cookie);
		
		try (PrintWriter out = response.getWriter()) {
			out.print(getMyInfo(user, false));
		}
		
	}
	
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws java.lang.ClassNotFoundException
	 */
	protected void processShowUsersRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, ClassNotFoundException {
		
		response.setContentType("text/html;charset=UTF-8");
		
		List<User> users = UserDB.getUsers();
		
		try (PrintWriter out = response.getWriter()) {
			out.print("<ul><li id=\"myinfo\" class=\"left\">"
				+ "<a href=\"#\" onclick=\"showMyInfo();\">My Info</a></li>"
				+ "<li id=\"checkusers\" class=\"left hidden\">"
				+ "<a href=\"#\" onclick=\"showUsers();\">See Users</a></li>"
				+ "<li id=\"showphotos\" class=\"left\">"
				+ "<a href=\"#\" onclick=\"showMyPhotos();\";\">My Images</a></li>"
				+ "<li id=\"showallphotos\" class=\"left\">"
				+ "<a href=\"#\" onclick=\"showAllPhotos();\";\">Latest Images</a></li>"
				+ "<li id=\"loadphotos\" class=\"left\">"
				+ "<input id=\"input_images\" type=\"file\" onchange=\"loadPhotos();\" hidden/>"
				+ "<label id=\"input_images_label\" for=\"input_images\" class=\"inputbutton\">Upload Image</label></li>"
				+ "<li id=\"signout\" class=\"right\"><a href=\"signin.html\" onclick=\"signOut();\">"
				+ "Sign out</a></li></ul>"
				+ "<main id=\"main_content\">"
				+ "<div id=\"contentbox\" class=\"centered\">"
				+ "<table><tr><th>Username</th></tr>");
			users.forEach((user) -> {
				out.print("<tr class=\"noborder\"><td>" + user.getUserName()
					+ "</td>"
					+ "<td><button class=\"edit\" id=\"clickuserinfo\" onclick=\"clickUserInfo(" + user.getUserName() + ");\">Show Info</button></td>"
					+ "<td><button class=\"edit\" id=\"clickusephotos\" onclick=\"clickUserPhotos(" + user.getUserName() + ");\">Show Images</button></td>"
					+ "</tr>");
			});
			out.print("</table></div></main>");		
		}
		
	}
	
	protected void processShowMyInfoRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, ClassNotFoundException {
		
		response.setContentType("text/html;charset=UTF-8");
		Cookie cookies[] = request.getCookies();
		User user;
		
		if(cookies.length != 0 && cookies[0].getValue() != null) {
			user = UserDB.getUser(cookies[0].getValue());
			try (PrintWriter out = response.getWriter()) {
				out.print(getMyInfo(user, false));		
			}
		}else {
			response.setStatus(401);
		}
		
	}
	
	protected void processSignOutRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		Cookie cookies[] = request.getCookies();
		Cookie cookie;
		
		if(cookies.length != 0 && cookies[0].getValue() != null) {
			cookie = cookies[0];
			cookie.setValue(null);
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}else {
			response.setStatus(400);
		}
		
	}
	
	protected void processChangeRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, ClassNotFoundException {
		
		response.setContentType("text/html;charset=UTF-8");
		Cookie cookies[] = request.getCookies();
		User user;
		
		if(cookies.length != 0 && cookies[0].getValue() != null) {
			user = UserDB.getUser(cookies[0].getValue());
			switch (request.getParameter("value")) {
				case "name":
					user.setFirstName(request.getParameter("first_name"));
					user.setLastName(request.getParameter("last_name"));
					try (PrintWriter out = response.getWriter()) {
						out.print(user.getFirstName() + " " + user.getLastName());
					}
					break;
				case "password":
					user.setPassword(request.getParameter("password"));
					try (PrintWriter out = response.getWriter()) {
						out.print(getBulletPoints(user.getPassword()));
					}	
					break;
				case "email":
					if(!UserDB.checkValidEmail(request.getParameter("email"))) {
						try (PrintWriter out = response.getWriter()) {
							out.print("error");
						}
						return;
					}
					user.setEmail(request.getParameter("email"));
					try (PrintWriter out = response.getWriter()) {
						out.print(user.getEmail());
					}
					break;
				case "comments":
					user.setInfo(request.getParameter("comments"));
					try (PrintWriter out = response.getWriter()) {
						out.print(user.getInfo());
					}	
					break;
				default:
					break;
			}
			
			UserDB.updateUser(user);
			
		}else {
			response.setStatus(401);
		}
		
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		try {
			switch (request.getParameter("action")) {
				case "signup":
					processSignUpRequest(request, response);
					break;
				case "signin":
					processSignInRequest(request, response);
					break;
				case "showusers":
					processShowUsersRequest(request, response);
					break;
				case "showmyinfo":
					processShowMyInfoRequest(request, response);
					break;
				case "signout":
					processSignOutRequest(request, response);
					break;
				case "change":
					processChangeRequest(request, response);
					break;
				default:
					break;
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(WebAppServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		try {
			switch (request.getParameter("action")) {
				case "signup":
					processSignUpRequest(request, response);
					break;
				case "signin":
					processSignInRequest(request, response);
					break;
				case "showusers":
					processShowUsersRequest(request, response);
					break;
				case "showmyinfo":
					processShowMyInfoRequest(request, response);
					break;
				case "signout":
					processSignOutRequest(request, response);
					break;
				case "change":
					processChangeRequest(request, response);
					break;
				default:
					break;
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(WebAppServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "My App Servlet";
	}

}
