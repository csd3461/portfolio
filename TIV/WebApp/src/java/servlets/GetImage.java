/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cs359db.db.PhotosDB;
import cs359db.model.Photo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author skerdbash
 */
@WebServlet(name = "GetImage", urlPatterns = {"/GetImage"})
public class GetImage extends HttpServlet {

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws java.lang.ClassNotFoundException
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ClassNotFoundException {
		
		int id = Integer.parseInt(request.getParameter("image"));
		String metadata = request.getParameter("metadata");
		String resized = request.getParameter("resized");
		Photo photo_info = PhotosDB.getPhotoMetadataWithID(id);
		byte[] photo_data;
		
		/*resized != null && resized.equals("true")*/
		
		if(resized != null && resized.equals("true")) {
			RequestDispatcher rd = request.getRequestDispatcher("GetImageResized");
			rd.forward(request,response);
		}else if(metadata.equals("true")) {
			response.setContentType("application/json");
			try (PrintWriter out = response.getWriter()) {
				out.println("{userName:" + photo_info.getUserName()
						+ ", title:" + photo_info.getTitle()
						+ ", date:" + photo_info.getDate()
						+ ", contentType:" + photo_info.getContentType()
						+ ", numberOfRatings:" + photo_info.getNumberOfRatings() + "}"
				);
			}
		}else {
			photo_data = PhotosDB.getPhotoBlobWithID(id);
			response.setContentType("text/html;charset=UTF-8");
			byte[] encoded = Base64.getEncoder().encode(photo_data);
			try (PrintWriter out = response.getWriter()) {
				out.print("data:"+photo_info.getContentType()+";base64,"+new String(encoded));
			}
		}
		
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}

}
