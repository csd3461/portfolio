/*
 * test_microtcp_client.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ifaddrs.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../lib/microtcp.h"

char* read_file(char* name) {
	int packets_multitude;
	char* str;
	char c;
	FILE* f;
	int count;

	f = fopen(name, "r");
	if(f == NULL) return NULL;

	count = 0;
	while((c = fgetc(f) != EOF)) count++;


	str = (char *) malloc(count + 1);
	count = 0;
	rewind(f);
	while((c = fgetc(f)) != EOF) {
		str[count] = c;
		count++;
	}

	str[count] = '\0';

	fclose(f);

	return str;
}

int main(int argc, char **argv){

    char* str;

    struct sockaddr_in sin;
    socklen_t server_address_len = sizeof(struct sockaddr_in);

    /*Creating a microTCP socket using the UDP protocol*/
    microtcp_sock_t socket = microtcp_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(socket.state == INVALID) {
        printf("ERROR : microtcp_socket\n");
        exit(EXIT_FAILURE);
    }

    memset(&sin, 0, server_address_len);
    sin.sin_family = AF_INET;
    /*Port that server listens at */
    sin.sin_port = htons(6886);
    /* The server's IP*/
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");

    socket = microtcp_connect(socket, (struct sockaddr *)&sin, server_address_len);
    if(socket.state == INVALID) {
        printf("ERROR : microtcp_connect\n");
        exit(EXIT_FAILURE);
    }

    microtcp_send(&socket, "Hello World!", 13, 0);

    if(argc > 1) {
		str = read_file(argv[1]);
		microtcp_send(&socket, str, strlen(str) + 1, 0);
	} else {
		printf("There wasn't any input file\n");
	}

    if((socket = microtcp_shutdown(socket, 0)).state == INVALID) {
        printf("ERROR : microtcp_shutdown\n");
        exit(EXIT_FAILURE);
    }

    return 0;

}
