/*
 * test_microtcp_server.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ifaddrs.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../lib/microtcp.h"


int main(int argc, char **argv){

    char buffer[BUF_LEN];
    int received;

    struct sockaddr_in sin;
    struct sockaddr client_addr;
    socklen_t client_address_len = sizeof(struct sockaddr);
    socklen_t server_address_len = sizeof(struct sockaddr_in);

    /*Creating a microTCP socket using the UDP protocol*/
    microtcp_sock_t socket = microtcp_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(socket.state == INVALID) {
        printf("ERROR : microtcp_socket\n");
        exit(EXIT_FAILURE);
    }

    memset(&sin, 0, server_address_len);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(6886);
    /* Bind to all available network interfaces */
    sin.sin_addr.s_addr = INADDR_ANY;

    if(microtcp_bind(socket, (struct sockaddr *)&sin, server_address_len) == -1){
        printf("ERROR : microtcp_bind\n");
        exit(EXIT_FAILURE);
    }

    while((socket = microtcp_accept(socket, &client_addr, client_address_len)).state != INVALID) {
        printf("New connection accepted!\n");
        while((received = microtcp_recv(&socket, buffer, BUF_LEN, 0)) != -1) {
            buffer[received] = 0;
            printf("Received from client: %s\n", buffer);
        }
        printf("Waiting for new connections...\n");
        printf("All connections closed.\n");
        break;
    }

    return 0;

}
