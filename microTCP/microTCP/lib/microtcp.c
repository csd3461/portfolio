/*
 * microtcp.c
 */

#include <string.h>
#include "microtcp.h"
#include "../utils/crc32.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

/*void set_timeout(int sd) {

	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = MICROTCP_ACK_TIMEOUT_US;

	if(setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, &timeout ,
		sizeof(struct timeval)) < 0) {
			perror("setsockopt");
	}

}*/

microtcp_sock_t microtcp_socket(int domain, int type, int protocol) {

	microtcp_sock_t sock;
	int value;

	value = socket(domain, type, protocol);
    if(value == -1) {
        sock.state = INVALID;
    }else {
        sock.state = UNKNOWN;
    }
    sock.sd = value;

    return sock;

}

int microtcp_bind(microtcp_sock_t socket, const struct sockaddr *address,
	socklen_t address_len) {
		return bind(socket.sd, address, address_len);
}

/* Final implementation (warning no retransmissions) */
microtcp_sock_t microtcp_connect(microtcp_sock_t socket,
	const struct sockaddr *address, socklen_t address_len) {

		srand(time(NULL));

        uint32_t seq_number, ack_number, checksum, tmp_ack_number, tmp_seq_number;
        uint16_t control;

        microtcp_header_t *header = malloc(HEADER_LEN);
        memset(header, 0, HEADER_LEN);
		uint8_t *packet = malloc(HEADER_LEN);
        memset(packet, 0, HEADER_LEN);

        /* SENDTO | SYN, seq = N */
        seq_number = rand();
        header->seq_number = htonl(seq_number);
		SET(control, 1);
        header->control = htons(control);
        memcpy(packet, header, HEADER_LEN);
        checksum = htonl(crc32(packet, HEADER_LEN));
        memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
        sendto(socket.sd, packet, HEADER_LEN, 0, address, address_len);
		printf("SENDTO | SYN, seq = N -> PASSED\n");

        /* RECVFROM | SYN, ACK, seq = M, ack = N + 1 */
		recvfrom(socket.sd, packet, HEADER_LEN, 0, (struct sockaddr *)address, &address_len);
		header = (microtcp_header_t *) packet;
        checksum = ntohl(header->checksum);
        memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN); /* header->checksum = 0 */
        if(checksum != crc32(packet, HEADER_LEN)) {
			socket.state = INVALID;
			return socket;
        }
		tmp_seq_number = seq_number;
        seq_number = ntohl(header->seq_number);
        ack_number = ntohl(header->ack_number);
        control = ntohs(header->control);

		/* Checks if the received packet is what was exptected */
		if(!IS_SET(control, 1) || !IS_SET(control, 3)
			|| (ack_number != tmp_seq_number + 1)) {
				socket.state = INVALID;
				return socket;
		}
		printf("RECVFROM | SYN, ACK seq = M, ack = N + 1 -> PASSED\n");

        /* SENDTO | ACK, seq = N + 1, ack = M + 1 */
		tmp_ack_number = ack_number;
        ack_number = seq_number + 1;
        seq_number = tmp_ack_number;
        header->seq_number = htonl(seq_number);
        header->ack_number = htonl(ack_number);
		CLEAR(control, 1); /* removes SYN */
        header->control = htons(control);
        memcpy(packet, header, HEADER_LEN);
		checksum = htonl(crc32(packet, HEADER_LEN));
        memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
        sendto(socket.sd, packet, HEADER_LEN, 0, address, address_len);
		printf("SENDTO| ACK seq = N + 1, ack = M + 1 -> PASSED\n");

		socket.peeraddr = (struct sockaddr *)address;
		socket.seq_number = seq_number;
		socket.ack_number = ack_number;
		socket.recvbuf = malloc(MICROTCP_RECVBUF_LEN); /* TODO : is this needed? */
        socket.state = ESTABLISHED;

		free(packet);

        return socket;
}

/* Final implementation (warning no retransmissions) */
microtcp_sock_t microtcp_accept(microtcp_sock_t socket,
	struct sockaddr *address, socklen_t address_len) {

		srand(time(NULL));

        uint32_t seq_number, ack_number, checksum, tmp_seq_number, tmp_ack_number;
        uint16_t control;

        microtcp_header_t *header = malloc(HEADER_LEN);
        memset(header, 0, HEADER_LEN);
		uint8_t *packet = malloc(HEADER_LEN);
        memset(packet, 0, HEADER_LEN);

        /* RECVFROM | SYN, seq = N */
        recvfrom(socket.sd, packet, HEADER_LEN, 0, address, &address_len);
        header = (microtcp_header_t *) packet;
        checksum = ntohl(header->checksum);
        memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN); /* header->checksum = 0 */
        if(checksum != crc32(packet, HEADER_LEN)) {
			socket.state = INVALID;
			return socket;
        }
        seq_number = ntohl(header->seq_number);
        control = ntohs(header->control);

		/* Checks if the received packet is what was exptected */
		if(!IS_SET(control, 1)) {
			socket.state = INVALID;
			return socket;
		}
		printf("RECVFROM | SYN, seq = N -> PASSED\n");

        /* SENDTO | SYN, ACK, seq = M, ack = N + 1 */
		tmp_seq_number = seq_number;
        seq_number = rand();
    	ack_number = tmp_seq_number + 1;
        header->seq_number = htonl(seq_number);
        header->ack_number = htonl(ack_number);
		SET(control, 3); /* SYN is already set */
        header->control = htons(control);
        memcpy(packet, header, HEADER_LEN);
		checksum = htonl(crc32(packet, HEADER_LEN));
        memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
        sendto(socket.sd, packet, HEADER_LEN, 0, address, address_len);
		printf("SENDTO | SYN, ACK seq = M, ack = N + 1 -> PASSED\n");

        /* RECVFROM | ACK, seq = N + 1, ack = M + 1 */
		recvfrom(socket.sd, packet, HEADER_LEN, 0, address, &address_len);
        header = (microtcp_header_t *) packet;
        checksum = ntohl(header->checksum);
		memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN); /* header->checksum = 0 */
        if(checksum != crc32(packet, HEADER_LEN)) {
			socket.state = INVALID;
			return socket;
        }
		tmp_seq_number = seq_number;
		tmp_ack_number = ack_number;
        seq_number = ntohl(header->seq_number);
        ack_number = ntohl(header->ack_number);

		/* Checks if the received packet is what was exptected */
		if(!IS_SET(control, 3) || (ack_number != tmp_seq_number + 1)
			|| (seq_number != tmp_ack_number)) {
				socket.state = INVALID;
				return socket;
		}
		printf("RECVFROM | ACK seq = N + 1, ack = M + 1 -> PASSED\n");

		socket.peeraddr = address;
		socket.seq_number = seq_number;
		socket.ack_number = ack_number;
		socket.recvbuf = malloc(MICROTCP_RECVBUF_LEN);
        socket.state = ESTABLISHED;

		free(packet);

        return socket;
}


/* Final implementation (warning no retransmissions) */
microtcp_sock_t microtcp_shutdown(microtcp_sock_t socket, int how) {

	uint32_t seq_number, ack_number, checksum, tmp_seq_number, tmp_ack_number;
	uint16_t control;

	srand(time(NULL));

	microtcp_header_t *header = malloc(HEADER_LEN);
	memset(header, 0, HEADER_LEN);
	uint8_t *packet = malloc(HEADER_LEN);
	memset(packet, 0, HEADER_LEN);

    struct sockaddr *address = socket.peeraddr;
	socklen_t address_len = ADDRESS_LEN;

    /* SENDTO | FIN, ACK, seq = X */
	seq_number = socket.seq_number;
	header->seq_number = htonl(seq_number);
    SET(control, 0);
    SET(control, 3);
    header->control = htons(control);
	memcpy(packet, header, HEADER_LEN);
	checksum = htonl(crc32(packet, HEADER_LEN));
	memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
    sendto(socket.sd, packet, HEADER_LEN, 0, address, address_len);
	printf("SENDTO | FIN, ACK, seq = X -> PASSED\n");

    /* RECVFROM | ACK, ack = X + 1 */
	recvfrom(socket.sd, packet, HEADER_LEN, 0, address, &address_len);
	header = (microtcp_header_t *) packet;
	checksum = ntohl(header->checksum);
	memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN);
	if(checksum != crc32(packet, HEADER_LEN)) {
		socket.state = INVALID;
		return socket;
	}
	ack_number = ntohl(header->ack_number);
	control = ntohs(header->control);

	/* Checks if the received packet is what was exptected */
	if(!IS_SET(control, 3) || (ack_number != seq_number + 1)) {
		socket.state = INVALID;
		return socket;
	}
	printf("RECVFROM | ACK, ack = X + 1 -> PASSED\n");

	socket.state = CLOSING_BY_HOST;

    /* RECVFROM | FIN, ACK, seq = Y */
	recvfrom(socket.sd, packet, HEADER_LEN, 0, address, &address_len);
	header = (microtcp_header_t *) packet;
	checksum = ntohl(header->checksum);
	memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN);
	if(checksum != crc32(packet, HEADER_LEN)) {
		socket.state = INVALID;
		return socket;
	}
    seq_number = ntohl(header->seq_number);
	control = ntohs(header->control);

	/* Checks if the received packet is what was exptected */
	if(!IS_SET(control, 3) || !IS_SET(control, 0)) {
		socket.state = INVALID;
		return socket;
	}
	printf("RECVFROM | FIN, ACK, seq = Y -> PASSED\n");

    /* SENDTO | ACK, seq = X + 1, ack = Y + 1 */
	tmp_seq_number = seq_number;
	seq_number = ack_number;
	ack_number = tmp_seq_number + 1; /* TODO */
	header->seq_number = htonl(seq_number);
    header->ack_number = htonl(ack_number);
	CLEAR(control, 0); /* removes FIN */
    header->control = htons(control);
	memcpy(packet, header, HEADER_LEN);
	checksum = htonl(crc32(packet, HEADER_LEN));
	memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
    sendto(socket.sd, packet, HEADER_LEN, 0, address, address_len);
	printf("SENDTO | ACK, seq = X + 1, ack = Y + 1 -> PASSED\n");

	socket.seq_number = seq_number;
	socket.ack_number = ack_number;
	free(socket.recvbuf);
	socket.state = CLOSED;

	free(packet);

    return socket;

}

/* Example microtcp_send */
ssize_t microtcp_send(microtcp_sock_t *socket, const void *buffer,
	size_t length, int flags) {

		uint32_t seq_number, checksum;

		microtcp_header_t *header = malloc(HEADER_LEN);
		memset(header, 0, HEADER_LEN);
		uint8_t *packet = malloc(MICROTCP_MSS);
		memset(packet, 0, MICROTCP_MSS);

		struct sockaddr *address = socket->peeraddr;
		socklen_t address_len = ADDRESS_LEN;

		seq_number = socket->seq_number;
		header->seq_number = htonl(seq_number);
		header->data_len = htonl(1);
		memcpy(packet, header, HEADER_LEN);
		memcpy(packet + HEADER_LEN, buffer, length);
		checksum = htonl(crc32(packet, MICROTCP_MSS));
		memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
		sendto(socket->sd, packet, MICROTCP_MSS, 0, address, address_len);
		printf("SENDTO MESSAGE -> PASSED\n");

}

/* Simple implementation, used to test microtcp_shutdown */
ssize_t microtcp_recv(microtcp_sock_t *socket, void *buffer, size_t length,
	int flags) {

		uint32_t seq_number, ack_number, checksum, tmp_ack_number, tmp_seq_number, data_len;
		uint16_t control;

		microtcp_header_t *header = malloc(HEADER_LEN);
		memset(header, 0, HEADER_LEN);
		uint8_t *packet = malloc(MICROTCP_MSS);
		memset(packet, 0, MICROTCP_MSS);

		struct sockaddr *address = socket->peeraddr;
		socklen_t address_len = ADDRESS_LEN;

		/* MAIN RECEIVER */
		recvfrom(socket->sd, packet, MICROTCP_MSS, 0, address, &address_len);
	    header = (microtcp_header_t *) packet;
		checksum = ntohl(header->checksum);
		data_len = ntohl(header->data_len);
		memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN);
		if(data_len == 0) {
			if(checksum != crc32(packet, HEADER_LEN)) {
				socket->state = INVALID;
				return -1;
			}
		}else {
			if(checksum != crc32(packet, MICROTCP_MSS)) {
				socket->state = INVALID;
				return -1;
			}
		}
		control = ntohs(header->control);

		if(IS_SET(control, 0) && IS_SET(control, 3)) { /* RECVFROM | FIN, ACK, seq = X */
			printf("RECVFROM | FIN, ACK, seq = X -> PASSED\n");

			/* SENDTO | ACK, ack = X + 1 */
			seq_number = ntohl(header->seq_number);
			ack_number = seq_number + 1;
			header->ack_number = htonl(ack_number);
			CLEAR(control, 0); /* removes FIN */
			header->control = htons(control);
			memcpy(packet, header, HEADER_LEN);
			checksum = htonl(crc32(packet, HEADER_LEN));
			memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
		    sendto(socket->sd, packet, HEADER_LEN, 0, address, address_len);
			printf("SENDTO | ACK, ack = X + 1 -> PASSED\n");

			socket->state = CLOSING_BY_PEER;

			/* SENDTO | FIN, ACK, seq = Y */
			seq_number = socket->seq_number;
			header->seq_number = htonl(seq_number);
			SET(control, 0); /* ACK is already set */
			header->control = htons(control);
			header->checksum = 0;
			memcpy(packet, header, HEADER_LEN);
			checksum = htonl(crc32(packet, HEADER_LEN));
			memcpy(packet + HEADER_LEN - CHECKSUM_LEN, &checksum, CHECKSUM_LEN);
		    sendto(socket->sd, packet, HEADER_LEN, 0, address, address_len);
			printf("SENDTO | FIN, ACK, seq = Y -> PASSED\n");

			/* RECVFROM | ACK, seq = X + 1, ack = Y + 1 */
			recvfrom(socket->sd, packet, HEADER_LEN, 0, address, &address_len);
			header = (microtcp_header_t *) packet;
			checksum = ntohl(header->checksum);
			memset(packet + HEADER_LEN - CHECKSUM_LEN, 0, CHECKSUM_LEN);
			if(checksum != crc32(packet, HEADER_LEN)) {
				socket->state = INVALID;
				return -1;
			}
			tmp_ack_number = ack_number;
			tmp_seq_number = seq_number;
			seq_number = ntohl(header->seq_number);
			ack_number = ntohl(header->ack_number);

			/* Checks if the received packet is what was exptected */
			if(!IS_SET(control, 3) || (seq_number != tmp_ack_number)
				|| (ack_number != tmp_seq_number + 1)) {
					socket->state = INVALID;
					return -1;
			}
			printf("RECVFROM | ACK, seq = X + 1, ack = Y + 1 -> PASSED\n");

			free(socket->recvbuf);
			socket->state = CLOSED;

			free(packet);

			return -1;

		}else {

			uint8_t *data = packet + HEADER_LEN;

			memcpy(buffer, data, length);

			printf("RECVFROM MESSAGE -> PASSED\n");

		}

		return length;

}

char** create_packets(char* str, int size) {
	char** packets;
	int i, j, multitude;

	multitude = (strlen(str) % size == 0)?0:1 + strlen(str) / size;
	packets = malloc(multitude * sizeof(char**));

	for(i = 0; i < multitude; i++) {
		packets[i] = malloc((size + 1) * sizeof(char*));
	}

	for(i = 0; i < multitude; i++) {
		for(j = 0; j < size; j++) {
			packets[i][j] = str[i * size + j];
		}
		packets[i][size] = '\0';
	}

	return packets;
}

/*
ssize_t microtcp_send(microtcp_sock_t *socket, const void *buffer,
	size_t length, int flags) {
		int multitude;
		char** packets;
		int packet_size;
		char* str;
		int i;

		str = (char*) buffer;
		//packet_size = MICROTCP_MSS;
		packet_size = 40;

		microtcp_header_t *header;
		header = malloc(sizeof(microtcp_header_t));
		memset(header, 0, sizeof(microtcp_header_t));
		socket->recvbuf = malloc(MICROTCP_RECVBUF_LEN * sizeof(uint8_t));
		memset(socket->recvbuf, 0, MICROTCP_RECVBUF_LEN);

		struct sockaddr *address = socket->peeraddr;
		socklen_t address_len = sizeof(struct sockaddr);

		header->seq_number = socket->seq_number;
		memcpy(socket->recvbuf, header, sizeof(microtcp_header_t));
		memcpy(socket->recvbuf + sizeof(microtcp_header_t), buffer, length);
		header->checksum = htonl(crc32(socket->recvbuf, MICROTCP_RECVBUF_LEN));
		memcpy(socket->recvbuf, header, sizeof(microtcp_header_t));
		//memcpy(socket->recvbuf + sizeof(microtcp_header_t), buffer, length);
		//sendto(socket->sd, socket->recvbuf, MICROTCP_RECVBUF_LEN, 0, address, address_len);

		multitude = ((strlen(str) % packet_size == 0)?0:1 + strlen(str) / packet_size);
		packets = create_packets(str, packet_size);

		for(i = 0; i < multitude; i++) {
			memcpy(socket->recvbuf + sizeof(microtcp_header_t), packets[i], packet_size + 1);
			sendto(socket->sd, socket->recvbuf, MICROTCP_RECVBUF_LEN, 0, address, address_len);
		}

}
*/
