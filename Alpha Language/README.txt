====MEMBERS====
Sofia Kampaki 3221
Simidalas Nikos 3461
Skerdi Basha 3377

====HOWTO====
alpha -> ./alpha ../input/test.alpha ../output/binary.avm
avm -> ./avm ../output/binary.avm
You can run avm with -nowarn (doesn't print warnings) 
or with -outwarn (prints warnings on a file named warnings)