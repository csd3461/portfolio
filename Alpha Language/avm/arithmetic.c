#include "arithmetic.h"

extern void runtime_error(char *providedMessage, ...);

arithmetic_func_t arithmeticFuncs[] = {
	add_impl,
	sub_impl,
	mul_impl,
	div_impl,
	mod_impl
};

/* Performs the add operation */
double add_impl(double x, double y) {

	return x + y;

}

/* Performs the sub operation */
double sub_impl(double x, double y) {

	return x - y;

}

/* Performs the mul operation */
double mul_impl(double x, double y) {

	return x * y;

}

/* Performs the div operation */
double div_impl(double x, double y) {

	if(y == 0) {
		runtime_error("illegal division by zero");
		return 0;
	}

	return x / y;

}

/* Performs the mod operation */
double mod_impl(double x, double y) {

	if(y == 0) {
		runtime_error("illegal modulo operation by zero");
		return 0;
	}

	return ((int) x) % ((int) y);

}

/* Executes an arithmetic operation */
void execute_arithmetic(Instruction *instr) {

	AVM_Memcell *lv = avm_translate_operand(&instr->result, NULL);
	AVM_Memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	AVM_Memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	assert(lv && (((&stack[AVM_STACKSIZE-1] >= lv) && (lv > &stack[top])) || lv == &retval));
	assert(rv1 && rv2);

	if(rv1->type != number_m || rv2->type != number_m) {
		runtime_error("illegal variable type in arithmetic expression");
	}else {
		arithmetic_func_t op = arithmeticFuncs[instr->opcode - add_v];
		avm_memcellclear(lv);
		lv->type = number_m;
		lv->data.numVal = (*op)(rv1->data.numVal, rv2->data.numVal);
	}

	return;

}