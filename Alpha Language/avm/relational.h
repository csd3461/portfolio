#ifndef RELATIONAL_H_
#define RELATIONAL_H_

#include "environment.h"

typedef unsigned char (*tobool_func_t)(AVM_Memcell*);
typedef unsigned char (*cmp_func_t)(AVM_Memcell*, AVM_Memcell*);
typedef unsigned char (*relational_func_t)(AVM_Memcell*, AVM_Memcell*);

/* Returns the boolean represantation of a number */
unsigned char number_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a string */
unsigned char string_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a boolean */
unsigned char bool_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a table */
unsigned char table_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a user function */
unsigned char userfunc_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a library function */
unsigned char libfunc_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of nil */
unsigned char nil_tobool(AVM_Memcell *m);

/* This functions should never be called */
unsigned char undef_tobool(AVM_Memcell *m);

/* Returns the boolean represantation of a variable */
unsigned char avm_tobool(AVM_Memcell *m);

/* Compares two numbers */
unsigned char cmp_numbers(AVM_Memcell *m1, AVM_Memcell *m2);

/* Compares two strings */
unsigned char cmp_strings(AVM_Memcell *m1, AVM_Memcell *m2);

/* Compares two bools */
unsigned char cmp_bools(AVM_Memcell *m1, AVM_Memcell *m2);

/* Compares two tables */
unsigned char cmp_tables(AVM_Memcell *m1, AVM_Memcell *m2);

/* Compares two user functions */
unsigned char cmp_userfuncs(AVM_Memcell *m1, AVM_Memcell *m2);

/* Compares two library functions */
unsigned char cmp_libfuncs(AVM_Memcell *m1, AVM_Memcell *m2);

/* Performs the jeq operation */
unsigned char jeq_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs the jne operation */
unsigned char jne_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs the jle operation */
unsigned char jle_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs the jge operation */
unsigned char jge_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs the jlt operation */
unsigned char jlt_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs the jgt operation */
unsigned char jgt_impl(AVM_Memcell *rv1, AVM_Memcell *rv2);

/* Performs a relational operation */
void execute_relational(Instruction *instr);

#endif