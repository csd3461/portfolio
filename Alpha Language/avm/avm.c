#include "decoder.h"
#include "environment.h"
#include <sys/stat.h>
#include <stdarg.h>

void runtime_error(const char *providedMessage, ...) {

	va_list args;
    va_start(args, providedMessage);
	fprintf(stderr, "avm: runtime error: ");
    vfprintf(stderr, providedMessage, args);
	fprintf(stderr, ": line: %u\n", currLine);
    va_end(args);
	executionFinished = 1;

	return;

}

int main(int argc, char **argv) {

    ++argv, --argc; /* skipping over program name */

	mkdir("../output", 0777);

	warnings = stderr;
	
	if(argc == 0) {
		fprintf(stderr, "No input\n");
        return 1;
	}else if(argc == 2) {
		if(strcmp(argv[1], "-nowarn") == 0) warnings = NULL;
		else if(strcmp(argv[1], "-outwarn") == 0) {
			warnings = fopen("../output/warnings", "w");
			if(warnings == NULL) {
				fprintf(stderr, "Failed to open file\n");
				exit(EXIT_FAILURE);
			}
		}
	}

	binary_reader("../output/vertext.avm", argv[0]);

	avm_init();

	while(!executionFinished) execute_cycle();
	
	return 0;
	
}