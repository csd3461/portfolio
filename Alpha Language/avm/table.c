#include "table.h"
#include <stdint.h>

#define TTEST(x) fprintf(stderr, "TABLE(%d)\n", x);

extern void runtime_error(char *providedMessage, ...);

extern char *typeStrings[];
extern cmp_func_t cmpFuncs[];

/* Alpha hash function implementation for pointers */
unsigned int pointer_hash(void *ptr) {

	unsigned int hash = (uintptr_t) ptr;

	hash = number_hash(hash);

	return hash;

}

/* Alpha hash function implementation for numbers */
unsigned int number_hash(double value) {

	unsigned int hash = (int) value;

	hash = (hash * 2) + 23;
	hash =  hash * 89;
	hash = hash % AVM_TABLE_HASHSIZE;

	return hash;

}

/* Alpha hash function implementation for strings */
unsigned int string_hash(char *str) {

	int i = 0;
	unsigned int hash = 127;
	size_t len = strlen(str);

	while (i != len) {
		hash = hash * 83 + str[i] * 41;
		i++;
	}
	hash = hash % AVM_TABLE_HASHSIZE;

	return hash;

}

/* Increases table's reference counter */
void avm_tableincrefcounter(AVM_Table *t) {

	++t->refCounter;

	return;

}

/* Decreases table's reference counter */
void avm_tabledecrefcounter(AVM_Table *t) {

	assert(t->refCounter > 0);
	if(!--t->refCounter) avm_tabledestroy(t);

	return;

}

/* Initializes the table buckets of a hash table */
void avm_tablebucketsinit(AVM_TableBucket **p) {
	
	int i = 0;

	for(i = 0; i < AVM_TABLE_HASHSIZE; ++i) {
		p[i] = NULL;
	}

	return;

}

/* Destroys a table bucket */
void avm_tablebucketdestroy(AVM_TableBucket **p) {

	int i = 0;
	AVM_TableBucket *b = NULL;

	for(i = 0; i < AVM_TABLE_HASHSIZE; ++i) { // Thus Spoke a bug : ++p
		for(b = p[i]; b;) {
			AVM_TableBucket *del = b;
			b = b->next;
			avm_memcellclear(&del->key);
			avm_memcellclear(&del->value);
			free(del);
		}
		p[i] = NULL;
	}

	return;

}

/* Creates a new table */
AVM_Table *avm_tablenew() {

	AVM_Table *t = malloc(sizeof(AVM_Table));
	AVM_WIPEOUT(*t);

	t->refCounter = 0;
	t->totalN = 0;
	t->totalS = 0;
	t->totalB = 0;
	t->totalT = 0;
	t->totalU = 0;
	t->totalL = 0;
	avm_tablebucketsinit(t->numIndexed);
	avm_tablebucketsinit(t->strIndexed);
	avm_tablebucketsinit(t->boolIndexed);
	avm_tablebucketsinit(t->tableIndexed);
	avm_tablebucketsinit(t->userfuncIndexed);
	avm_tablebucketsinit(t->libfuncIndexed);

	return t;

}

/* Destroys the table */
void avm_tabledestroy(AVM_Table *t) {

	avm_tablebucketdestroy(t->numIndexed);
	avm_tablebucketdestroy(t->strIndexed);
	avm_tablebucketdestroy(t->boolIndexed);
	avm_tablebucketdestroy(t->tableIndexed);
	avm_tablebucketdestroy(t->userfuncIndexed);
	avm_tablebucketdestroy(t->libfuncIndexed);
	free(t);

	return;

}

/* Executes a newtable operation */
void execute_newtable(Instruction *instr) {

	AVM_Memcell *lv = avm_translate_operand(&instr->result, NULL);
	assert(lv && (((&stack[AVM_STACKSIZE-1] >= lv) && (lv > &stack[top])) || lv == &retval));

	avm_memcellclear(lv);

	lv->type = table_m;
	lv->data.tableVal = avm_tablenew();
	avm_tableincrefcounter(lv->data.tableVal);

	return;

}

/* Executes a tablegetelem  operation */
void execute_tablegetelem(Instruction *instr) {

	AVM_Memcell *lv = avm_translate_operand(&instr->result, NULL);
	AVM_Memcell *t = avm_translate_operand(&instr->arg1, NULL);
	AVM_Memcell *i = avm_translate_operand(&instr->arg2, &ax);

	assert(lv && (((&stack[AVM_STACKSIZE-1] >= lv) && (lv > &stack[top])) || lv == &retval));
	assert(t && (&stack[AVM_STACKSIZE-1] >= t) && (t > &stack[top]));
	assert(i);

	avm_memcellclear(lv);
	lv->type = nil_m;

	if(t->type != table_m) {
		runtime_error("illegal use of '%s' as table", typeStrings[t->type]);
	}else {
		AVM_Memcell *content = avm_tablegetelem(t->data.tableVal, i);
		if(content) {
			avm_assign(lv, content);
		}
		// I drink your milkshake
	}

	return;

}

/* Executes a tablesetelem operation */
void execute_tablesetelem(Instruction *instr) {

	AVM_Memcell *t = avm_translate_operand(&instr->result, NULL);
	AVM_Memcell *i = avm_translate_operand(&instr->arg1, &ax);
	AVM_Memcell *c = avm_translate_operand(&instr->arg2, &bx);

	assert(t && (&stack[AVM_STACKSIZE-1] >= t) && (t > &stack[top]));
	assert(i && c);

	if(t->type != table_m) {
		runtime_error("illegal use of '%s' as table", typeStrings[t->type]);
	}else {
		avm_tablesetelem(t->data.tableVal, i, c);
	}

	return;

}

/* Performs the tablegetelem operation */
AVM_Memcell *avm_tablegetelem(AVM_Table *table, AVM_Memcell *index) {

	switch(index->type) {
		case number_m: return avm_table_getvalue(table->numIndexed[number_hash(index->data.numVal)], index);
		case string_m: return avm_table_getvalue(table->strIndexed[string_hash(index->data.strVal)], index);
		case bool_m: return avm_table_getvalue(table->boolIndexed[index->data.boolVal], index);
		case table_m: return avm_table_getvalue(table->tableIndexed[pointer_hash(index->data.tableVal)], index);
		case userfunc_m: return avm_table_getvalue(table->userfuncIndexed[number_hash(index->data.funcVal)], index);
		case libfunc_m: return avm_table_getvalue(table->libfuncIndexed[get_libfunc_num(index->data.libfuncVal)], index);
		case nil_m:
		case undef_m:
			runtime_error("illegal use of '%s' as table index", typeStrings[index->type]);
			break;
		default:
			assert(0);
	}

	return NULL;

}

/* Returns the value of a table bucket */
AVM_Memcell *avm_table_getvalue(AVM_TableBucket *b, AVM_Memcell *key) {

	AVM_TableBucket *tmp = b;

	while(tmp != NULL) {
		if((*cmpFuncs[key->type])(&tmp->key, key) == 1) return &tmp->value;
		assert(key->type != bool_m && key->type != libfunc_m);
		tmp = tmp->next;
	}

	return NULL;

}

/* Performs the tablesetelem operation */
void avm_tablesetelem(AVM_Table *table, AVM_Memcell *index, AVM_Memcell *content) {

	switch(index->type) {
		case number_m:
		    table->totalN += avm_table_setvalue(&table->numIndexed[number_hash(index->data.numVal)], index, content);
			break;
		case string_m: 
			table->totalS += avm_table_setvalue(&table->strIndexed[string_hash(index->data.strVal)], index, content);
			break;
		case bool_m: 
			table->totalB += avm_table_setvalue(&table->boolIndexed[index->data.boolVal], index, content);
			break;
		case table_m: 
			table->totalT += avm_table_setvalue(&table->tableIndexed[pointer_hash(index->data.tableVal)], index, content);
			break;
		case userfunc_m: 
			table->totalU += avm_table_setvalue(&table->userfuncIndexed[number_hash(index->data.funcVal)], index, content);
			break;
		case libfunc_m: 
			table->totalL += avm_table_setvalue(&table->libfuncIndexed[get_libfunc_num(index->data.libfuncVal)], index, content);
			break;
		case nil_m:
		case undef_m:
			runtime_error("illegal use of '%s' as table index", typeStrings[index->type]);
			break;
		default: 
			assert(0);

	}
	
    return;
	
}

/* Returns a new table bucket element */
AVM_TableBucket *new_elem(AVM_Memcell *key, AVM_Memcell *value) {

	AVM_TableBucket *elem = malloc(sizeof(AVM_TableBucket));
	AVM_WIPEOUT(elem->key);
	AVM_WIPEOUT(elem->value);
	avm_assign(&elem->key, key);
	avm_assign(&elem->value, value);
	elem->next = NULL;

	return elem;

}

/* Stores an elememnt in a table bucket */
signed char avm_table_setvalue(AVM_TableBucket **b, AVM_Memcell *key, AVM_Memcell *value) {

	AVM_TableBucket *elem = NULL;
	AVM_TableBucket *tmp = NULL;
	AVM_TableBucket *ptmp = NULL;

	if(value->type == nil_m) {
		if(*b == NULL) return 0;
		ptmp = *b;
		tmp = (*b)->next;
		if((*cmpFuncs[key->type])(&ptmp->key, key) == 1) {
			avm_memcellclear(&ptmp->key);
			avm_memcellclear(&ptmp->value);
			free(ptmp);
			*b = tmp;
			return -1;
		}
		while(tmp != NULL) {
			if((*cmpFuncs[key->type])(&tmp->key, key) == 1) {
				ptmp->next = tmp->next;
				avm_memcellclear(&tmp->key);
				avm_memcellclear(&tmp->value);
				free(tmp);
				return -1;
			}
			ptmp = tmp;
			tmp = tmp->next;
		}
		return 0;
	}

	if(*b == NULL) {
		elem = new_elem(key, value);
		*b = elem;
	}else {
		tmp = *b;
		while(tmp != NULL) {
			if((*cmpFuncs[key->type])(&tmp->key, key) == 1) {
				avm_memcellclear(&tmp->key);
				avm_memcellclear(&tmp->value);
				avm_assign(&tmp->key, key);
				avm_assign(&tmp->value, value);
				return 0;
			}
			assert(key->type != bool_m && key->type != libfunc_m);
			tmp = tmp->next;
		}
		elem = new_elem(key, value);
		elem->next = *b;
		*b = elem;
	}

	return 1;

}

