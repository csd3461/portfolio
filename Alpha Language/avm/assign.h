#ifndef ASSIGN_H_
#define ASSIGN_H_

#include "environment.h"

/* Prints an assign related warning */
void undef_assign_warning();

/* Executes an assign operation */
void execute_assign(Instruction *instr);

/* Performs the assign operation on the AVM */
void avm_assign(AVM_Memcell *lv, AVM_Memcell *rv);

#endif