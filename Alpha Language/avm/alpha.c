#include "alpha.h"

/* Returns a constant number function form the constant numbers array, given an index */
double consts_getnumber(unsigned int index) {

	return numConsts[index];

}

/* Returns a constant string form the constants string array, given an index */
char *consts_getstring(unsigned int index) {

	return stringConsts[index];

}

/* Returns a user function form the user functions array, given an index */
UserFunc userfuncs_getfunc(unsigned int index) {

	return userFuncs[index];

}

/* Returns a library function form the library functions array, given an index */
char *libfuncs_getused(unsigned int index) {

	return namedLibfuncs[index];

}

/* Initializes the constant numbers array */
void numberconsts_init() {

	numConsts = malloc(totalNumConsts * sizeof(double));

	return;

}

/* Initializes the constant strings array */
void stringconsts_init() {

	stringConsts = malloc(totalStringConsts * sizeof(char*));

	return;

}

/* Initializes the user functions array */
void userfuncs_init() {

	userFuncs = malloc(totalStringConsts * sizeof(UserFunc));

	return;

}

/* Initializes the library functions array */
void libfuncs_init() {

	namedLibfuncs = malloc(totalNamedLibfuncs * sizeof(char*));

	return;

}

/* Initializes the instructions array */
void instr_init() {

	instructions = malloc(totalInstr * sizeof(Instruction));

	return;

}

/* Returns a new empty instruction */
Instruction new_instr() {

	Instruction instr;
	instr.opcode = nop_v;
	instr.arg1.type = omit_a;
	instr.arg1.val = 0;
	instr.arg2.type = omit_a;
	instr.arg2.val = 0;
	instr.result.type = omit_a;
	instr.result.val = 0;
	instr.srcLine = 0;

	return instr;

}

/* Stores an isntruction in the instruction array */
void emit_instr(Instruction t) {

	Instruction *p 	= instructions + currInstr;
	p->opcode   	= t.opcode;
	p->arg1 		= t.arg1;
	p->arg2 		= t.arg2;
	p->result 		= t.result;
	p->srcLine		= t.srcLine;

	return;

}