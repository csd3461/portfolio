#ifndef LIBFUNC_H_
#define LIBFUNC_H_

#include "table.h"
#include "environment.h"
#include <math.h>

#define TOTAL_LIBFUNCS 12

typedef void (*lib_func_t)(void);

/* Stores the keys of a table to another table */
void copy_keys(AVM_Table *table, AVM_TableBucket *indices[], unsigned int totalIndexed);

/* Stores the buckets of a table in another table */
void copy_buckets(AVM_Table *table, AVM_TableBucket *indices[], unsigned int totalIndexed);

/* Returns the representative number of a library function */
unsigned char get_libfunc_num(char *name);

/* Returns a library function corresponding to the given id */
lib_func_t avm_getlibfunc(char *id);

/* Implements the print library function */
void libfunc_print();

/* Implements the input library function */
void libfunc_input();

/* Implements the objectmemberkeys library function */
void libfunc_objectmemberkeys();

/* Implements the tablelength library function */
void libfunc_objecttotalmembers();

/* Implements the objectcopy library function */
void libfunc_objectcopy();

/* Implements the totalarguments library function */
void libfunc_totalarguments();

/* Implements the arguments library function */
void libfunc_argument();

/* Implements the typeof library function */
void libfunc_typeof();

/* Implements the strtonum library function */
void libfunc_strtonum();

/* Implements the sqrt library function */
void libfunc_sqrt();

/* Implements the cos library function */
void libfunc_cos();

/* Implements the sin library function */
void libfunc_sin();

#endif