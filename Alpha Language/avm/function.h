#ifndef FUNCTION_H_
#define FUNCTION_H_

#include "environment.h"
#include "libfunc.h"

/* Returns a user function that resides to the given address */
UserFunc *avm_getfuncinfo(unsigned int address);

/* Executes a call operation */
void execute_call(Instruction *instr);

/* Executes a pusharg operation */
void execute_pusharg(Instruction *instr);

/* Executes a funcenter operation */
void execute_funcenter(Instruction *instr);

/* Executes a funcexit operation */
void execute_funcexit(Instruction *unused);

/* Performs a call to a library function corresponding to the given id */
void avm_calllibfunc(char *id);

#endif