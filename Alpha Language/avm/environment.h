#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#include "alpha.h"
#include <math.h>

#define execute_add execute_arithmetic
#define execute_sub execute_arithmetic
#define execute_mul execute_arithmetic
#define execute_div execute_arithmetic
#define execute_mod execute_arithmetic

#define execute_jeq execute_relational
#define execute_jne execute_relational
#define execute_jle execute_relational
#define execute_jge execute_relational
#define execute_jlt execute_relational
#define execute_jgt execute_relational

#define MAX_TABLE_RECURSIONS 1000

#define AVM_STACKENV_SIZE 4
#define AVM_MAX_INSTRUCTIONS (unsigned int) nop_v
#define AVM_ENDING_PC totalInstr

#define AVM_STACKSIZE 4096
#define AVM_WIPEOUT(m) memset(&m, 0, sizeof(m))
#define AVM_TABLE_HASHSIZE 211

#define AVM_NUMACTUALS_OFFSET 4
#define AVM_SAVEDPC_OFFSET 3
#define AVM_SAVEDTOP_OFFSET 2
#define AVM_SAVEDTOPSP_OFFSET 1

typedef struct AlphaTable AVM_Table;
typedef struct AlphaMemcell AVM_Memcell;
typedef struct AlphaTableBucket AVM_TableBucket;
typedef enum AlphaMemcellType AVM_MemcellType;
typedef void (*execute_func_t)(Instruction*); 
typedef void (*memclear_func_t)(AVM_Memcell*);
typedef char* (*tostring_func_t)(AVM_Memcell*);

enum AlphaMemcellType {
	number_m = 0,
	string_m = 1,
	bool_m = 2,
	table_m = 3,
	userfunc_m = 4,
	libfunc_m = 5,
	nil_m = 6,
	undef_m = 7
};

struct AlphaMemcell {
	AVM_MemcellType type;
	union {
		double 			numVal;
		char 			*strVal;
		unsigned char 	boolVal;
		AVM_Table 		*tableVal;
		unsigned int 	funcVal;
		char 			*libfuncVal;
	} data;
};

struct AlphaTableBucket {
	AVM_Memcell key;
	AVM_Memcell value;
	struct AlphaTableBucket *next;
};

struct AlphaTable {
	unsigned int refCounter;
	AVM_TableBucket *numIndexed[AVM_TABLE_HASHSIZE];
	AVM_TableBucket *strIndexed[AVM_TABLE_HASHSIZE];
	AVM_TableBucket *boolIndexed[AVM_TABLE_HASHSIZE];
	AVM_TableBucket *tableIndexed[AVM_TABLE_HASHSIZE];
	AVM_TableBucket *userfuncIndexed[AVM_TABLE_HASHSIZE];
	AVM_TableBucket *libfuncIndexed[AVM_TABLE_HASHSIZE];
	unsigned int totalN;
	unsigned int totalS;
	unsigned char totalB;
	unsigned int totalT;
	unsigned int totalU;
	unsigned char totalL;
};

unsigned char executionFinished;
unsigned int pc;
unsigned int currLine;

unsigned int totalActuals;

// Registers for instruction's fields
AVM_Memcell ax, bx, cx;
AVM_Memcell retval;

unsigned int top;
unsigned int topsp;

AVM_Memcell *stack;

FILE *warnings;

unsigned int table_recursion_detected;
unsigned int table_recursion_counter;

/* Initializes the AVM */
void avm_init();

/* Initializes the AVM stack */
void avm_stack_init();

/* Initializes the environment variables */
void environment_init();

/* Executes a cycle(instruction) */
void execute_cycle();

/* Translates a virual machine argument */
AVM_Memcell *avm_translate_operand(VMarg *arg, AVM_Memcell *reg);

/* Converts a number to a string */
char *number_tostring(AVM_Memcell *m);

/* Converts a string to a string */
char *string_tostring(AVM_Memcell *m);

/* Converts a bool to a string */
char *bool_tostring(AVM_Memcell *m);

/* Merges two strings */
char *strmerge(char *buf1, char *buf2, unsigned int *max_size);

/* Converts the indexed elements of a table to a string */
char *strmerge_indexed(AVM_TableBucket *b[], unsigned int totalIndexed, unsigned int *count, 
								unsigned int all, char *str, unsigned int *current_size);

/* Converts a table to a string */
char *table_tostring(AVM_Memcell *m);

/* Converts a userfunc to a string */
char *userfunc_tostring(AVM_Memcell *m);

/* Converts a libfunc to a string */
char *libfunc_tostring(AVM_Memcell *m);

/* Converts a nil to a string */
char *nil_tostring(AVM_Memcell *m);

/* Converts an undef to a string */
char *undef_tostring(AVM_Memcell *m);

/* Returns the string represantation of a variable */
char *avm_tostring(AVM_Memcell *m);

/* Returns an environment value from the stack */
unsigned int avm_get_envvalue(unsigned int i);

/* Returns the number of total actuals */
unsigned int avm_totalactuals();

/* Returns the ith actual */
AVM_Memcell *avm_getactual(unsigned int i);

/* Decreases the top of the stack */
void avm_dec_top(void);

/* Stores a value in the stack */
void avm_push_envalue(unsigned int val);

/* Saves the current environment variables */
void avm_callsaveenvironment();

/* Clears a string */
void memclear_string(AVM_Memcell *m);

/* Clears a table */
void memclear_table(AVM_Memcell *m);

/* Clears a memory cell */
void avm_memcellclear(AVM_Memcell *m);

/* Executes a jump instruction */
void execute_jump(Instruction *instr);

/* Executes a nop instruction */
void execute_nop();

#endif