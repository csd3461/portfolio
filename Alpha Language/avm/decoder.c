#include "decoder.h"

/* Prints a virtual machine argument */
void print_vmarg(FILE *file, VMarg arg) {
		
	if(arg.type != omit_a) {
		if(arg.type == retval_a) fprintf(file, "%d   ", arg.type);
		else fprintf(file, "%d %d   ", arg.type, arg.val);
	}
	
	return;

}

/* Reads a virtual machine argument from a binary file */
VMarg read_binary_vmarg(FILE *bfile) {

	VMarg arg;

	fread(&arg.type, sizeof(VMargType), 1, bfile);
	fread(&arg.val, sizeof(unsigned int), 1, bfile);

	if(arg.type == global_a) {
		if(totalGlobalVars <= arg.val) totalGlobalVars = arg.val + 1;
	}
	
	return arg;

}

/* Converts a file from binary to text */
void binary_reader(const char *vpath, const char *bpath) {

	int i = 0;
	long int magic_number = 0;
	size_t string_length = 0;
	Instruction t = new_instr();
	totalGlobalVars = 0;

	// VERIFICATION FILE (fprintf)
	FILE *vfile = fopen(vpath, "w");
	if(vfile == NULL) {
		fprintf(stderr, "Failed to open file\n");
		exit(EXIT_FAILURE);
	}

	// BINARY FILE (fread)
	FILE *bfile = fopen(bpath, "rb");
	if(bfile == NULL) {
		fprintf(stderr, "Failed to open file\n");
		exit(EXIT_FAILURE);
	}

	// Reads magic number
	fread(&magic_number, sizeof(long int), 1, bfile);
	if(magic_number != MAGIC_NUMBER) {
		fprintf(stderr, "Failed to load file, wrong version number\n");
		exit(EXIT_FAILURE);
	}
	fprintf(vfile, "%li\n", magic_number);

	// Reads const numbers
	fread(&totalNumConsts, sizeof(unsigned int), 1, bfile);
	numberconsts_init();
	fprintf(vfile, "%u\n", totalNumConsts);
	for(i = 0; i < totalNumConsts; i++) {
		fread(&numConsts[i], sizeof(double), 1, bfile);
		fprintf(vfile, "%f\n", numConsts[i]);
	}
	
	// Reads const strings
	fread(&totalStringConsts, sizeof(unsigned int), 1, bfile);
	stringconsts_init();
	fprintf(vfile, "%u\n", totalStringConsts);
	for(i = 0; i < totalStringConsts; i++) {
		fread(&string_length, sizeof(size_t), 1, bfile);
		stringConsts[i] = malloc(sizeof(char) * string_length);
		fread(stringConsts[i], sizeof(char), string_length, bfile);
		fprintf(vfile, "%zu %s\n", string_length, stringConsts[i]);
	}

	// Reads user functions
	fread(&totalUserFuncs, sizeof(unsigned int), 1, bfile);
	userfuncs_init();
	fprintf(vfile, "%u\n", totalUserFuncs);
	for(i = 0; i < totalUserFuncs; i++) {
		fread(&userFuncs[i].address, sizeof(unsigned int), 1, bfile);
		fread(&userFuncs[i].localSize, sizeof(unsigned int), 1, bfile);
		fread(&string_length, sizeof(size_t), 1, bfile);
		userFuncs[i].id = malloc(sizeof(char) * string_length);
		fread(userFuncs[i].id, sizeof(char), string_length, bfile);
		fprintf(vfile, "%u %u %zu %s\n", userFuncs[i].address, userFuncs[i].localSize, string_length, userFuncs[i].id);
	}

	// Reads library functions
	fread(&totalNamedLibfuncs, sizeof(unsigned int), 1, bfile);
	libfuncs_init();
	fprintf(vfile, "%u\n", totalNamedLibfuncs);
	for(i = 0; i < totalNamedLibfuncs; i++) {
		fread(&string_length, sizeof(size_t), 1, bfile);
		namedLibfuncs[i] = malloc(sizeof(char) * string_length);
		fread(namedLibfuncs[i], sizeof(char), string_length, bfile);
		fprintf(vfile, "%zu %s\n", string_length, namedLibfuncs[i]);
	}
	
	// Reads instructions
	fread(&totalInstr, sizeof(unsigned int), 1, bfile);
	instr_init();
	fprintf(vfile, "%u\n", totalInstr);
	for(currInstr = 0; currInstr < totalInstr; currInstr++) {
		fread(&t.opcode, sizeof(VirtualMachineOpCode), 1, bfile);
		fprintf(vfile, "%d   ", t.opcode);
		t.arg1 = read_binary_vmarg(bfile);
		print_vmarg(vfile, t.arg1);
		t.arg2 = read_binary_vmarg(bfile);
		print_vmarg(vfile, t.arg2);
		t.result = read_binary_vmarg(bfile);
		print_vmarg(vfile, t.result);
		fread(&t.srcLine, sizeof(unsigned int), 1, bfile);
		fprintf(vfile, "%u", t.srcLine);
		fprintf(vfile, "\n");
		emit_instr(t);
	}

	fclose(vfile);
	fclose(bfile);

	return;

}