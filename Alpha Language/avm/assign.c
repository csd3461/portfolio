#include "assign.h"

#define ATEST(x) fprintf(stderr, "ASSIGN(%d)\n", x);

extern void avm_tableincrefcounter(AVM_Table *t);

/* Prints an assign related warning */
void undef_assign_warning() {

	if(warnings != NULL) {
		fprintf(warnings, "avm: warning: assign from undefined variable");
		fprintf(warnings, ": line: %u\n", currLine);
	}

	return;

}

/* Executes an assign operation */
void execute_assign(Instruction *instr) {
	
	AVM_Memcell *lv = avm_translate_operand(&instr->result, NULL);
	AVM_Memcell *rv = avm_translate_operand(&instr->arg1, &ax);
	
	assert(lv && (((&stack[AVM_STACKSIZE-1] >= lv) && (lv > &stack[top])) || lv == &retval));
	assert(rv);
	
	avm_assign(lv, rv);
	
	return;
	
}

/* Performs the assign operation */
void avm_assign(AVM_Memcell *lv, AVM_Memcell *rv) {
	
	if(lv == rv) return;
	
	if(lv->type == table_m && rv->type == table_m 
		&& lv->data.tableVal == rv->data.tableVal) return;
	   
	if(rv->type == undef_m) {
		undef_assign_warning();
	}

	avm_memcellclear(lv);
	
	memcpy(lv, rv, sizeof(AVM_Memcell));
	
	if(lv->type == string_m) lv->data.strVal = strdup(rv->data.strVal);
	else if(lv->type == table_m) avm_tableincrefcounter(lv->data.tableVal);

	return;

}