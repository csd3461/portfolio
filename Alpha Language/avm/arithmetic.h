#ifndef ARITHMETIC_H_
#define ARITHMETIC_H_

#include "environment.h"

typedef double (*arithmetic_func_t)(double x, double y);

/* Performs the add operation */
double add_impl(double x, double y);

/* Performs the sub operation */
double sub_impl(double x, double y);

/* Performs the mul operation */
double mul_impl(double x, double y);

/* Performs the div operation */
double div_impl(double x, double y);

/* Performs the mod operation */
double mod_impl(double x, double y);

/* Executes an arithmetic operation */
void execute_arithmetic(Instruction *instr);

#endif