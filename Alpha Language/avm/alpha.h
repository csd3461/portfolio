#ifndef ALPHA_H_
#define ALPHA_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef enum VMOpCode {
	assign_v, add_v, sub_v,
	mul_v, div_v, mod_v,
	jeq_v, jne_v, jle_v, jge_v, 
	jlt_v, jgt_v, call_v, pusharg_v, 
	funcenter_v, funcexit_v, 
	newtable_v, tablegetelem_v, 
	tablesetelem_v, jump_v, nop_v
} VirtualMachineOpCode;

typedef enum VMargType {
	label_a = 0,
	global_a = 1,
	formal_a = 2,
	local_a = 3,
	number_a = 4,
	string_a = 5,
	bool_a = 6,
	nil_a = 7,
	userfunc_a = 8, 
	libfunc_a = 9,
	retval_a = 10,
	omit_a = 11
} VMargType;

typedef struct VirtualMachinearg {
	VMargType type;
	unsigned int val;
} VMarg;

typedef struct InstructionInfo {
	VirtualMachineOpCode 		opcode;
	VMarg 						arg1;
	VMarg 						arg2;
	VMarg 						result;
	unsigned int				srcLine;
} Instruction;

typedef struct UserFunction {
	unsigned int address;
	unsigned int localSize;
	char *id;
} UserFunc;

Instruction *instructions;
unsigned int currInstr;
unsigned int totalInstr;

double *numConsts;
unsigned int totalNumConsts;
char **stringConsts;
unsigned int totalStringConsts;
UserFunc *userFuncs;
unsigned int totalUserFuncs;
char **namedLibfuncs;
unsigned int totalNamedLibfuncs;

unsigned int totalGlobalVars;

/* Returns a constant number function form the constant numbers array, given an index */
double consts_getnumber(unsigned int index);

/* Returns a constant string form the constants string array, given an index */
char *consts_getstring(unsigned int index);

/* Returns a user function form the user functions array, given an index */
UserFunc userfuncs_getfunc(unsigned int index);

/* Returns a library function form the library functions array, given an index */
char *libfuncs_getused(unsigned int index);

/* Initializes the constant numbers array */
void numberconsts_init();

/* Initializes the constant strings array */
void stringconsts_init();

/* Initializes the user functions array */
void userfuncs_init();

/* Initializes the library functions array */
void libfuncs_init();

/* Initializes the instructions array */
void instr_init();

/* Returns a new empty instruction */
Instruction new_instr();

/* Stores an isntruction in the instruction array */
void emit_instr(Instruction t);

#endif