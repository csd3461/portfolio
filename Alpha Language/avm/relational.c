#include "relational.h"

#define RTEST(x) fprintf(stderr, "RELATIONAL(%d)\n", x);

extern void runtime_error(char *providedMessage, ...);

extern char *typeStrings[];

tobool_func_t toboolFuncs[] = {
	number_tobool,
	string_tobool,
	bool_tobool,
	table_tobool,
	userfunc_tobool,
	libfunc_tobool,
	nil_tobool,
	NULL // undef (can't be called)
};

cmp_func_t cmpFuncs[] = {
	cmp_numbers,
	cmp_strings,
	0, // bool
	cmp_tables,
	cmp_userfuncs,
	cmp_libfuncs
};

relational_func_t relationalFuncs[] = {
	jeq_impl,
	jne_impl,
	jle_impl,
	jge_impl,
	jlt_impl,
	jgt_impl
};

/* Returns the boolean represantation of a number */
unsigned char number_tobool(AVM_Memcell *m) {
	
	return m->data.numVal != 0;
	
}

/* Returns the boolean represantation of a string */
unsigned char string_tobool(AVM_Memcell *m) {
	
	return m->data.strVal[0] != '\0';
	
}

/* Returns the boolean represantation of a boolean */
unsigned char bool_tobool(AVM_Memcell *m) {
	
	return m->data.boolVal;
	
}

/* Returns the boolean represantation of a table */
unsigned char table_tobool(AVM_Memcell *m) {
	
	return 1;
	
}

/* Returns the boolean represantation of a user function */
unsigned char userfunc_tobool(AVM_Memcell *m) {
	
	return 1;
	
}

/* Returns the boolean represantation of a library function */
unsigned char libfunc_tobool(AVM_Memcell *m) {
	
	return 1;
	
}

/* Returns the boolean represantation of nil */
unsigned char nil_tobool(AVM_Memcell *m) {
	
	return 0;
	
}

/* Returns the boolean represantation of a variable */
unsigned char avm_tobool(AVM_Memcell *m) {
	
	assert(m->type >= 0 && m->type < undef_m);
	
	return (*toboolFuncs[m->type])(m);
	
}

/* Compares two numbers */
unsigned char cmp_numbers(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return m1->data.numVal == m2->data.numVal;
	
}

/* Compares two strings */
unsigned char cmp_strings(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return strcmp(m1->data.strVal, m2->data.strVal) == 0;
	
}

/* Compares two bools */
unsigned char cmp_bools(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return m1->data.boolVal == m2->data.boolVal;
	
}

/* Compares two tables */
unsigned char cmp_tables(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return m1->data.tableVal == m2->data.tableVal;
	
}

/* Compares two user functions */
unsigned char cmp_userfuncs(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return m1->data.funcVal == m2->data.funcVal;
	
}

/* Compares two library functions */
unsigned char cmp_libfuncs(AVM_Memcell *m1, AVM_Memcell *m2) {
	
	return strcmp(m1->data.libfuncVal, m2->data.libfuncVal) == 0;
	
}

/* Performs the jeq operation */
unsigned char jeq_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {
	
	unsigned char result = 0;
	
	if(rv1->type == undef_m || rv2->type == undef_m) {
		runtime_error("undefined variable involved in equality");
	}else if(rv1->type == nil_m || rv2->type == nil_m) {
		result = rv1->type == nil_m && rv2->type == nil_m;
	} else if(rv1->type == bool_m || rv2->type == bool_m) {
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	} else if(rv1->type != rv2->type) {
		runtime_error("illegal coparison: %s == %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	} else {
		result = (*cmpFuncs[rv1->type])(rv1, rv2) == 1;
	}
	
	return !executionFinished && result;
	
}

/* Performs the jne operation */
unsigned char jne_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {
	
	unsigned char result = 0;
	
	if(rv1->type == undef_m || rv2->type == undef_m) {
		runtime_error("undefined variable involved in equality");
	}else if(rv1->type == nil_m || rv2->type == nil_m) {
		result = rv1->type != nil_m || rv2->type != nil_m;
	} else if(rv1->type == bool_m || rv2->type == bool_m) {
		result = (avm_tobool(rv1) != avm_tobool(rv2));
	} else if(rv1->type != rv2->type) {
		runtime_error("illegal coparison: %s != %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	} else {
		result = (*cmpFuncs[rv1->type])(rv1, rv2)  == 0;
	}
	
	return !executionFinished && result;
	
}

/* Performs the jle operation */
unsigned char jle_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {
	
	if(rv1->type != number_m || rv2->type != number_m) {
		runtime_error("illegal coparison: %s <= %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	}

	return !executionFinished && (rv1->data.numVal <= rv2->data.numVal);
	
}

/* Performs the jge operation */
unsigned char jge_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {
	
	if(rv1->type != number_m || rv2->type != number_m) {
		runtime_error("illegal coparison: %s >= %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	}
	
	return !executionFinished && (rv1->data.numVal >= rv2->data.numVal);
	
}

/* Performs the jlt operation */
unsigned char jlt_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {

	if(rv1->type != number_m || rv2->type != number_m) {
		runtime_error("illegal coparison: %s < %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	}

	return !executionFinished && (rv1->data.numVal < rv2->data.numVal);
	
}

/* Performs the jgt operation */
unsigned char jgt_impl(AVM_Memcell *rv1, AVM_Memcell *rv2) {
	
	if(rv1->type != number_m || rv2->type != number_m) {
		runtime_error("illegal coparison: %s > %s", typeStrings[rv1->type], typeStrings[rv2->type]);
	}

	return !executionFinished && (rv1->data.numVal > rv2->data.numVal);
	
}

/* Performs a relational operation */
void execute_relational(Instruction *instr) {
	
	assert(instr->result.type == label_a);
	
	AVM_Memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	AVM_Memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);
	
	assert(rv1 && rv2);

	if((*relationalFuncs[instr->opcode - jeq_v])(rv1, rv2) == 1) pc = instr->result.val;

	return;
	
}