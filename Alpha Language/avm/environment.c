#include "environment.h"

#include <limits.h>

#define ETEST(x) fprintf(stderr, "ENVIRONMENT(%d)\n", x);

extern void runtime_error(char *providedMessage, ...);

extern unsigned int number_hash(double value);

extern void execute_assign(Instruction *instr);
extern void execute_arithmetic(Instruction *instr);
extern void execute_relational(Instruction *instr);
extern void execute_call(Instruction *instr);
extern void execute_pusharg(Instruction *instr);
extern void execute_funcenter(Instruction *instr);
extern void execute_funcexit(Instruction *instr);
extern void execute_newtable(Instruction *instr);
extern void execute_tablegetelem(Instruction *instr);
extern void execute_tablesetelem(Instruction *instr);

extern void avm_tabledecrefcounter(AVM_Table *t);

execute_func_t executeFuncs[] = {
	execute_assign,
	execute_add, 
	execute_sub, 
	execute_mul, 
	execute_div, 
	execute_mod,
	execute_jeq, 
	execute_jne, 
	execute_jle,
	execute_jge, 
	execute_jlt, 
	execute_jgt,
	execute_call, 
	execute_pusharg,
	execute_funcenter, 
	execute_funcexit,
	execute_newtable, 
	execute_tablegetelem, 
	execute_tablesetelem, 
	execute_jump,
	execute_nop
};

memclear_func_t memclearFuncs[] = {
	0, // number
	memclear_string,
	0, // bool,
	memclear_table,
	0, // userfunc
	0, // libfunc
	0, // nil
	0  // undef
};

char *typeStrings[] = {
	"number",
	"string",
	"bool",
	"table",
	"user function",
	"library function",
	"nil",
	"undefined"
};

tostring_func_t tostringFuncs[] = {
	number_tostring,
	string_tostring,
	bool_tostring,
	table_tostring,
	userfunc_tostring,
	libfunc_tostring,
	nil_tostring,
	undef_tostring
};

/* Initializes the AVM */
void avm_init() {

	avm_stack_init();
	environment_init();

	return;

}

/* Initializes the AVM stack */
void avm_stack_init() {

	int i = 0;

	topsp = AVM_STACKSIZE - 1 - totalGlobalVars;
	top = AVM_STACKSIZE - 1 - totalGlobalVars;
	stack = malloc(sizeof(AVM_Memcell) * AVM_STACKSIZE);

	for(i = 0;i < AVM_STACKSIZE;i++) {
		AVM_WIPEOUT(stack[i]);
		stack[i].type = undef_m;
	}

	return;

}

/* Initializes the environment variables */
void environment_init() {

	AVM_WIPEOUT(ax);
	ax.type = undef_m;
	AVM_WIPEOUT(bx);
	bx.type = undef_m;
	AVM_WIPEOUT(cx);
	cx.type = undef_m;
	AVM_WIPEOUT(retval);
	retval.type = undef_m;
	
	executionFinished = 0;
	pc = 0;
	currLine = 0;
	totalActuals = 0;
	table_recursion_detected = 0;
	table_recursion_counter = 0;
	
	return;
	
}

/* Executes a cycle(instruction) */
void execute_cycle() {
	
	if(executionFinished) {
		return;
	} else if(pc == AVM_ENDING_PC) {
		executionFinished = 1;
		return;
	} else {
		assert(pc < AVM_ENDING_PC);
		Instruction *instr = instructions + pc;
		assert(instr->opcode >= 0 && instr->opcode <= AVM_MAX_INSTRUCTIONS);
		
		if(instr->srcLine) currLine = instr->srcLine;
		
		unsigned int oldPC = pc;
		(*executeFuncs[instr->opcode])(instr);
		
		if(pc == oldPC) pc++;
	}
	
	return;
	
}


/* Translates a virual machine argument */
AVM_Memcell *avm_translate_operand(VMarg *arg, AVM_Memcell *reg) {
	
	switch(arg->type) {
		case global_a:
			return &stack[AVM_STACKSIZE - 1 - arg->val];
		case local_a:	
			return &stack[topsp - arg->val];
		case formal_a:
			return &stack[topsp + AVM_STACKENV_SIZE + 1 + arg->val];
		case retval_a:
			return &retval;
		case number_a:
			reg->type = number_m;
			reg->data.numVal = consts_getnumber(arg->val);
			return reg;
		case string_a:
			reg->type = string_m;
			reg->data.strVal = strdup(consts_getstring(arg->val));
			return reg;
		case bool_a:
			reg->type = bool_m;
			reg->data.boolVal = arg->val;
			return reg;
		case nil_a:
			reg->type = nil_m;
			return reg;
		case userfunc_a:
			reg->type = userfunc_m;
			reg->data.funcVal = userfuncs_getfunc(arg->val).address;
			return reg;
		case libfunc_a:
			reg->type = libfunc_m;
			reg->data.libfuncVal = libfuncs_getused(arg->val);
			return reg;
		default:
			assert(0);
	}

	return NULL;
	
}


/* Converts a number to a string */
char *number_tostring(AVM_Memcell *m) {

	char *tmp = NULL;
	char *format = NULL;
	int digits = 6;
	char *str = malloc(sizeof(char) * digits);

	if(roundf(m->data.numVal) == m->data.numVal) format = "%.0f";
	else format = "%.3f";

	while(snprintf(str, digits, format, m->data.numVal) >= digits) {
		digits = digits * 2;
		tmp = realloc(str, sizeof(char) * digits);
		if(tmp != NULL) {
			str = tmp;	
		}else {
			fprintf(stderr, "Failed to allocate memory\n");
			exit(EXIT_FAILURE);
		}
	}
	
	return str;
	
}

/* Converts a string to a string */
char *string_tostring(AVM_Memcell *m) {
	
	return strdup(m->data.strVal);
	
}

/* Converts a bool to a string */
char *bool_tostring(AVM_Memcell *m) {
	
	if(m->data.boolVal == 1) return strdup("true");
	else return strdup("false");
	
}

/* Merges two strings */
char *strmerge(char *buf1, char *buf2, unsigned int *max_size) {

	unsigned int size1 = strlen(buf1);
	unsigned int size2 = strlen(buf2);
	char *tmp = NULL;

	if(size1 + size2 >= *max_size) {
		*max_size = (size1 + size2) * 2;
		tmp = realloc(buf1, *max_size);
		if(tmp != NULL) {
			buf1 = tmp;		
		}else {
			fprintf(stderr, "Failed to allocate memory\n");
			exit(EXIT_FAILURE);	
		}
	}

	buf1 = strcat(buf1, buf2);

	return buf1;

}

/* Converts the indexed elements of a table to a string */
char *strmerge_indexed(AVM_TableBucket *b[], unsigned int totalIndexed, unsigned int *count, 
								unsigned int all, char *str, unsigned int *current_size) {

	unsigned char i = 0;
	unsigned int c = 0;
	char *key = NULL;
	char *value = NULL;
	AVM_TableBucket *tmp = NULL;

	while(i < AVM_TABLE_HASHSIZE && c < totalIndexed) {
		tmp = b[i];
		while(tmp != NULL) {
			str = strmerge(str, "{ ", current_size);
			key = avm_tostring(&tmp->key);
			str = strmerge(str, key, current_size);
			value = avm_tostring(&tmp->value);
			str = strmerge(str, " : ", current_size);
			str = strmerge(str, value, current_size);
			str = strmerge(str, " }", current_size);
			if(*count < all - 1) str = strmerge(str, ", ", current_size);
			free(key);
			free(value);
			(*count)++;
			c++;
			tmp = tmp->next;
		}
		i++;
	}

	return str;

}

/* Converts a table to a string */
char *table_tostring(AVM_Memcell *m) { 

	if(table_recursion_counter++ > MAX_TABLE_RECURSIONS) {
		table_recursion_detected = 1;
		return strdup("");
	}

	unsigned int count = 0;
	unsigned int current_size = 1024 * sizeof(char);
	char *str = malloc(current_size);
	AVM_Table *t = m->data.tableVal;
	unsigned int total = t->totalN + t->totalS + t->totalB + t->totalT + t->totalU + t->totalL;

	str[0] = '[';
	str[1] = ' ';
	str[2] = '\0';

	str = strmerge_indexed(t->numIndexed, t->totalN, &count, total, str, &current_size);
	str = strmerge_indexed(t->strIndexed, t->totalS, &count, total, str, &current_size);
	str = strmerge_indexed(t->boolIndexed, t->totalB, &count, total, str, &current_size);
	str = strmerge_indexed(t->tableIndexed, t->totalT, &count, total, str, &current_size);
	str = strmerge_indexed(t->userfuncIndexed, t->totalU, &count, total, str, &current_size);
	str = strmerge_indexed(t->libfuncIndexed, t->totalL, &count, total, str, &current_size);

	str = strmerge(str, " ]", &current_size);

	table_recursion_counter--;
	
	return str;
			
}

/* Converts a userfunc to a string */
char *userfunc_tostring(AVM_Memcell *m) {
	
	unsigned int len = strlen("user function()") + 10 + 1;
	char *str = malloc(sizeof(char) * len);
	
	sprintf(str, "user function(%d)", m->data.funcVal);

	return str;
	
}

/* Converts a libfunc to a string */
char *libfunc_tostring(AVM_Memcell *m) {
	
	unsigned int len = strlen("library function()") + strlen(m->data.libfuncVal) + 1;
	char *str = malloc(sizeof(char) * len);
	
	sprintf(str, "library function(%s)", m->data.libfuncVal);

	return str;
			
}

/* Converts a nil to a string */
char *nil_tostring(AVM_Memcell *m) {
	
	return strdup("nil");
	
}

/* Converts an undef to a string */
char *undef_tostring(AVM_Memcell *m) {
	
	return strdup("undefined");
	
}

/* Returns the string represantation of a variable */ /* TODO */
char *avm_tostring(AVM_Memcell *m) {
	
	assert(m->type >= 0 && m->type <= undef_m);

	return (*tostringFuncs[m->type])(m);
	
}

/* Returns an environment value from the stack */
unsigned int avm_get_envvalue(unsigned int i) { 

	assert(stack[i].type == number_m);
	unsigned int val = (unsigned int)stack[i].data.numVal;
	assert(stack[i].data.numVal == ((double)val));
	
	return val;

}

/* Returns the number of total actuals */
unsigned int avm_totalactuals() {
	
	return avm_get_envvalue(topsp + AVM_NUMACTUALS_OFFSET);
	
}

/* Returns the ith actual */
AVM_Memcell *avm_getactual(unsigned int i) {
	
	assert(i < avm_totalactuals());
	
	return &stack[topsp + AVM_STACKENV_SIZE + 1 + i];
	
}

/* Decreases the top of the stack */
void avm_dec_top(void) {

	if(!top) {
		runtime_error("stack overflow");
	}else {
		--top;
	}

}

/* Stores a value in the stack */
void avm_push_envalue(unsigned int val) {

	stack[top].type = number_m;
	stack[top].data.numVal = val;
	avm_dec_top();

	return;

}

/* Saves the current environment variables */
void avm_callsaveenvironment() {

	avm_push_envalue(totalActuals);
	avm_push_envalue(pc + 1);
	avm_push_envalue(top + totalActuals + 2);
	avm_push_envalue(topsp);

	return;

}

/* Clears a string */
void memclear_string(AVM_Memcell *m) {

	assert(m->data.strVal);
	free(m->data.strVal);

	return;

}

/* Clears a table */
void memclear_table(AVM_Memcell *m) {

	assert(m->data.tableVal);
	avm_tabledecrefcounter(m->data.tableVal);

	return;

}

/* Clears a memory cell */
void avm_memcellclear(AVM_Memcell *m) {

	if(m->type != undef_m) {
		memclear_func_t f = memclearFuncs[m->type];
		if(f) (*f)(m);
		m->type = undef_m;
	}

	return;

}

/* Executes a jump instruction */
void execute_jump(Instruction *instr) {

	assert(instr->result.type == label_a);
	pc = instr->result.val;
	
	return;

}

/* Executes a nop instruction */
void execute_nop() {

	return;

}