#include "function.h"

#define FTEST(x) fprintf(stderr, "FUNCTION(%d)\n", x);

extern void runtime_error(char *providedMessage, ...);

extern void avm_assign(AVM_Memcell *lv, AVM_Memcell *rv);

extern char *typeStrings[];

/* Returns a user function that resides to the given address */
UserFunc *avm_getfuncinfo(unsigned int address) { 

	int i =0;
	for(i = 0; i < totalUserFuncs; i++) {
		if(userFuncs[i].address == address) return &userFuncs[i];
	}

	assert(0);

}

/* Executes a call operation */
void execute_call(Instruction *instr) {

	AVM_Memcell *func = avm_translate_operand(&instr->result, &ax);
	assert(func);
	avm_callsaveenvironment();

	switch(func->type) {
		case userfunc_m :
			pc = func->data.funcVal;
			assert(pc < AVM_ENDING_PC);
			assert(instructions[pc].opcode == funcenter_v);
			break;
		case string_m :
			avm_calllibfunc(func->data.strVal);
			break;
		case libfunc_m :
			avm_calllibfunc(func->data.libfuncVal);
			break;
		default :
			runtime_error("call : cannot bind %s to function", typeStrings[func->type]);
	}

	return;

}

/* Executes a pusharg operation */
void execute_pusharg(Instruction *instr) {

	AVM_Memcell *arg = avm_translate_operand(&instr->result, &ax);
	assert(arg);

	avm_assign(&stack[top], arg);
	++totalActuals;
	avm_dec_top();

	return;

}

/* Executes a funcenter operation */
void execute_funcenter(Instruction *instr) {

	AVM_Memcell *func = avm_translate_operand(&instr->result, &ax);
	assert(func);
	assert(pc == func->data.funcVal);

	totalActuals = 0;
	UserFunc *funcInfo = avm_getfuncinfo(pc);
	topsp = top;
	top = top - funcInfo->localSize;

	return;

}

/* Executes a funcexit operation */
void execute_funcexit(Instruction *unused) {

	unsigned int oldTop = top;

	top = avm_get_envvalue(topsp + AVM_SAVEDTOP_OFFSET);
	pc = avm_get_envvalue(topsp + AVM_SAVEDPC_OFFSET);
	topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);

	while(++oldTop <= top) avm_memcellclear(&stack[oldTop]);

	return;

}

/* Performs a call to a library function corresponding to the given id */
void avm_calllibfunc(char *id) {

	lib_func_t f = avm_getlibfunc(id);
	if(!f) {
		runtime_error("unsupported library function called: %s", id);
	}else {
		topsp = top;
		totalActuals = 0;
		(*f)();
		if(!executionFinished) execute_funcexit(NULL);
	}

	return;

}