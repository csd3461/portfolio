#include "libfunc.h"

#define LTEST(x) fprintf(stderr, "LIBFUNC(%d)\n", x);

extern void runtime_error(char *providedMessage, ...);
extern void avm_assign(AVM_Memcell *lv, AVM_Memcell *rv);

extern char *typeStrings[];

char *libFuncNames[] = {
	"print",
	"input",
	"objectmemberkeys",
	"objecttotalmembers",
	"objectcopy",
	"totalarguments",
	"argument",
	"typeof",
	"strtonum",
	"sqrt",
	"cos",
	"sin"
};

lib_func_t libFuncs[] = {
	libfunc_print,
	libfunc_input,
	libfunc_objectmemberkeys,
	libfunc_objecttotalmembers,
	libfunc_objectcopy,
	libfunc_totalarguments,
	libfunc_argument,
	libfunc_typeof,
	libfunc_strtonum,
	libfunc_sqrt,
	libfunc_cos,
	libfunc_sin
};

/* Stores the keys of a table to another table */
void copy_keys(AVM_Table *table, AVM_TableBucket *indices[], unsigned int totalIndexed) {

	int i = 0;
	unsigned int count = 0;
	AVM_TableBucket *b = NULL;
	while(i < AVM_TABLE_HASHSIZE && count < totalIndexed) {
		b = indices[i];
		while(b != NULL) {
			ax.type = number_m;
			ax.data.numVal = count;
			avm_tablesetelem(table, &ax, &b->key);
			b = b->next;
			count++;
		}
		i++;
	}

	return;

}

/* Stores the buckets of a table in another table */
void copy_buckets(AVM_Table *table, AVM_TableBucket *indices[], unsigned int totalIndexed) {

	int i = 0;
	unsigned int count = 0;
	AVM_TableBucket *b = NULL;
	while(i < AVM_TABLE_HASHSIZE && count < totalIndexed) {
		b = indices[i];
		while(b != NULL) {
			avm_tablesetelem(table, &b->key, &b->value);
			b = b->next;
			count++;
		}
		i++;
	}

	return;

}

/* Returns the representative number of a library function */
unsigned char get_libfunc_num(char *id) {

	int i = 0;

	for(i = 0;i < TOTAL_LIBFUNCS; i++) {
		if(strcmp(id, libFuncNames[i]) == 0) return i;
	}

	assert(0);

}

/* Returns a library function corresponding to the given id */
lib_func_t avm_getlibfunc(char *id) { 

	int i = 0;

	for(i = 0;i < TOTAL_LIBFUNCS; i++) {
		if(strcmp(id, libFuncNames[i]) == 0) return libFuncs[i];
	}

	assert(0);
	
}

/* Implements the print library function */
void libfunc_print() {
	
	unsigned int n = avm_totalactuals();
	int i = 0;
	char *s = NULL;

	for(i = 0; i < n; i++) {
		s = avm_tostring(avm_getactual(i));
		if(table_recursion_detected) {
			runtime_error("circular reference detected while printing object");
			break;
		}
		printf("%s", s);
		free(s);
	}

	// I still drink your milkshake
	avm_memcellclear(&retval);
	retval.type = nil_m;
	
	return;
	
}

/* Implements the input library function */
void libfunc_input() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 0) {
		runtime_error("number of arguments (%d) differs from expected (0) in 'input'", n);
	} else {
		avm_memcellclear(&retval);
		
		char *str = NULL;
		size_t len = 0;
		getline(&str, &len, stdin);
		str[strlen(str) - 1] = '\0';
		
		if(!strcmp(str, "true") || !strcmp(str, "false")) {
			retval.type = bool_m;
			retval.data.boolVal = !strcmp(str, "true")?1:0;
		} else if(!strcmp(str, "nil")) {
			retval.type = nil_m;
		} else {
			char *tmp;
			double ret;
			ret = strtod(str, &tmp);
			
			if(tmp != NULL && strlen(tmp) == 0 && strlen(str) > 0) {
				retval.type = number_m;
				retval.data.numVal = ret;
			} else {
				retval.type = string_m;
				retval.data.strVal = strdup(str);
			}
			
		}
		free(str);
	}

	return;
	
}

/* Implements the objectmemberkeys library function */
void libfunc_objectmemberkeys() {

	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'objectmemberkeys'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		if(arg->type != table_m) {
			runtime_error("table argument was expected in 'objectmemberkeys', not: %s", typeStrings[arg->type]);
			return;
		}
		
		avm_memcellclear(&retval);

		AVM_Table *t = arg->data.tableVal;

		retval.type = table_m;
		retval.data.tableVal = avm_tablenew();
		avm_tableincrefcounter(retval.data.tableVal);

		copy_keys(retval.data.tableVal, t->numIndexed, t->totalN);
		copy_keys(retval.data.tableVal, t->strIndexed, t->totalS);
		copy_keys(retval.data.tableVal, t->boolIndexed, t->totalB);
		copy_keys(retval.data.tableVal, t->tableIndexed, t->totalT);
		copy_keys(retval.data.tableVal, t->userfuncIndexed, t->totalU);
		copy_keys(retval.data.tableVal, t->libfuncIndexed, t->totalL);
	}

	return;

}

/* Implements the tablelength library function */
void libfunc_objecttotalmembers() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'tablelength'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		if(arg->type != table_m) {
			runtime_error("table argument was expected in 'objecttotalmembers', not: %s", typeStrings[arg->type]);
			return;
		}

		avm_memcellclear(&retval);

		AVM_Table *t = arg->data.tableVal;
			
		retval.type = number_m;
		retval.data.numVal = t->totalN + t->totalS + t->totalB + t->totalT + t->totalU + t->totalL;
		
	}
	
	return;
	
}

/* Implements the objectcopy library function */
void libfunc_objectcopy() {

	unsigned int n = avm_totalactuals();

	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'tablelength'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		if(arg->type != table_m) {
			runtime_error("table argument was expected in 'objectcopy', not: %s", typeStrings[arg->type]);
			return;
		}

		avm_memcellclear(&retval);

		AVM_Table *t = arg->data.tableVal;
			
		retval.type = table_m;
		retval.data.tableVal = avm_tablenew();
		avm_tableincrefcounter(retval.data.tableVal);

		copy_buckets(retval.data.tableVal, t->numIndexed, t->totalN);
		copy_buckets(retval.data.tableVal, t->strIndexed, t->totalS);
		copy_buckets(retval.data.tableVal, t->boolIndexed, t->totalB);
		copy_buckets(retval.data.tableVal, t->tableIndexed, t->totalT);
		copy_buckets(retval.data.tableVal, t->userfuncIndexed, t->totalU);
		copy_buckets(retval.data.tableVal, t->libfuncIndexed, t->totalL);
	}

	return;

}

/* Implements the totalarguments library function */
void libfunc_totalarguments() {
	
	unsigned int p_topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);
	avm_memcellclear(&retval);
	
	if(!p_topsp) {
		retval.type = nil_m;
	} else {
		retval.type = number_m;
		retval.data.numVal = avm_get_envvalue(p_topsp + AVM_NUMACTUALS_OFFSET);
	}
	
	return;
	
}

/* Implements the arguments library function */
void libfunc_argument() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'argument'", n);
	} else {
		unsigned int p_topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);
		unsigned int args = avm_get_envvalue(p_topsp + AVM_NUMACTUALS_OFFSET);
		avm_memcellclear(&retval);
		
		if(args == 0) {
			retval.type = nil_m;
			return;
		}
		
		AVM_Memcell *index = avm_getactual(0);
		
		if(index->type != number_m) {
			runtime_error("number argument was expected in 'argument', not: %s", typeStrings[index->type]);
		}
		
		if(index->data.numVal < 0 || index->data.numVal > args) {
			runtime_error("index out of bounds");
		}
		
		avm_assign(&retval, &stack[(int)index->data.numVal + p_topsp + AVM_NUMACTUALS_OFFSET + 1]);
	}
	
	return;
	
}

/* Implements the typeof library function */
void libfunc_typeof() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) { 
		runtime_error("number of arguments (%d) differs from expected (1) in 'typeof'", n);
	} else {
		avm_memcellclear(&retval);
		retval.type = string_m;
		retval.data.strVal = strdup(typeStrings[avm_getactual(0)->type]);
	}
	
	return;
	
}

/* Implements the strtonum library function */
void libfunc_strtonum() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'strtonum'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		avm_memcellclear(&retval);
		
		if(arg->type != string_m) {
			retval.type = nil_m;
			return;
		}
		
		char *tmp;
		double ret;
		ret = strtod(arg->data.strVal, &tmp);
		
		if(tmp != NULL && strlen(tmp) == 0 && strlen(arg->data.strVal) > 0) {
			retval.type = number_m;
			retval.data.numVal = ret;
		} else {
			retval.type = nil_m;
		}
	}
	
	return;
	
}


/* Implements the sqrt library function */
void libfunc_sqrt() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'sqrt'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		avm_memcellclear(&retval);
		
		if(arg->type != number_m || arg->data.numVal < 0) {
			retval.type = nil_m;
			return;
		}
			
		retval.type = number_m;
		retval.data.numVal = sqrt((arg->data.numVal));
	}
	
	return;
	
}

/* Implements the cos library function */
void libfunc_cos() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'cos'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		avm_memcellclear(&retval);
		
		if(arg->type != number_m) {
			retval.type = nil_m;
			return;
		}
			
		retval.type = number_m;
		retval.data.numVal = cos((arg->data.numVal));
	}
	
	return;
	
}

/* Implements the sin library function */
void libfunc_sin() {
	
	unsigned int n = avm_totalactuals();
	
	if(n != 1) {
		runtime_error("number of arguments (%d) differs from expected (1) in 'sin'", n);
	} else {
		AVM_Memcell *arg = avm_getactual(0);
		avm_memcellclear(&retval);
		
		if(arg->type != number_m) {
			retval.type = nil_m;
			return;
		}
			
		retval.type = number_m;
		retval.data.numVal = sin((arg->data.numVal));
	}
	
	return;
	
}