#ifndef TABLE_H_
#define TABLE_H_

#include "assign.h"
#include "function.h"
#include "relational.h"
#include "environment.h"

/* Prints a missing element warning */
void missing_elem_warning(char *ts, char *is);

/* Alpha hash function implementation for pointers */
unsigned int pointer_hash(void *ptr);

/* Alpha hash function implementation for numbers */
unsigned int number_hash(double value);

/* Alpha hash function implementation for strings */
unsigned int string_hash(char *str);

/* Increases table's reference counter */
void avm_tableincrefcounter(AVM_Table *t);

/* Decreases table's reference counter */
void avm_tabledecrefcounter(AVM_Table *t);

/* Initializes the table buckets of a hash table */
void avm_tablebucketsinit(AVM_TableBucket **p);

/* Destroys a table bucket */
void avm_tablebucketdestroy(AVM_TableBucket **p);

/* Creates a new table */
AVM_Table *avm_tablenew();

/* Destroys the table */
void avm_tabledestroy(AVM_Table *t);

/* Executes a newtable operation */
void execute_newtable(Instruction *instr);

/* Executes a tablegetelem  operation */
void execute_tablegetelem(Instruction *instr);

/* Executes a tablesetelem operation */
void execute_tablesetelem(Instruction *instr);

/* Performs the tablegetelem operation */
AVM_Memcell *avm_tablegetelem(AVM_Table *table, AVM_Memcell *index);

/* Performs the tablesetelem operation */
void avm_tablesetelem(AVM_Table *table, AVM_Memcell *index, AVM_Memcell *content);

/* Returns the value of a table bucket */
AVM_Memcell *avm_table_getvalue(AVM_TableBucket *b, AVM_Memcell *key);

/* Returns a new table bucket element */
AVM_TableBucket *new_elem(AVM_Memcell *key, AVM_Memcell *value);

/* Stores an elememnt in a table bucket */
signed char avm_table_setvalue(AVM_TableBucket **b, AVM_Memcell *key, AVM_Memcell *value);

#endif