#ifndef DECODER_H_
#define DECODER_H_

#include "alpha.h"

#define MAGIC_NUMBER 27052017

/* Prints a virtual machine argument */
void print_vmarg(FILE *file, VMarg arg);

/* Reads a virtual machine argument from a binary file */
VMarg read_binary_vmarg(FILE *bfile);

/* Converts a file from binary to text */
void binary_reader(const char *vpath, const char *bpath);

#endif
