function send_friend_request(from, to, object) {
	$.get("send_friend_request.php?sender=" + from + "&recipient=" + to, function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		$(object).text("cancel");
		$(object).attr("onclick","cancel_friend_request('" + from + "', '" + to + "', this);");
		
    });
}

function cancel_friend_request(from, to, object) {
	$.get("cancel_friend_request.php?sender=" + from + "&recipient=" + to, function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		$(object).text("person_add");
		$(object).attr("onclick","send_friend_request('" + from + "', '" + to + "', this);");
    });
}

function cancel_friend_request_from_list(from, to, object) {
	$.get("cancel_friend_request.php?sender=" + from + "&recipient=" + to, function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		location.reload();
    });
}

function accept_friend_request(from, to) {
	$.get("accept_friend_request.php?sender=" + from + "&recipient=" + to, function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		location.reload();
    });
}

function open_message_window(from, to_email, to_username, object) {
	get_messages(from, to_email);
	if($("#message_window").is(":visible") && $("#friend").text() !== to_username) {
		$("#new_message").val("");
	} else {
		$("#message_window").show();
	}
	
	$("#send_message").attr("onclick","send_message('" + from + "', '" + to_email + "');");
	$("#friend").text(to_username);
	$(object).attr("onclick","close_message_window('" + from + "', '" + to_email + "', '" + to_username + "', this);");
	
}

function close_message_window(from, to_email, to_username, object) {
	if($("#message_window").is(":visible") && $("#friend").text() !== to_username) {
		open_message_window(from, to_email, to_username, object);
	} else if(!$("#message_window").is(":visible")) {
		open_message_window(from, to_email, to_username, object);
	} else {
		$("#message_window").hide();
		$(object).attr("onclick","open_message_window('" + from + "', '" + to_email + "', '" + to_username + "', this);");
	}
}

function send_message(from, to) {
	$.get("send_message.php?sender=" + from + "&recipient=" + to + "&message=" + $("#new_message").val().toString(), function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		
    });
	$("#new_message").val("");
}

function get_messages(from, to) {
	var d = new Date();
    var n = d.getTime();
    //setInterval(function(){alert( (n = d.getTime()) + " Hello"); }, 3000);
	//setInterval(function(){alert( (n = d.getTime()) + " Hello"); }, 5000);
	//serInterval(function(){
		$.get("get_messages.php?sender=" + from + "&recipient=" + to, function(data, status){
		if(data === "OK") {
			// ...CODE...
		} else {
			// ...CODE...
		}
		$("#messages").html(data);
    });
	//}, 5000);
}

/*
function press_enter(from, to, object) {
	$(object).keyup(function(e){
    if(e.keyCode === 13) {
        send_message(from, to);
    }
	
});
}
*/