function login_enable() {
	$("#login").show();
	$("#register").hide();
	$("#login_active").addClass("active");
	$("#register_active").removeClass("active");
}

function register_enable() {
	$("#register").show();
	$("#login").hide();
	$("#register_active").addClass("active");
	$("#login_active").removeClass("active");
}