<!DOCTYPE html>
<?php session_start(); ?>

<?php 
	if(!isset($_SESSION["email"])) {
?>
	<html>
		<head>
			<title>403 Forbidden</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="stylesheet" href="style.css" />
		</head>
		<body>
			<h1> Forbidden </h1>
			<p> You don't have permission to access /img/ on this server. </p>
		</body>
	</html>
<?php } else { ?>
	
	<html>
		<head>
			<title>Talkbook | Requests</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<link rel="shortcut icon" href="http://www.foxsports.com/content/dam/fsdigital/fscom/global/dev/static_resources/mlb/teams/retina/30.png"> 
			<link rel="stylesheet" href="style.css" />
		
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="javascript/person_event.js"></script>
			
		</head>
		
		<body>
			
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
					  </button>
					  <a style="pointer-events: none;" class="navbar-brand" href="#">Talkbook</a>
					</div>

					<div class="collapse navbar-collapse" id="myNavbar">
						  <ul class="nav navbar-nav">
							  <li><a href="home.php">Home</a></li>
							  <li><a href="index.html">Log out</a></li>
							  <li><a href="find_people.php">Find People</a></li>
						  </ul>

						  <ul class="nav navbar-nav navbar-right">
							  <li class="dropdown">
								  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Events<span class="caret"></span></a>
								  <ul class="dropdown-menu">
									  <li class="active"><a style="pointer-events: none;">Requests</a></li>
									  <li><a href="#">Messages</a></li>
									  <li><a href="#">Notices</a></li>
								  </ul>
							  </li>
							  <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
						  </ul>
					</div>
					
				</div>
			</nav>
			
			<?php 
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "Talkbook_db";

				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);

				// Check connection
				if ($conn->connect_error) {
					die("Connection failed: " . $conn->connect_error);
				} 
				$email = $_SESSION['email'];
				
				$sql = "SELECT username, email FROM friend_requests, account WHERE sender = '$email' and recipient = email";
				$result = $conn->query($sql);

				if ($result != null && $result->num_rows > 0) {
			?>
				<center>
					<div style="height: 300px; overflow:auto;">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th colspan="2">Sended Requests</th>
								</tr>

								<tr>
									<th>Username</th>
									<th>Cancel</th>
								</tr>

							</thead>

							<tbody>
								<?php
									// output data of each row
									while($row = $result->fetch_assoc()) {
								?>
									<tr>
										<td><a href="#"><?php echo $row["username"]; ?></a></td>
										<td><a class="material-icons" onclick="cancel_friend_request_from_list('<?php echo $_SESSION["email"]; ?>', '<?php echo $row["email"]; ?>', this);">cancel</a></td>
									<tr>
								<?php } ?>
							</tbody>
						</table>

						<?php
							$sql = "SELECT username, email FROM friend_requests, account WHERE recipient = '$email' and sender = email";
							$result = $conn->query($sql);

							if ($result != null && $result->num_rows > 0) {
						?>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th colspan="3">Recieved Requests</th>
									</tr>

									<tr>
										<th>Username</th>
										<th>Accept</th>
										<th>Refuse</th>
									</tr>

								</thead>

								<tbody>
									<?php
										// output data of each row
										while($row = $result->fetch_assoc()) {
									?>
										<tr>
											<td><a href="#"><?php echo $row["username"]; ?></a></td>
											<td><a class="material-icons" onclick="accept_friend_request('<?php echo $row["email"]; ?>', '<?php echo $_SESSION["email"]; ?>', this);"><i class="fa fa-check"></i></a></td>
											<td><a class="material-icons" onclick="cancel_friend_request_from_list('<?php echo $row["email"]; ?>', '<?php echo $_SESSION["email"]; ?>', this);"><i class="fa fa-remove"></i></a></td>
										<tr>
									<?php } ?>
								</tbody>
							</table>
						<?php } ?>
					</div>
				</center>
			<?php } $conn->close(); ?>
			
		</body>
	</html>
<?php } ?>