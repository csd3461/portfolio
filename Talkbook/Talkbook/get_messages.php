<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Talkbook_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sender = $_GET["sender"];
$recipient = $_GET["recipient"];

$sql = "SELECT message, sender FROM message "
		. "WHERE (sender = '$sender' and recipient = '$recipient') or "
		. "(sender = '$recipient' and recipient = '$sender')";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		if($row["sender"] === $sender) {
			echo ' <p style="word-wrap: break-word; margin-left: 200px; min-width: 200px; max-width: 200px; text-align: right;"><span style="background-color:blue; border-radius:5%;">' . $row["message"] . '</span></p>';
		} else {
			echo ' <p style="word-wrap: break-word; max-width: 200px; text-align: left; background-color: red; border-radius: 5%;">' . $row["message"] . '</p> ';
		}
	}
}
$conn->close();