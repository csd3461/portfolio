<!DOCTYPE html>
<?php session_start(); ?>

<?php 
	if(!isset($_SESSION["email"])) {
?>
	<html>
		<head>
			<title>403 Forbidden</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="shortcut icon" href="http://www.foxsports.com/content/dam/fsdigital/fscom/global/dev/static_resources/mlb/teams/retina/30.png"> 
			<link rel="stylesheet" href="style.css" />
		
		</head>
		<body>
			<h1> Forbidden </h1>
			<p> You don't have permission to access /img/ on this server. </p>
		</body>
	</html>
<?php } else { ?>
	
	<html>
		<head>
			<title>Talkbook | Home</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<link rel="shortcut icon" href="http://www.foxsports.com/content/dam/fsdigital/fscom/global/dev/static_resources/mlb/teams/retina/30.png"> 
			<link rel="stylesheet" href="style.css" />
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="javascript/person_event.js"></script>
			
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  
			<script>
				/**
 * Created by 192.168.3.46 on 4/19/2015.
 */
function htmlbodyHeightUpdate(){
    var height3 = $( window ).height()
    var height1 = $('.nav').height()+50
    height2 = $('.main').height()
    if(height2 > height3){
        $('html').height(Math.max(height1,height3,height2)+10);
        $('body').height(Math.max(height1,height3,height2)+10);
    }
    else
    {
        $('html').height(Math.max(height1,height3,height2));
        $('body').height(Math.max(height1,height3,height2));
    }

}
$(document).ready(function () {
    htmlbodyHeightUpdate()
    $( window ).resize(function() {
        htmlbodyHeightUpdate()
    });
    $( window ).scroll(function() {
        height2 = $('.main').height()
        htmlbodyHeightUpdate()
    });
});
			</script>
		</head>
		
		<body>
			
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>                        
					</button>
					<a style="pointer-events: none;" class="navbar-brand"> Talkbook</a>
				  </div>
					
				  <div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav">
							<li class="active"><a style="pointer-events: none;">Home</a></li>
							<li><a href="index.html">Log out</a></li>
							<li><a href="find_people.php">Find People</a></li>
						</ul>
					
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">Events<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="requests.php">Requests</a></li>
									<li><a href="#">Messages</a></li>
									<li><a href="#">Notices</a></li>
								</ul>
							</li>
							<li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
						</ul>
				  </div>
					
				</div>
			</nav>
			
			<nav class="navbar navbar-inverse sidebar" role="navigation">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a style="pointer-events: none;" class="navbar-brand" href="#">Friends</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
						<ul class="nav navbar-nav"  style="height: 538px; overflow:auto;">
							<?php 
								$servername = "localhost";
								$username = "root";
								$password = "";
								$dbname = "Talkbook_db";

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);

								// Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								} 
								$email = $_SESSION['email'];

								$sql = "SELECT username, email FROM friends, account WHERE (first_person = '$email' and second_person = email) or (second_person = '$email' and first_person = email)";
								$result = $conn->query($sql);

								if ($result->num_rows > 0) {
									while($row = $result->fetch_assoc()) {
							?>
									<li ><a onclick="open_message_window('<?php echo $_SESSION["email"]; ?>', '<?php echo $row["email"]; ?>','<?php echo $row["username"]; ?>', this);"><?php echo $row["username"]?><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
									
								<?php } } ?>
						</ul>
					</div>
				</div>
			</nav>
			
			<div id="message_window" class="chatbox" style="height: 300px;" hidden>
				<p style="background-color:blue; text-align: right;margin: 0 !important;font-size: 20px;"><a onclick="close_messages_tab();">X</a></p>
				<p id="friend" style="background-color:blue; padding:16px;"></p>
				<div id="messages" class="messages" style="height: 300px; overflow:auto;">

				</div>
				<div class="chatbox" style="padding: 16px;">
				<input id="new_message" class="form-control" type="text" style="width:20em; float:left;" placeholder="Type a message">
				<button id="send_message" class="btn btn-primary" style="width:5em;">SEND</button>		</div>
			</div>
			
<div class="main">
    <!-- Content Here -->
</div>
			
		</body>
	</html>
<?php } ?>

	

