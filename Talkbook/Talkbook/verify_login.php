<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$login_email = $_GET["login_email"];
$login_password = $_GET["login_password"];

session_start();
$_SESSION["email"] = $login_email;

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Talkbook_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT email, password FROM account";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if( $row["email"] === $login_email && $row["password"] === $login_password) {
			echo "OK";
		}
    }
} 
$conn->close();