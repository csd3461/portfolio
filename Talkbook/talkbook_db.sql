-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 13 Φεβ 2017 στις 22:26:06
-- Έκδοση διακομιστή: 10.1.9-MariaDB
-- Έκδοση PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `talkbook_db`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `account`
--

CREATE TABLE `account` (
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('unkown','male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `account`
--

INSERT INTO `account` (`fname`, `lname`, `username`, `email`, `password`, `city`, `country`, `birthday`, `gender`) VALUES
('Chris', 'Redfield', 'Chris Redfield', 'chrish_redfield@gmail.com', 'Chris123', 'London', 'United Kingdom', '0000-00-00', 'male'),
('fgfg', 'fgfg', 'dfd', 'dogdog@gmail.com', '12345', 'erer', 'rr', '0000-00-00', 'unkown'),
('', '', 'Helias', 'hellworld@hotmail.com', '', '', '', '0000-00-00', 'unkown'),
('sfdf', 'egef', 'mike', 'nick@gmail.com', 'csd3461', '', '', '0000-00-00', 'male'),
('aaasdad', 'asdfsfa', 'sdfsgafFFA', 'nickamorg0@gmail.com', 'Aa1!12311', 'Heraklion', 'Greece', '0000-00-00', 'unkown'),
('fgr', 'erere', 'dsfaf', 'sdg2@gmail.com', '12345', 'dfdf', 'dfdfd', '0000-00-00', 'unkown');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `friends`
--

CREATE TABLE `friends` (
  `first_person` varchar(40) NOT NULL,
  `second_person` varchar(40) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `friends`
--

INSERT INTO `friends` (`first_person`, `second_person`, `date`) VALUES
('nick@gmail.com', 'hellworld@hotmail.com', '0000-00-00'),
('nickamorg0@gmail.com', 'nick@gmail.com', '0000-00-00');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `friend_requests`
--

CREATE TABLE `friend_requests` (
  `sender` varchar(40) NOT NULL,
  `recipient` varchar(40) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `friend_requests`
--

INSERT INTO `friend_requests` (`sender`, `recipient`, `date`) VALUES
('dogdog@gmail.com', 'nickamorg0@gmail.com', '0000-00-00'),
('dogdog@gmail.com', 'sdg2@gmail.com', '0000-00-00'),
('nickamorg0@gmail.com', 'dogdog@gmail.com', '0000-00-00'),
('nickamorg0@gmail.com', 'sdg2@gmail.com', '0000-00-00'),
('sdg2@gmail.com', 'dogdog@gmail.com', '0000-00-00'),
('sdg2@gmail.com', 'nick@gmail.com', '0000-00-00'),
('sdg2@gmail.com', 'nickamorg0@gmail.com', '0000-00-00');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `message`
--

CREATE TABLE `message` (
  `sender` varchar(40) NOT NULL,
  `recipient` varchar(40) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `message`
--

INSERT INTO `message` (`sender`, `recipient`, `message`, `date`) VALUES
('nick@gmail.com', 'to', 'I dont know...What do you say ?', '2017-02-11 20:16:25'),
('nick@gmail.com', 'hellworld@hotmail.com', 'from', '2017-02-11 20:31:37'),
('nick@gmail.com', 'nickamorg0@gmail.com', 'You are the most beautiful girl in the world', '2017-02-11 20:32:04'),
('nick@gmail.com', 'hellworld@hotmail.com', 'I will come with you today', '2017-02-12 09:59:03'),
('nick@gmail.com', 'nickamorg0@gmail.com', 'I love you', '2017-02-12 10:49:55'),
('nick@gmail.com', 'hellworld@hotmail.com', 'Tomorrow I have 380 and 464, you ?', '2017-02-12 10:51:02'),
('nick@gmail.com', 'nickamorg0@gmail.com', 'I do not understand', '2017-02-12 11:01:57');

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `account`
--
ALTER TABLE `account`
  ADD UNIQUE KEY `email` (`email`);

--
-- Ευρετήρια για πίνακα `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`first_person`,`second_person`);

--
-- Ευρετήρια για πίνακα `friend_requests`
--
ALTER TABLE `friend_requests`
  ADD PRIMARY KEY (`sender`,`recipient`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
