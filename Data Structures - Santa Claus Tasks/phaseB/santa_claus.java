/**********************************************************************
 * @file   santa_claus.java                                           *
 * @author Nikolaos Simidalas <csd3461@csd.uoc.gr>                    *
 *                                                                    *
 * @brief  Main file for the needs of the Data Structures (HY-240a)   *
 * project (Fall 2015)                                                *
 * Computer Science Department, University of Crete, Greece           *
**********************************************************************/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class santa_claus {
    
    static int primes_g[] = {    5,   7,  11,  13,  17,  19,  23,  29,  31,  37,
                                41,  43,  47,  53,  59,  61,  67,  71,  73,  79,
                                83,  89,  97, 101, 103, 107, 109, 113, 127, 131,
                                137, 139, 149, 151, 157, 163, 167, 173, 179, 181,
                                191, 193, 197, 199, 211, 223, 227, 229, 233, 239,
                                241, 251, 257, 263, 269, 271, 277, 281, 283, 293,
                                307, 311, 313, 317, 331, 337, 347, 349, 353, 359,
                                367, 373, 379, 383, 389, 397, 401, 409, 419, 421,
                                431, 433, 439, 443, 449, 457, 461, 463, 467, 479,
                                487, 491, 499, 503, 509, 521, 523, 541, 547, 557,
                                563, 569, 571, 577, 587, 593, 599, 601, 607, 613,
                                617, 619, 631, 641, 643, 647, 653, 659, 661, 673,
                                677, 683, 691, 701, 709, 719, 727, 733, 739, 743,
                                751, 757, 761, 769, 773, 787, 797, 809, 811, 821,
                                823, 827, 829, 839, 853, 857, 859, 863, 877, 881,
                                883, 887, 907, 911, 919, 929, 937, 941, 947, 953    };

    static Child[] age_category = new Child[4];
    static Child[] tmpChild = new Child[3];
    static Present present = null;
    static Present presentGuard = new Present();
    static District district = null;
    static int childInsertKind = 1;
    static int presentDeleteKind = 0;
    static int MAX_KEY = -1;
    static Random rand = new Random();
    static int prime_n = primes_g[rand.nextInt(primes_g.length)];
    static int a = rand.nextInt(prime_n);
    static int b = rand.nextInt(prime_n);
    static int MAX_OF_ALL = -1;
    static Present copyPresent(Present p, int pid, int stock_cnt ) {
        p.pid = pid;
        p.stock_cnt = stock_cnt;
        return p;
    }

    static Present insertPresent(Present p, int pid, int stock_cnt, Present parent) {
        
        if(p == null) {
            p = new Present(pid, stock_cnt);
            p.lc = presentGuard;
            p.rc = presentGuard;
            p.parent = parent;
            return p;   
        }    
        
        if(pid > p.pid) {
            if(p.rc.pid == -1) {
                p.rc = new Present(pid, stock_cnt);
                p.rc.lc = presentGuard;
                p.rc.rc = presentGuard;
                p.rc.parent = p;
            } else {
                p.rc = insertPresent(p.rc, pid, stock_cnt, p);
                p.rc.parent = p;
            }
        } else if(pid < p.pid) {
            if(p.lc.pid == -1) {
                p.lc = new Present(pid, stock_cnt);
                p.lc.rc = presentGuard;
                p.lc.lc = presentGuard;
                p.lc.parent = p;
            } else {
                p.lc = insertPresent(p.lc, pid, stock_cnt, p);
                p.lc.parent = p;
            }
        } else {
            p = copyPresent(p, pid, stock_cnt);
        }
        return p;
        
    }
    
    static void printPresent(Present p) {
        if(p.pid == -1) return;
        
        printPresent(p.lc);
        System.out.println(p.pid);
        printPresent(p.rc);
        
    }
    
    static void printStock(Present p) {
        if(p == null) return;
        
        printStock(p.lc);
        if(p.pid != -1) System.out.print("<"+ p.pid + ":" + p.stock_cnt + "> ");
        printStock(p.rc);
        
    }
    
    static void printChild(Child c) {
        if(c == null) return;
        
        printChild(c.lc);
        System.out.print("<"+ c.cid + ":{" + c.present_choices[0] + ' ' + c.present_choices[1] + ' ' + c.present_choices[2] + "}> ");
        printChild(c.rc);
        
    }
    
    public static Child more_compatible(Child child, Child temp, Child temp2, int key) {
        
        if(temp == null && temp2 == null && child.cid < key) return child;
        if(temp == null && child == null && temp2.cid < key) return temp2;
        if(temp2 == null && child == null && temp.cid < key) return temp;
        if(temp == null && temp2 != null && child != null) {
            if(temp2.cid > child.cid && temp2.cid < key) return temp2;
            if(temp2.cid < child.cid && child.cid < key) return child;
            if(temp2.cid < key) return temp2;
            if(child.cid < key) return child;
        }
        if(temp2 == null && temp != null && child != null) {
            if(temp.cid > child.cid && temp.cid < key) return temp;
            if(temp.cid < child.cid && child.cid < key) return child;
            if(temp.cid < key) return temp;
            if(child.cid < key) return child;
        }
        if(child == null && temp != null && temp2 != null) {
            if(temp2.cid > temp.cid && temp2.cid < key) return temp2;
            if(temp2.cid < temp.cid && temp.cid < key) return temp;
            if(temp2.cid < key) return temp2;
            if(temp.cid < key) return temp;
        }
        if(child != null && temp != null & temp2 != null) {
            if(temp.cid > child.cid && temp.cid < key) {
                child = temp;
            }
            if(temp2.cid > child.cid && temp2.cid < key) {  // apo tmp se tmp2
                child = temp2;
            }
            return child;
        }
        return null;
    }
    
    public static Child next_smaller(Child child, int key) {
        Child temp = null, temp2 = null;
        if(child == null) return null;
        temp = next_smaller(child.lc, key);
        temp2 = next_smaller(child.rc, key);
        return more_compatible(child, temp, temp2, key);
    }
    
    public static Child smallest_of_all(Child child) {
        Child temp = null, temp2 = null;
        if(child == null) return null;
        temp = smallest_of_all(child.lc);
        temp2 = smallest_of_all(child.rc);
        
        if(temp == null && temp2 == null) return child;
        if(temp == null && child == null) return temp2;
        if(temp2 == null && child == null) return temp;
        if(temp == null && temp2 != null && child != null) {
            if(temp2.cid > child.cid) return child;
            if(temp2.cid < child.cid) return temp2;
        }
        if(temp2 == null && temp != null && child != null) {
            if(temp.cid > child.cid) return child;
            if(temp.cid < child.cid) return temp;
        }
        if(child == null && temp != null && temp2 != null) {
            if(temp2.cid > temp.cid) return temp;
            if(temp2.cid < temp.cid) return temp2;
        }
        if(child != null && temp != null && temp2 != null) {
            if(child.cid < temp.cid) {
                temp = child;
            } 
            if(temp2.cid < temp.cid) {
                temp = temp2;
            }
            return temp;
        }
        return null;
    }
    
    public static void copyChild(Child childList, Child child) {
        childList.age = child.age;
        childList.cid = child.cid;
        childList.did = child.did;
        childList.present_choices[0] = child.present_choices[0];
        childList.present_choices[1] = child.present_choices[1];
        childList.present_choices[2] = child.present_choices[2];
        
    }
    
    public static int childExistAlready(Child child, Child newChild) {
        int flag = 0;
        if(child == null) return 0;
        if(child.cid == newChild.cid) {
            copyChild(child, newChild);
            return 1;
        }
        flag = childExistAlready(child.rc, newChild);
        if(flag == 1) return 1;
        return childExistAlready(child.lc, newChild);
    }
    
    public static Child addChild(Child child, Child newChild) {
        Child tmp;
        if(newChild == null) return child;
        
        if(child == null) {
            child = newChild;
            return child;
        }
        
        tmp = next_smaller(child, newChild.cid);
        if(tmp == null) {
            tmp = smallest_of_all(child);
        }
        if(tmp.lc == null) {
            tmp.lc = newChild;
            return child;
        } else if(tmp.rc == null) {
            tmp.rc = newChild;
            return child;
        } else {
            if(childInsertKind == 1) { 
                childInsertKind = 0;
                tmp = prev_child(tmp.lc);
                if(tmp.lc == null) { 
                    tmp.lc = newChild;
                } else {
                    tmp.rc = newChild;
                }
            } else {
                childInsertKind = 1;
                tmp = next_child(tmp.rc);
                if(tmp.lc == null) { 
                    tmp.lc = newChild;
                } else {
                    tmp.rc = newChild;
                }
            }
        }
        return child;
    }
    
    public static Child next_child(Child child) {
        if(child.lc == null) return child;
        return next_child(child.lc);
    }
     
    public static Child prev_child(Child child) {
        if(child.rc == null) return child;
        return next_child(child.rc);
    }
     
    static public District insertDistrict(int did) {
                
        // If the district list is empty
        if(district == null) {
            district = new District(did);
            return district;
        }
        District newDistrict = new District(did);
        District tmp = district;
        District tmp2 = null;
        
        // Sorted list, so check if the new element is smaller from the first of district's list
        if(newDistrict.did < district.did) {
                newDistrict.next = district;
                return newDistrict;
        }
        
        while(newDistrict.did > district.did && district.next!= null) {
            if(district.next != null) {
                tmp2 = district;
                district = district.next;
            }
        }
        
        if(newDistrict.did == district.did) {
            district.children_cnt++;
        } else if(district.next == null && newDistrict.did > district.did) {
            district.next = newDistrict;
        } else {
            newDistrict.next = district;
            tmp2.next = newDistrict;
        }
        return tmp;
    }
    
    public static Present next_Present(Present present) {
        if(present.lc == null) return present;
        return next_Present(present.lc);
    }
     
    public static Present prev_Present(Present present) {
        if(present.rc == null) return present;
        return next_Present(present.rc);
    }
    
    public static Present deleteEmptyPresent(Present present) {
        if(present.pid == -1) return present;
        if(present.stock_cnt == 0) {
            if(present.lc.pid == -1 && present.rc.pid == -1) {
                return presentGuard;
            } else if(present.lc.pid != -1 && present.rc.pid == -1) {
                present.lc.parent = present.parent;
                return present.lc;
            } else if(present.rc.pid != -1 && present.lc.pid == -1) {
                present.rc.parent = present.parent;
                return present.rc;
            } else {
                Present tmp;
                if(presentDeleteKind == 0) {
                    presentDeleteKind = 1;
                    tmp =prev_Present(present);
                } else {
                    presentDeleteKind = 0;
                    tmp = next_Present(present);
                }
                tmp.parent = present.parent;
                tmp.lc = present.lc;
                tmp.rc = present.rc;
            }
        } else {
            present.request_cnt = 0;
        }
        present.lc = deleteEmptyPresent(present.lc);
        present.rc = deleteEmptyPresent(present.rc);
        return present;
    }
    
    public static Child addChildAsList(Child ChildList, Child child) {
        if(ChildList != null) child.rc = ChildList;
        return child;
    }
    
    public static Child go2NextCategory(int i, Child child) {
        Child father, tmp;
        if(child == null) return null;
        
        child.lc = go2NextCategory(i, child.lc);
        child.rc = go2NextCategory(i, child.rc);
        child.age++;
        if(child.age > i * 4 + 3) {
            if(i < 3) {
                Child newChild = new Child(child.cid, child.age, child.did, -1, -1, -1);
                tmpChild[i] = addChild(tmpChild[i], newChild);
            }
            if(child.lc == null && child.rc == null) {
                return null;
            } else if(child.lc != null && child.rc == null) {
                return child.lc;
            } else if(child.rc != null && child.lc == null) {
                return child.rc;
            } else {
                if(presentDeleteKind == 0) {
                    presentDeleteKind = 1;
                    tmp = prev_child(child.lc);
                    father = findFather(child, tmp);
                    father.rc = tmp.lc;
                } else {
                    presentDeleteKind = 0;
                    tmp = next_child(child.rc);
                    father = findFather(child, tmp);
                    father.lc = tmp.rc;
                }
                copyChild(child, tmp);
            }
            
        } else {
            child.present_choices[0] = -1;
            child.present_choices[1] = -1;
            child.present_choices[2] = -1;
        }
        return child;
    }
    
    public static void deleteSomeChild(Child child) {
        Child father, tmp = null;
        if(child == null) return;
        deleteSomeChild(child.lc);
        deleteSomeChild(child.rc);
        child.age++;

        if(child.present_choices[0] == -1 && child.present_choices[1] == -1 && child.present_choices[2] == -1) {
            System.out.println("PROS DIAGRAFH ... " + child.cid);
            if(child.lc == null && child.rc == null) {
                child = null;
            } else if(child.lc != null && child.rc == null) {
                child = child.lc;
            } else if(child.rc != null && child.lc == null) {
                child = child.rc;
            } else {
                if(presentDeleteKind == 0) {
                    presentDeleteKind = 1;
                    tmp = prev_child(child);
                    father = findFather(child, tmp);
                    father.rc = tmp.lc;
                } else {
                    presentDeleteKind = 0;
                    tmp = next_child(child);
                    father = findFather(child, tmp);
                    father.lc = tmp.rc;
                }
                tmp.lc = child.lc;
                tmp.rc = child.rc;
                child = tmp;
            }
        }
    }
    
    public static Child deleteChild(Child child) {
        Child father, tmp = null;
        if(child == null) return null;
        child.lc = deleteChild(child.lc);
        child.rc = deleteChild(child.rc);
        child.age++;
        if(child.present_choices[0] == -1 && child.present_choices[1] == -1 && child.present_choices[2] == -1) {
            if(child.lc == null && child.rc == null) {
                return null;
            } else if(child.lc != null && child.rc == null) {
                return child.lc;
            } else if(child.rc != null && child.lc == null) {
                return child.rc;
            } else {
                if(presentDeleteKind == 0) {
                    presentDeleteKind = 1;
                    tmp = prev_child(child.lc);
                    father = findFather(child, tmp);
                    father.rc = tmp.lc;
                } else {
                    presentDeleteKind = 0;
                    tmp = next_child(child.rc);
                    father = findFather(child, tmp);
                    father.lc = tmp.rc;
                }
                copyChild(child, tmp);
                return child;
            }
            
        }
        return child;
    }
    
    public static Child findEmptyChild(Child child) {
        Child tmp;
        if(child == null) return null;
        if(child.rc != null) return child.rc;
        if(child.lc != null) return child.lc;
        tmp = findEmptyChild(child.rc);
        if(tmp != null) return tmp;
        return findEmptyChild(child.lc);
    }
    public static Child findFather(Child childList, Child child) {
        Child father;
        if(childList == null) return null;
        if(childList.rc == child || childList.lc == child) return childList;
        father = findFather(childList.rc, child);
        if(father != null) return father;
        return findFather(childList.lc, child);
    }
    /**
    * @brief Create a new present and add it to the stock
    *
    * @param pid The new present's id
    * @param stock_cnt The instances of the new present to be stored at the stock
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int buy_present(int pid, int stock_cnt) {
        present =  insertPresent(present, pid, stock_cnt, null);
        return 1;
    }
    /**
     * @brief Create a new request for present from the child  and handle the counters about the stock
     *
     * @param cid The id of the child who sent the letter
     * @param age The age of the child
     * @param did The id of the district in which the child lives
     * @param pid1 The id of the present of the 1st preference of the child
     * @param pid2 The id of the present of the 2nd preference of the child
     * @param pid3 The id of the present of the 3rd preference of the child
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int letter_received(int cid, int age, int did, int pid1, int pid2, int pid3) {
        Child newChild = new Child(cid, age, did, pid1, pid2, pid3);
        Present found;
        if(age > 15 || age < 0) return -1;
        
        for(int i = 0; i < 3; i++) {
            presentGuard.pid = newChild.present_choices[i];
            if(present.pid != newChild.present_choices[i]) {
                found = findPresent(newChild.present_choices[i], present);
                if(found != null) {
                    found.request_cnt++;
                    break;
                }
            }
            if( present.pid == newChild.present_choices[i]) {
                present.request_cnt++;
                break;
            }
        }
        presentGuard.pid = -1;
        if(age < 4) {
           if(childExistAlready(age_category[0], newChild) == 1) return 1;
           age_category[0] = addChild(age_category[0], newChild);
        } else if(age < 8) {
            if(childExistAlready(age_category[1], newChild) == 1) return 1;
            age_category[1] = addChild(age_category[1], newChild);
        } else if(age < 12) {
            if(childExistAlready(age_category[2], newChild) == 1) return 1;
            age_category[2] = addChild(age_category[2], newChild);
        } else if(age < 16) {
            if(childExistAlready(age_category[3], newChild) == 1) return 1;
           age_category[3] = addChild(age_category[3], newChild);
        }
        district = insertDistrict(did);
        return 1;
    }
    
    public static Present bring2Root(int pid, Present present) {
        Present newPresent = findPresent(pid, present);
         
        if(newPresent!= null && newPresent != present) {
            if(newPresent.parent.rc == newPresent) {
                if(newPresent.lc != null) {
                    newPresent.parent.rc = newPresent.lc;
                    newPresent.parent.rc.parent = newPresent.parent;
                    newPresent.lc = newPresent.parent;
                } else {
                    newPresent.parent.rc = null;
                    newPresent.lc = newPresent.parent;
                }
                newPresent.parent.parent = newPresent;
                newPresent.parent = null;
            } else {
                if(newPresent.rc != null) {
                    newPresent.parent.lc = newPresent.rc;
                    newPresent.parent.lc.parent = newPresent.parent;
                    newPresent.rc = newPresent.parent;
                } else {
                    newPresent.parent.lc = null;
                    newPresent.rc = newPresent.parent;
                }
                newPresent.parent.parent = newPresent;
                newPresent.parent = null;
            }
            return newPresent;
        }
        return present;
    }
    
    public static void RemoveAssign() {
        District newDistrict = district;
        while(newDistrict != null) {
            newDistrict.assignHT = null;
          //  newDistrict.children_cnt = 0;
            newDistrict.assignN = null;
            newDistrict = newDistrict.next;
        }
    }
    
    public static Present findPresent(int pid, Present present) {
        Present found = null;
        if(present == null) return null;
        if(present.request_cnt == -1) return null;
        if(pid == present.pid) {
            return present;
        }

        if(pid > present.pid) {
            found = findPresent(pid, present.rc);
        } else {
            found = findPresent(pid, present.lc);
        } 
        return found;
    }
    
    public static void prepareDistrict() {
        District newDistrict = district;
        
        while(newDistrict != null) {
            newDistrict.assignHT = new Present_assign[MAX_OF_ALL];
            if(newDistrict.did % 3 == 0) {
                newDistrict.assignN = new int[MAX_OF_ALL];
                for(int i = 0; i < MAX_OF_ALL; i++) {
                    newDistrict.assignN[i] = -1;
                }
            }
            newDistrict = newDistrict.next;
        }
    }
    
    public static int hasFunction1(int x) {
        return x % MAX_OF_ALL;
    }
    
    public static int hashFunction2(int x) {
        return ((a * x + b) % prime_n) % MAX_OF_ALL;
    }
    
    public static int hasFunction(int x) {
        return (hashFunction2(x)  + hasFunction1(x)) % MAX_OF_ALL;
    }
    
    public static void doubleFragmentation(Child child, int choice) {
        District newDistrict = district;
        int pos;

        while(newDistrict.did != child.did && newDistrict != null) {
            newDistrict = newDistrict.next;
        }
        MAX_KEY = newDistrict.children_cnt;
        pos = hasFunction(child.cid);
        
        if(newDistrict.assignHT[pos] == null) {
            newDistrict.assignHT[pos] = new Present_assign(child.cid, choice);
        } else {
            while(newDistrict.assignHT[pos] != null) {
                pos = (pos + 1) % MAX_OF_ALL;
            }
            newDistrict.assignHT[pos] = new Present_assign(child.cid, choice);
            
        }
    }
    
    public static void doubleSortedFragmentation(Child child, int choice) {
        District newDistrict = district;
        int pos;
        Present_assign tmpChild;

        while(newDistrict.did != child.did && newDistrict != null) {
            newDistrict = newDistrict.next;
        }
        MAX_KEY = newDistrict.children_cnt;
        pos = hasFunction(child.cid);
        if(newDistrict.assignHT[pos] == null) {
            newDistrict.assignHT[pos] = new Present_assign(child.cid, choice);
        } else {
            tmpChild = new Present_assign(child.cid, choice);
            while(newDistrict.assignHT[pos] != null) {
                if(newDistrict.assignHT[pos].cid > tmpChild.cid) {
                    tmpChild = newDistrict.assignHT[pos];
                    newDistrict.assignHT[pos] = tmpChild;
                } 
                pos = (pos + 1) % MAX_OF_ALL;
            }
            newDistrict.assignHT[pos] = tmpChild;
            
        }
    }
    
    public static void mixedChains(Child child, int choice) {
        District newDistrict = district;
        int pos;

        while(newDistrict.did != child.did && newDistrict != null) {
            newDistrict = newDistrict.next;
        }
        MAX_KEY = newDistrict.children_cnt;
        pos = ((MAX_KEY / 14) + hasFunction(child.cid)) % MAX_OF_ALL;
        if(newDistrict.assignHT[pos] == null) {
            newDistrict.assignHT[pos] = new Present_assign(child.cid, choice);
        } else {
            System.out.println("OLA KALA? " + pos);
            while(newDistrict.assignHT[pos] != null) {
                pos = newDistrict.assignN[pos];
                if(pos == -1) break;
            }
            
            do {
                pos = (pos + 1) % MAX_OF_ALL;
            } while(newDistrict.assignHT[pos] != null);
            newDistrict.assignHT[pos] = new Present_assign(child.cid, choice);
        }
    }
    
    public static void typeOfFragmentation(Child child, int choice) { 
        switch(child.did % 3) {
            case 0:
                mixedChains(child, choice);
                break;
            case 1:
                doubleFragmentation(child, choice);
                break;
            default:
                doubleSortedFragmentation(child, choice);
        }
        
    }
    
    public static void givePresent(Child child) {
        int flag = -1;
        if(child == null) return;
        for(int i = 0; i < 3; i++) {
            presentGuard.pid = child.present_choices[i];
            flag  = hasStock(child.present_choices[i], present);
            if(flag == 1) {
                flag = i;
                break;
            }
        }
        if(flag != -1) {
            typeOfFragmentation(child, child.present_choices[flag]);
        } else {
            typeOfFragmentation(child, -2);
        }
        presentGuard.pid = -1;
        givePresent(child.lc);
        givePresent(child.rc);
        
    }
    
    public static int hasStock(int choice, Present present) {
        int found;
        if(present == presentGuard) return 0;
        if(choice == present.pid) {
            if(present.stock_cnt > 0) {
                present.stock_cnt--;
                return 1;
            }
            return 0;
        }
        if(choice > present.pid) {
            found = hasStock(choice, present.rc);
        } else {
            found = hasStock(choice, present.lc);
        }
        return found;
    }
    
    /**
    * @brief Assign presents to children taking into consideration their preference and stock availability 
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int prepare_presents() {
        District newDistrict = district;

        for(int i = 0; i < 4; i++) {
            givePresent(age_category[i]);
        }
        return 1;
        
    }
    
    /**
    * @brief Assign a satisfaction degree to the present received by the child
    *
    * @param cid The id of the child who gives feedback
    * @param did The id of the district where the child lives
    * @param s_degree The number of the satisfaction degree of the child
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int give_feedback(int cid, int did, int s_degree) {
        District newDistrict = district;
        while(newDistrict != null && newDistrict.did != did) {
            newDistrict = newDistrict.next;
        }
        if(newDistrict != null && newDistrict.assignHT != null) {
            for(int i = 0; i < newDistrict.assignHT.length; i++) {
               // System.out.println(i);
                if(newDistrict.assignHT[i] != null && newDistrict.assignHT[i].cid == cid) {
                    newDistrict.assignHT[i].s_degree = s_degree;
                    return 1;
                }
            }
        }
        return -1;
        
    }
    
    /**
    * @brief Identify the k most popular presents
    * 
    * @param k 
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int analytics(int k) {
            return 1;
    }
    
    /**
    * @brief Change the year of presents dispatch
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int new_season() {
        Child newChild = new Child();
        for(int i = 0; i < 4; i++) {
            age_category[i] = go2NextCategory(i, age_category[i]);
            if(i < 3) {
                if(tmpChild[i] != null) {
                      age_category[i + 1] = addChild(age_category[i + 1], tmpChild[i]);
                }
            }
        }
        present = deleteEmptyPresent(present);
        return 1;
    }
    
    /**
    * @brief Remove children who did not send a letter to Santa Claus
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int clear_list_of_children() {
        
        for(int i = 0; i < 4; i++) {
            age_category[i] = deleteChild(age_category[i]);
           // deleteSomeChild(age_category[i]);
        }
        return 1;
    }
    
    /**
    * @brief Search for a specific present in stock list of presents
    *
    * @param pid The id of the present searched
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int search_present(int pid) {
        Present found = null;
        
        presentGuard.pid = pid;
        bring2Root(pid, present);
        if( present.pid == pid) {
            present = found;
            System.out.println("S " + "<" + pid + ">"); 
            System.out.println("    " + "<" + found.stock_cnt + ">, " + "<" + found.request_cnt + ">"); 
            System.out.println("DONE");
            presentGuard.pid = -1;
            return 1;
        }
        presentGuard.pid = -1;
        return -1;
    }
    
    /**
    * @brief Print the children lists of all age categories
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int print_child() {
        System.out.println('H');
        for(int i = 0; i < 4; i++) {
            System.out.print("     " + i * 4 + "-" + (i * 4 + 3) + ":   ");
            printChild(age_category[i]);
            System.out.println();            
        }
        System.out.println("DONE\n");
            return 1;
    }
    
    /**
    * @brief Print the children id and presents id of all districts
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int print_district() {
        if(district == null) return 0;
        
        // If there are districts, then print them
        District newDistrict = district;
        System.out.println("    DISTRICTS:");
        while(newDistrict != null) {
            System.out.print("    <" + newDistrict.did + ", " + newDistrict.children_cnt + ">\n       ASSIGNMENT: ");
            if(newDistrict.assignHT != null) {
                for(int i = 0; i < newDistrict.assignHT.length; i++) {
                    if(newDistrict.assignHT[i] != null) {
                        System.out.print("<" + newDistrict.assignHT[i].cid + ":" + newDistrict.assignHT[i].pid + ":" + newDistrict.assignHT[i].s_degree + "> ");
                    }
                }
            }
            System.out.println();
            newDistrict = newDistrict.next;
        }
        return 1;
    }
    
    /**
    * @brief Print the id of available presents of the stock
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int print_stock() {
        printStock(present);
        return 1;
    }
    
    /**
    * @brief Print the most popular present of each district
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int advanced_analytics() {
            return 1;
    }
   
    /**
    * @param args the command line arguments
    * @throws java.io.FileNotFoundException
    */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedReader inputFile;
        String line;
        String [] params;
                
	/* Check command buff arguments */       
        if (args.length != 1) {
            System.err.println("Usage: <executable-name> <input_file>");
            System.exit(0);
        }

	/* Open input file */        
        inputFile = new BufferedReader(new FileReader(args[0]));

	/* Read input file and handle the events */
        while ( (line = inputFile.readLine()) != null ) {
            params = line.split(" ");
            char eventID = line.charAt(0);

            switch(eventID) {

		/* Buy present
		 * B <pid> <stock_cnt> */
		case 'B':
		{
			int pid = Integer.parseInt(params[1]);
			int stock_cnt = Integer.parseInt(params[2]);

			if (buy_present(pid, stock_cnt) == 1) {	
                            System.out.println("B " + "<" + pid +":" + stock_cnt + "> ");
                            System.out.print("    Presents = ");
                            print_stock();
                            System.out.println("\nDONE\n");
			} else {
                            System.err.println(eventID + " " + pid + " " + stock_cnt + " failed");
			}
			break;
		}

		/* Letter received
		 * L <cid> <age> <did> <pid1> <pid2> <pid3> */
		case 'L':
		{
			int cid = Integer.parseInt(params[1]);
			int age = Integer.parseInt(params[2]);
			int did = Integer.parseInt(params[3]);
			int pid1 = Integer.parseInt(params[4]);
			int pid2 = Integer.parseInt(params[5]);
			int pid3 = Integer.parseInt(params[6]);

                        if(cid > MAX_OF_ALL) MAX_OF_ALL = cid;
			if (letter_received(cid, age, did, pid1, pid2, pid3) == 1) {
                            System.out.println("L " + "<" + cid + ">" + " " +  "<" + age + ">" + " " + "<" + did + ">" + " " + "<" + pid1 + ">" + " " + "<" + pid2 + ">" + " " + "<" + pid3 + ">" + " \nDONE\n");
			} else {
                            System.err.println(eventID + " <" + cid + ">" + " " +  "<" + age + ">" + " " + "<" + did + ">" + " " + "<" + pid1 + ">" + " " + "<" + pid2 + ">" + " " + "<" + pid3 + ">" + " " + "failed");
			}
			break;
		}

		/* Prepare presents
		 * P */
		case 'P':
		{
                    prepareDistrict();
                    if (prepare_presents() == 1) {
                        System.out.println("P");
                        print_district();
                        System.out.println("DONE");
                    } else {
                        System.err.println(eventID + " failed\n");
                    }
                    /*
                    District newDistrict = district;
                    while(newDistrict != null) {
                        System.out.println(newDistrict.assignHT.length);
                        newDistrict = newDistrict.next;
                    }
                    break;
                    */
                    break;
		}

		/* Give feedback
		 * F <cid> <did> <s_degree> */
		case 'F':
		{
                    int cid = Integer.parseInt(params[1]);
                    int did = Integer.parseInt(params[2]);
                    int s_degree = Integer.parseInt(params[3]);

                    if (give_feedback(cid, did, s_degree) == 1) {
                        System.out.println(eventID + " " + cid + " " + did + " " + s_degree + "\n DONE\n");
                    } else {
                        System.err.println(eventID + " " + cid + " " + did + " " + s_degree + " failed\n");
                    }
                    break;
		}

		/* Analytics
		 * A <k> */
		case 'A':
		{
                    int k = Integer.parseInt(params[1]);
                    
                    if (analytics(k) == 1) {
                        System.out.println(eventID + " " + k + " succeeded");
                    } else {
                        System.err.println(eventID + " " + k + " failed");
                    }
                    break;
		}

		/* New season
		 * N */
		case 'N':
		{
                    if (new_season() == 1) {
                        System.out.println(eventID + " succeeded");
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    RemoveAssign();
                    break;
		}

		/* Clear list of children
		 * C */
		case 'C':
		{

                    if (clear_list_of_children() == 1) {
                        System.out.println(eventID + " succeeded");
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* Search present
		 * S <pid> */
		case 'S':
		{
		    int pid = Integer.parseInt(params[1]);
                    if (search_present(pid) == -1) {
                        System.err.println(eventID + " " + pid + " failed");
                    }
                    break;
		}
                
                /* Print child
                 * H */
                case 'H':
                {
                    if (print_child() == -1) {
                        System.err.println(eventID + " failed");
                    }
                    break;
                    
                }
                
                /* Print district
		 * I */
		case 'I':
		{

                    System.out.println("I");
                    if (print_district() == 1) {
                        System.out.println("DONE\n");
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

                /* Print stock
		 * T */
		case 'T':
		{
                    System.out.print("T");
                    System.out.print("\n    Stock = ");
                    if (print_stock() == 11) {
                        System.err.println(eventID + " failed");
                    }
                    System.out.println("\nDONE\n");
                    break;
		}
                /* Advanced analytics
		 * D */
		case 'D':
		{
                    if (advanced_analytics() == 1) {
                        System.out.println(eventID + " succeeded");
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* Empty line */
		case '\n':
			break;

		/* Ignore everything else */
		default:
                    System.out.println("Ignoring " + line);
                    break;
		}
	}
        
    }
    
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
       
}