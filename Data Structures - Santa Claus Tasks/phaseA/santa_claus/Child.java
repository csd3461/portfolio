public class Child {
    int cid;
    int age;
    int did;
    int[] present_choices = new int[3];
    Child next;
    
    Child(int cid, int age, int did, int ch1, int ch2, int ch3) {
        this.cid = cid;
        this.age = age;
        this.did = did;
        this.present_choices[0] = ch1;
        this.present_choices[1] = ch2;
        this.present_choices[2] = ch3;
    }

    Child() { }

    void setData(int cid, int age, int did, int ch1, int ch2, int ch3) {
        this.cid = cid;
        this.age = age;
        this.did = did;
        this.present_choices[0] = ch1;
        this.present_choices[1] = ch2;
        this.present_choices[2] = ch3;
    }

    @Override
    public String toString() {
        return "cid: " + cid + "\nage: " + age + "\ndid: " + did + "\nch1: " + present_choices[0] + 
                "\nch2: " + present_choices[1] + "\nch3: " + present_choices[2];
    }
    
}