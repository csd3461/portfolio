public class Present {
    int pid;
    int stock_cnt;
    int request_cnt = 0;
    Present prev; 
    Present next;
    
    Present(int pid, int stock_cnt) {
        this.pid = pid;
        this.stock_cnt = stock_cnt;
    }
    
    Present() {
        pid = -1;
        stock_cnt = -1;
        request_cnt = -1;
        
    }
    
    @Override
    public String toString() {
        return "pid: " + pid +"\nsock_cnt: " + stock_cnt + "\nrequest_cnt " + request_cnt;
    }
    
    
}
