/**********************************************************************
 * @file   santa_claus.java                                           *
 * @author Nikolaos Simidalas <csd3461@csd.uoc.gr>                    *
 *                                                                    *
 * @brief  Main file for the needs of the Data Structures (HY-240a)   *
 * project (Fall 2015)                                                *
 * Computer Science Department, University of Crete, Greece           *
**********************************************************************/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class santa_claus {
    static Child[] age_category = new Child[4];
    static Present present = new Present();
    static District district = null;
    static String finalMessage = "Presents = ";   
    
    /**
    * @brief Clear the assigments to list district
    *
    * @param district The list district for cleaning
    * 
    * @return the cleard list
    */
    static District clearDistrict(District district) {
        District newDistrict = district;
        while(district != null) {
            district.assignL = null;
            district = district.next;
        }
        return newDistrict;
        
    }
    
    /**
    * @brief Remove a empty present assigment 
    *
    * @param Present The list of Present
    * 
    * @return the present assigments without the empty assigment
    */
    static Present removeEmptyPresent(Present p) {
         p = p.next;
         
         while(p.pid != -1) {
             if(p.stock_cnt == 0) {
                 p.next.prev = p.prev;
                 p.prev.next = p.next;
                 p = p.prev;
             } else {
                 p.request_cnt = 0;
             }
             p = p.next;
         }
         return p;
         
    }
    
    /**
    * @brief Insert a new district if necessary and a new child
    *
    * @param district The list with the districts
    * @param did The id of the district which will insert the next child
    * @param assign The data of the child which will insert
    *
    * @return The new list of districts
    */
    static District insertDistrict(District district, int did, Present_assign assign) {
        
        // If the district list is empty
        if(district == null) {
            district = new District(did, assign);
            return district;
        }
        District newDistrict = new District(did, assign);
        District tmp = district;
        District tmp2 = null;
        
        // Sorted list, so check if the new element is smaller from the first of district's list
        if(newDistrict.did < district.did) {
                newDistrict.next = district;
                return newDistrict;
        }
        
        while(newDistrict.did > district.did && district.next!= null) {
            if(district.next != null) {
                tmp2 = district;
                district = district.next;
            }
        }
        
        if(newDistrict.did == district.did) {
            newDistrict.assignL.next = district.assignL;
            district.assignL = newDistrict.assignL;
        } else if(district.next == null && newDistrict.did > district.did) {
            district.next = newDistrict;
        } else {
            newDistrict.next = district;
            tmp2.next = newDistrict;
        }
        return tmp;
        
    }
    
    /**
    * @brief Search and rise the request of the presents which the child interested in
    *
    * @param p The list with the presents
    * @param c The child which must check
    *
    * @return The list of present with grown request
    */
    static Present SearchAndRiseRequest(Child c, Present p) {
        Present tmp, tmp1;
        int flag = 0, flag1 = 0, flag2 = 0;
        int count;
        if(p.next == null) return p;
        p = p.next;
        while(p.pid != -1) {
            count = 0;
            
            if(p.pid == c.present_choices[0] || p.pid == c.present_choices[1] || p.pid == c.present_choices[2]) {
                if(p.pid == c.present_choices[0]) {
                    flag = 1;
                }
                if(p.pid == c.present_choices[1]) {
                    flag1 = 1;
                } 
                if(p.pid == c.present_choices[2]) {
                    flag2 = 1;
                }
                
                p.request_cnt++;
                if(p.prev.pid != -1) {
                    tmp = p;
                    p.next.prev = p.prev;
                    p = p.prev;
                    p.next = tmp.next;
                    for(int i = 1; i < 5; i++) {
                        if(p.pid != -1) {
                            count++;
                            p = p.prev;
                        } else {
                            break;
                        }
                    }   
                    tmp.next = p.next;
                    p.next.prev = tmp;
                    p.next = tmp;
                    tmp.prev = p;
                    for(int i = 0; i <= count; i++) {
                        p = p.next;
                    }
                }
            }
            p = p.next;
        }
        if(flag == 0 && c.present_choices[0] != 0) {
           p = insertPresent(p, c.present_choices[0], 10);
           p.prev.request_cnt++;
        }
        if(flag1 == 0 && c.present_choices[1] != 0) {
           p = insertPresent(p, c.present_choices[1], 10);
           p.prev.request_cnt++;
        }
        if(flag2 == 0 && c.present_choices[2] != 0) {
           p =  insertPresent(p, c.present_choices[2], 10);
           p.prev.request_cnt++;
        }
        return p;
        
    }
  
    /**
    * @brief Insert a new present if necessary else 
    *
    * @param p The list with the presents
    * @param pid The id of the new present will insert
    * @param stock_cnt The stock of the present which will insert
    *
    * @return The new list of presents
    */
    static Present insertPresent(Present p, int pid, int stock_cnt) {
        if(p.next == null) {
            Present newP = new Present(pid, stock_cnt);
            newP.next = p;
            newP.prev = p;
            p.next = newP;
            p.prev = newP;
            return p;   
        }
        Present newP = new Present(pid, stock_cnt);
        newP.prev = p.prev;
        newP.next = p;
        p.prev.next = newP;
        p.prev = newP;
        return p;
        
    }
    
    /**
    * @brief Change the presents of a child with other ones
    *
    * @param child The child whose will change the presents
    * @param newChild the child whose will take the presents
    *
    * @return The child with its new presents
    */
    static Child swapPresent(Child child, Child newChild) {
        child.present_choices[0] = newChild.present_choices[0];
        child.present_choices[1] = newChild.present_choices[1];
        child.present_choices[2] = newChild.present_choices[2];
        return child;
        
    }
    
    /**
    * @brief Insert a new child if necessary else just change its presents
    *
    * @param child The list with the childs
    * @param newChild The child for insert
    *
    * @return The new list of child
    */
    static Child insertChild(Child child, Child newChild) {
        if(child == null) {
            child = newChild;
            return child;
        }
        
        if(newChild.cid < child.cid) {
            newChild.next = child;
            return newChild;
        } else if(newChild.cid == child.cid) {
            child = swapPresent(child, newChild);
            return child;
        }
        
        Child tmp = child;
        Child tmp2 = null;
        while(newChild.cid > child.cid && child.next != null) {
            if(child.next != null) {
                tmp2 = child;
                child = child.next;
            }
        }
        
        if(child.next == null) {
            
            if(newChild.cid > child.cid) {
                child.next = newChild;
                return tmp;
            } else if(newChild.cid == child.cid) {
                child = swapPresent(child, newChild);
                return tmp;
            }
            
        }
        
        if(newChild.cid != child.cid) {
            newChild.next = child;
            tmp2.next = newChild;
            return tmp;
        }
        newChild.next = child.next;
        tmp2.next = newChild;
        return tmp;
        
    }

    /**
     * @brief Copy the elements of a child to another so that to have a new reference
     *
     * @return 1 on success
     *         0 on failure
     */
    public static Child copy(Child c) {
        Child newC = new Child( c.cid, c.age, c.did, c.present_choices[0], c.present_choices[1], c.present_choices[2]);
        return newC;
    }
    
    /**
     * @brief Check if a child has sent a letter
     *
     * @return 1 on success(has sent)
     *         0 on failure
     */
    public static int hasSentLetter(Child child) {
        if( child.present_choices[0] == 0 &&
            child.present_choices[1] == 0 &&
            child.present_choices[2] == 0 ) {
            return 0;
        }
        return 1;
    }
    
    /**
    * @brief Print the mmost popular present of every district
    */
    static public void most_popular_present() {
        int bestPid,  bestTimes, lessBestPid, lessBestTimes;
        District d = district;
        Present_assign newAss; 
        System.out.println("    Districts =");
        
        // Run the districts
        while(d != null) {
            lessBestPid = -1000;
            lessBestTimes = 0;
            bestPid = d.assignL.pid;
            bestTimes = 1;
            newAss = d.assignL;
            d.assignL = d.assignL.next;
            
            //Check every assigment of this district to find the most popular
            while(d.assignL != null) {
                // Rise the times
                if(d.assignL.pid == bestPid) {
                    bestTimes++;
                } else if(lessBestPid == d.assignL.pid) {
                    lessBestTimes++;
                } else {
                    // If have a new more popular present
                    if(bestTimes < lessBestTimes) {
                        bestTimes = lessBestTimes;
                        bestPid = lessBestPid;
                        lessBestTimes = 1;
                        lessBestPid = d.assignL.pid;
                    } else {
                        lessBestTimes = 1;
                        lessBestPid = d.assignL.pid;
                    }
                }
                d.assignL = d.assignL.next;
            } // New assigment
            
            // A last checking 
            if(lessBestTimes > bestTimes) {
                bestPid = lessBestPid;
            }
            d.assignL = newAss;
            System.out.println("    <" + d.did + "> : " + "<" + bestPid + ">");
            d = d.next;
        } // End district 
        System.out.println("DONE");
        
    }
    
    /**
    * @brief Create a new present and add it to the stock
    *
    * @param pid The new present's id
    * @param stock_cnt The instances of the new present to be stored at the stock
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int buy_present(int pid, int stock_cnt) {
        present = insertPresent(present, pid, stock_cnt);
        return 1;
    }

    /**
    * @brief Create a new request for present from the child  and handle the counters about the stock
    *
    * @param cid The id of the child who sent the letter
    * @param age The age of the child
    * @param did The id of the district in which the child lives
    * @param pid1 The id of the present of the 1st preference of the child
    * @param pid2 The id of the present of the 2nd preference of the child
    * @param pid3 The id of the present of the 3rd preference of the child
    *
    * @return 1 on success
    *         0 on failure
    */
    static int letter_received(int cid, int age, int did, int pid1, int pid2, int pid3) {
        Child newChild = new Child(cid, age, did, pid1, pid2, pid3);
        present = SearchAndRiseRequest(newChild, present); 
        if(age < 4) { 
            age_category[0] = insertChild(age_category[0], newChild);
        } else if(age < 8) {
            age_category[1] = insertChild(age_category[1], newChild);
        } else if(age < 12) {
            age_category[2] = insertChild(age_category[2], newChild);
        } else if(age < 16) {
            age_category[3] = insertChild(age_category[3], newChild);
        }
        return 1;

    }

    /**
    * @brief Assign presents to children taking into consideration their preference and stock availability 
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int prepare_presents() {
        Child[] temp_age_category = new Child[4];
        Present p1, p2, p3;
        int i, count = 0;
        
        for(i = 0; i < 4; i++) {
            if(age_category[i] == null) count++;
        }
        if(count == 4) return 0;
        
        System.out.println("\nP");
        for(i = 0; i < 4; i++) {
            temp_age_category[i] = age_category[i];
            while(temp_age_category[i] != null) {
                p1 = null;
                p2 = null;
                p3 = null;
                present = present.next;
                
                while(present.pid != -1) {
                    if(present.pid == temp_age_category[i].present_choices[0] && present.stock_cnt > 0) {
                        p1 = present;
                    } else if(present.pid == temp_age_category[i].present_choices[1] && present.stock_cnt > 0) {
                        p2 = present;
                    } else if(present.pid == temp_age_category[i].present_choices[2] && present.stock_cnt > 0) {
                        p3 = present;
                    }
                    present = present.next;
                }
                
                if(p1 != null) {
                    Present_assign assign = new Present_assign(temp_age_category[i].cid, temp_age_category[i].present_choices[0]);
                    p1.stock_cnt--;
                    district =  insertDistrict(district, temp_age_category[i].did, assign);
                } else if(p2 != null) {
                    Present_assign assign = new Present_assign(temp_age_category[i].cid, temp_age_category[i].present_choices[1]);
                    p2.stock_cnt--;
                    district =  insertDistrict(district, temp_age_category[i].did, assign);
                } else if(p3 != null) {
                    Present_assign assign = new Present_assign(temp_age_category[i].cid, temp_age_category[i].present_choices[2]);
                    p3.stock_cnt--;
                    district =  insertDistrict(district, temp_age_category[i].did, assign);
                } else {
                    Present_assign assign = new Present_assign(temp_age_category[i].cid, -2);
                    district =  insertDistrict(district, temp_age_category[i].did, assign); 
                }
                temp_age_category[i] = temp_age_category[i].next;
            }
            
        }
        return 1;

    }
    /**
     * @brief Assign a satisfaction degree to the present received by the child
     *
     * @param cid The id of the child who gives feedback
     * @param s_degree The number of the satisfaction degree of the child
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int give_feedback(int cid, int s_degree) {
        District newDistrict = district;

        while(newDistrict != null) {
            Present_assign temp = newDistrict.assignL;
            while(newDistrict.assignL != null) {
                
                if(newDistrict.assignL.cid == cid) {
                    
                    if(newDistrict.assignL.pid != -2) {
                        newDistrict.assignL.s_degree = s_degree;
                    } else {
                        newDistrict.assignL.s_degree = 1;
                    }
                    System.out.format("\nF <%d> <%d> <%d>\nDONE\n", cid, newDistrict.assignL.s_degree, newDistrict.assignL.pid);
                    newDistrict.assignL = temp;
                    return 1;
                    
                }
                newDistrict.assignL = newDistrict.assignL.next;
                
            }
            newDistrict.assignL = temp;
            newDistrict = newDistrict.next;
            
        }
        return -1;
        
    }

    /**
     * @brief Sort the stock list of presents
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int analytics() {
        if(present.next.pid == -1) return 0;
        int next = 0;
        int i;
        Present tmp, tmp1;
        present = present.next;
        tmp1 = present;
        while(present.pid != -1) {
            tmp1 = tmp1.next;
            present = tmp1;
            
            while(present.prev.pid != -1) {
                tmp = present;
                
                if(present.request_cnt > present.prev.request_cnt) {
                    present = present.prev;
                    present.next = tmp.next;
                    tmp.next.prev = present;

                    tmp.next = present;
                    tmp.prev = present.prev;

                    present.prev.next = tmp;
                    present.prev = tmp;
                    present = tmp;
                    //present = present.prev;
                } else break;
                
            }
            
        }
        System.out.println("\nA\n");
        present = present.next;
        while(present.pid != -1) {
            System.out.format("    <%d> : <%d>", present.pid, present.request_cnt);
            System.out.println();
            present = present.next;
        }
        System.out.println("DONE\n");
        return 1;
        
    }

    /**
     * @brief Change the year of presents dispatch
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int new_season() {
        int i, count = 4;
        Child[] temp_age_category = new Child[4];
        Child tmp = null;
        Child[] newChildList = new Child[4];

        for(i = 0; i < 4; i++) {
            if(age_category[i] == null) count++;
        }
        if(count == 4) return 0;
        
        // Check who children must move
        for(i = 0; i < 4; i++) {
            temp_age_category[i] = age_category[i];
            if(temp_age_category[i] != null) {
                temp_age_category[i].age++;
                temp_age_category[i].present_choices[0] = 0;
                temp_age_category[i].present_choices[1] = 0;
                temp_age_category[i].present_choices[2] = 0;
                
                while(temp_age_category[i].next != null) {
                    temp_age_category[i].next.age++;
                    temp_age_category[i].next.present_choices[0] = 0;
                    temp_age_category[i].next.present_choices[1] = 0;
                    temp_age_category[i].next.present_choices[2] = 0;
                    
                    if(temp_age_category[i].next.age > i * 4 + 3) {
                        newChildList[i] = insertChild(newChildList[i], copy(temp_age_category[i].next));
                        tmp = temp_age_category[i];
                        temp_age_category[i].next = temp_age_category[i].next.next;
                        continue;
                    }
                    
                    tmp = temp_age_category[i];
                    temp_age_category[i] = temp_age_category[i].next;
                }
                
                if(age_category[i].age > i * 4 + 3) {
                    newChildList[i] = insertChild(newChildList[i], copy(age_category[i]));
                    age_category[i] = age_category[i].next;
                }
                
                if(temp_age_category[i] == null && tmp.age > i * 4 + 3) {
                    newChildList[i] = insertChild(newChildList[i], copy(tmp));
                    tmp = null;
                }
                
            }
        }
        
        // Place the children to their right position
        for(i = 0; i < 3; i++) {
            Child childList = null;
            while(age_category[i + 1] != null && newChildList[i] != null) {
                
                if(newChildList[i].cid < age_category[i + 1].cid) {
                    childList = insertChild(childList, copy(newChildList[i]));
                    newChildList[i] = newChildList[i].next;
                } else {
                    childList = insertChild(childList, copy(age_category[i + 1]));
                    age_category[i + 1] = age_category[i + 1].next;
                }
                
            }
            
            if(age_category[i + 1] == null) {
                
                while(newChildList[i] != null) {
                    childList = insertChild(childList, copy(newChildList[i]));
                    newChildList[i] = newChildList[i].next;
                }
                
            } else {
                
                while(age_category[i + 1] != null) {
                    childList = insertChild(childList, copy(age_category[i + 1]));
                    age_category[i + 1] = age_category[i + 1].next;
                }
                
            }
            age_category[i + 1] = childList;
        }
        district = clearDistrict(district);
        present = removeEmptyPresent(present);
        return 1;
        
    }
      
    
    /**
     * @brief Remove children who did not send a letter to Santa Claus
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int clear_list_of_children() {
        int i, count = 0;
        Child[] temp_age_category = new Child[4];
        
        for(i = 0; i < 4; i++) {
            if(age_category[i] == null) count++;
        }
        if(count == 4) return 0;
        
        System.out.println("\nC");
        for(i = 0; i < 4; i++) {
            if(age_category[i] == null) continue;
            temp_age_category[i] = age_category[i];
            while(temp_age_category[i].next != null) {
                if(hasSentLetter(temp_age_category[i].next) == 0) {
                    temp_age_category[i].next = temp_age_category[i].next.next;
                    continue;
                }
                temp_age_category[i] = temp_age_category[i].next;
            }
            if(hasSentLetter(age_category[i]) == 0) {
                    age_category[i] = age_category[i].next;
            }
        }
        return 1;
        
    }

    /**
    * @brief Search for a specific present in stock list of presents
    *
    * @param pid The id of the present searched
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int search_present(int pid) {
        if(present.next == null) return 0;
        
        // If there are present, then look up it
        present = present.next;
        while(present.pid != -1 ) {
            if(present.pid == pid) { 
                // Found the present, so print it
                System.out.println("S " + pid);
                System.out.println("     " + present.stock_cnt + " , " + present.request_cnt);
                System.out.println("DONE");
                return 1;
            }
            present = present.next;
        }
        System.out.println("\nS " + pid);
        System.out.println("     0 , 0");
        System.out.println("DONE");
        return 1;
        
    }

    /**
    * @brief Print the children lists of all age categories
    *
    * @return 1 on success
    *         0 on failure
    */
    public static int print_child() {
        Child[] new_age_category = new Child[4];
        int i;
        
        // There are 4 age category for running 
        for(i = 0; i < 4; i++) {
            new_age_category[i] = age_category[i];
            System.out.print("     " + i * 4 + "-" + (i * 4 + 3) + ":   ");
            
            // If there are children, then print them
            while(new_age_category[i] != null) {
                System.out.print("<" + new_age_category[i].cid + "> ");
                new_age_category[i] = new_age_category[i].next;
            }
            
            System.out.println();
        }
        System.out.println("DONE\n");
        return 1;
        
    }

    /**
     * @brief Print the children id and presents id of all districts
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int print_district() {
        if(district == null) return 0;
        
        // If there are districts, then print them
        District newDistrict = district;
        System.out.println("    DISTRICTS:");
        while(newDistrict != null) {
            System.out.print("    <" + newDistrict.did + ">\n       ASSIGNMENT: ");
            Present_assign newAssign = newDistrict.assignL;
            
            // Every district has some assigments for printing 
            while(newDistrict.assignL != null) {
                System.out.format("<%d,%d> ", newDistrict.assignL.cid, newDistrict.assignL.pid);
                newDistrict.assignL = newDistrict.assignL.next;
            }
            
            newDistrict.assignL = newAssign;
            System.out.println();
            newDistrict = newDistrict.next;
        }
        System.out.print("DONE\n");
        return 1;

    }

    /**
     * @brief Print the id of available presents of the stock
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int print_stock() {
        if(present.next.pid == -1) return 0;
        
        //If there are presents, then print them
        present = present.next;
        System.out.print("\n    Stock = ");
        while(present.pid != -1) {
            if(present.stock_cnt > 0) {
                System.out.print("<" + present.pid + "> ");
            }
            present = present.next;
        }
        System.out.println("\nDONE");
        return 1;

    }

    /**
     * @brief Print the most popular present of each district
     *
     * @return 1 on success
     *         0 on failure
     */
    public static int advanced_analytics() {
        if(district == null) return 0;
        District newDistrict = district;
        Present_assign elem, pos, prevElem, holdBegin, newAss, tmpNewAss;
        
        // Run the districts
        while(newDistrict != null) {
            newAss = newDistrict.assignL;
            holdBegin = newDistrict.assignL;
            prevElem = null;
            
            // run every assigment for checking
            while(newAss != null) {
                pos = null;
                elem = newAss;
                tmpNewAss = newAss.next;
                
                // Check if the assigment is smaller than someone else
                while(tmpNewAss != null) {
                    if(elem.pid < tmpNewAss.pid) {
                        pos = tmpNewAss;
                    }
                    tmpNewAss = tmpNewAss.next;
                }
                newAss = newAss.next;
                // If there smaller assigment
                if(pos != null) {
                    if(prevElem != null) {
                        prevElem.next = elem.next;
                    } else {
                        holdBegin = holdBegin.next;
                    }
                    elem.next = pos.next;
                    pos.next = elem;
                } else {
                    prevElem = elem;
                }
            }
            newDistrict.assignL = holdBegin;
            newDistrict = newDistrict.next;
        }
        System.out.println("\nD");
        most_popular_present();
        return 1;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedReader inputFile;
        String line;
        String [] params;
                
	/* Check command buff arguments */
        if (args.length != 1) {
            System.err.println("Usage: <executable-name> <input_file>");
            System.exit(0);
        }

	/* Open input file */        
        inputFile = new BufferedReader(new FileReader(args[0]));

	/* Read input file and handle the events */
        while ( (line = inputFile.readLine()) != null ) {

            //System.out.println("Event: " + line);
            params = line.split(" ");
            char eventID = line.charAt(0);

            switch(eventID) {

		/* Buy present
		 * B <pid> <stock_cnt> */
		case 'B':
		{
                    int pid = Integer.parseInt(params[1]);
                    int stock_cnt = Integer.parseInt(params[2]);

                    if (buy_present(pid, stock_cnt) == 1) {
                        String message = "B " + pid + ' ' + stock_cnt;
                        finalMessage += String.format("%d ", pid);
                        System.out.println(message + '\n' + finalMessage + "\nDONE\n");
                    } else {
                        System.err.println("B failed");
                        System.err.println(eventID + " " + pid + " " + stock_cnt + " failed");
                    }
                    break;
		}

		/* Letter received
		 * L <cid> <age> <did> <pid1> <pid2> <pid3> */
		case 'L':
		{
                    int cid = Integer.parseInt(params[1]);
                    int age = Integer.parseInt(params[2]);
                    int did = Integer.parseInt(params[3]);
                    int pid1 = Integer.parseInt(params[4]);
                    int pid2 = Integer.parseInt(params[5]);
                    int pid3 = Integer.parseInt(params[6]);

                    if (letter_received(cid, age, did, pid1, pid2, pid3) == 1) {
                        System.out.println("\nL" + " " + cid + " " + age + " " +did+ " " +pid1+ " " +pid2+ " " +pid3 + "\n" + "DONE");
                    } else {
                        System.err.println(eventID + " " + cid + " " + age + " " +did+ " " +pid1+ " " +pid2+ " " +pid3 + " failed");
                    }
                    break;
		}

		/* Prepare presents
		 * P */
		case 'P':
		{
                    if (prepare_presents() == 1) {
                        print_district();
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* Give feedback
		 * F <cid> <s_degree> */
		case 'F':
		{
                    int cid = Integer.parseInt(params[1]);
                    int s_degree = Integer.parseInt(params[2]);

                    if (give_feedback(cid, s_degree) == 1) {
                    } else {
                        System.err.println(eventID + " " + cid + " " + s_degree + " failed");
                    }
                    break;
		}

		/* Analytics
		 * A */
		case 'A':
		{
                    if (analytics() != 1) {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* New season
		 * N */
		case 'N':
		{
                    if(new_season() == 1) {
                       System.out.println("\nN\nDONE");
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    
                    break;
		}

		/* Clear list of children
		 * C */
		case 'C':
		{
                    if (clear_list_of_children() == 1) {
                        print_child();
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* Search present
		 * S <pid> */
		case 'S':
		{
		    int pid = Integer.parseInt(params[1]);
                    search_present(pid);
                    break;
		}
                
                /* Print child
                 * H */
                case 'H':
                {
                    System.out.println("\nH");
                    if (print_child() == 1) {
                    } else {
                        System.err.println(eventID + " failed");
                    }
                    break;
                    
                }
                
                /* Print district
		 * I */
		case 'I':
		{
                    if(district != null) System.out.println("\nI");
                    if (print_district() != 1) {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

                /* Print stock
		 * T */
		case 'T':
		{
                    System.out.print("\nT");
                    if (print_stock() != 1) {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}
                /* Advanced analytics
		 * D */
		case 'D':
		{
                    if (advanced_analytics() == 0) {
                        System.err.println(eventID + " failed");
                    }
                    break;
		}

		/* Empty line */
		case '\n':
			break;

		/* Ignore everything else */
		default:
                    System.out.println("Ignoring " + line);
                    break;
		}
	} //End of while
    }
           
}