<%--
    Document   : newtransaction
    Created on : 13 Ιαν 2017, 3:20:50 μμ
    Author     : NickSyn
--%>

<%@page import="db.*" %>
<%@page import="model.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - New Transaction</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
 		
		<%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = AccountsDB.getTableName(name);
			int credit = AccountsDB.getCredit(name) - AccountsDB.getDebt(name);
			
			ArrayList<Merchant> Merchants = AccountsDB.getMerchants(); 

			String merchant = request.getParameter("merchant");

			if(credit > 0 && merchant != null) {
				int amount = Integer.parseInt(request.getParameter("amount"));
				String type = request.getParameter("type");
				AccountsDB.makePurchase(name, merchant, amount, Payment.valueOf(type));
				credit = AccountsDB.getCredit(name);
		%>
			<div id="snackbar">
				The transaction was completed <br /> 
				<% if(credit <= 0) { %> 
					<span  style = "color:red"> There is not enough credit</span> <br /> 
					<span  style = "color:red"> New Transaction option is not available </span>
				<% } %>
			</div>

			<script>
				var x = document.getElementById("snackbar");
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>

		<% } %>
			
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" >CCC</a>
				</div>
				
				<ul class="nav navbar-nav">
					<li><a href="index.html">Log Out</a></li>
					<li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
					<li class="active"><a>New Transaction</a></li>
					<li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
					<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
					<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
					<li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
					<% if(table_name == Table.EMPLOYEE) {%>
						<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
					<% } %>
				</ul>
				
			</div>
		</nav>
				
		<% if(credit == 0) { %>
			<div id="snackbar" style = "color:red" >There is not enough credit <br /> New Transaction option is not available </div>

			<script>
				var x = document.getElementById("snackbar");
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
			
		<% } else if(Merchants == null || Merchants.size() == 0) { %>
			<div id="snackbar" style = "color:red" >There is no available Merchant <br /> New Transaction option is not available </div>

			<script>
				var x = document.getElementById("snackbar");
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
			
		<% } else { %>
			<table class="table table-striped table-bordered">
				<tbody>
					 <tr>
						 <th colspan="2">Details</th>
					 </tr>

					 <tr>
						 <th>Credit</th>
						 <th title = "It has been removed the debt amount"><%=credit%></th>
					 </tr>

				 </tbody>
			</table>

			<form action="new_transaction.jsp" name="newtransaction" method="GET">
				<input type="text" name="name" value = "<%=name%>" hidden>
				<input type="text" name="account_type" value = "<%=account_type%>" hidden>
					<div class="form-inline">
						<div class="input-group">
							<div class="input-group-addon">€</div>
							<input id = "amount" class="form-control" type="number" name="amount" min="1" oninput="if(this.value > <%=credit%>) this.value= <%=credit%>" max="<%=credit%>" placeholder = "Amount" required>
							<div class="input-group-addon">.00</div>
						</div>
						<select class="form-control" id = "merchant" name = "merchant" required>
								<option value="" selected>Select Merchant</option>
								<% for(int i = 0; i < Merchants.size(); i++) { %>
										<option value="<%=Merchants.get(i).getName()%>"><%=Merchants.get(i).getName() %></option>
								<% } %>
						</select>
						<select class="form-control" id = "type" name = "type" required>
								<option value="" selected>Select Type</option>
								<option value="CHARGE" >CHARGE</option>
								<option value="CREDIT" >CREDIT</option>
						<input class="btn btn-primary" style="margin:1em;" type="submit" value="DONE">
					</div>
			</form>
		<% } %>
	
    </body>
</html>
