<%-- 
    Document   : reward
    Created on : 21 Ιαν 2017, 7:19:58 μμ
    Author     : NickSyn
--%>

<%@page import="model.Merchant"%>
<%@page import="db.AccountsDB"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Reward</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
		
		<% String name = request.getParameter("name"); %>
		
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
						<a class="navbar-brand">CCC</a>
					</div>
					
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
						<li><a href="good_customers.jsp?name=<%=name%>">Good Customers</a></li>
						<li><a href="bad_customers.jsp?name=<%=name%>">Bad Customers</a></li>
						<li class="active" title = "Reward The Merchant Of The Month"><a href="reward.jsp?name=<%=name%>">Reward</a></li>
						<li><a href="customers.jsp?name=<%=name%>">Customers</a></li>
						<li><a href="all_transactions.jsp?name=<%=name%>">Transactions</a></li>
					</ul>
			</div>
		</nav>
						
        <%	
			ArrayList<Merchant> merchants = AccountsDB.getMerchantsOfTheMonth();
			String merchant_names = "";
			String merchant_name;
			
			for (Merchant merchant : merchants) {
				merchant_name = merchant.getName();
				merchant_names += merchant_name+",";
				AccountsDB.rewardMerchantOfTheMonth(merchant_name);
			}

			if(!merchant_names.isEmpty()) {
		%>
			<center> <img src ="http://images.clipartpanda.com/reward-clipart-reward-clipart-1.jpg" /> </center>
			
			<div id="snackbar">
				<% if(merchants.size() > 1) {%>
					Merchants : <%=merchant_names%> were rewarded
				<% } else { %>
					Merchant : <%=merchant_names%> was rewarded
				<% } %>
			</div>

			<script>
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
			
		<%  } else  { %>
			<div id="snackbar" style ="color:red"> There has not been any Merchant having transactions the past month  </div>

			<script>
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
			
		<%  } %>
		
    </body>
</html>
