<%--
Document   : deleteaccount
Created on : Jan 12, 2017, 2:29:16 PM
Author     : skerdbash
--%>

<%@page import="model.Table"%>
<%@page import="db.AccountsDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Delete Account</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
       
        <%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = Table.valueOf(account_type);
			if(AccountsDB.deleteAccount(name) == true) {
        %>
		
			<nav class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.html">CCC</a>
					</div>
					<ul class="nav navbar-nav">
						<li><a href="login.html">Log in</a></li>
						<li><a href="register.html">Register</a></li>
					</ul>
				</div>
			</nav>
		
			<a href="index.html">Return to Log In page</a>
			
			<div id="snackbar">Account was deleted successfully</div>

			<script>
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
			
        <% } else { %>
			<nav class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" >CCC</a>
					</div>
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
                        <li class="active"><a>Delete Account</a></li>
						<% if(table_name != Table.MERCHANT) {%>
							<li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
							<li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
						<% } %>
						<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
						<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
						<li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
						<% if(table_name == Table.EMPLOYEE) {%>
							<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
						<% } %>
					</ul>
				</div>
			</nav>
			
			<% if(AccountsDB.getDebt(name) > 0) { %>
			
				<div id="snackbar" style ="color:red"> This account can't be deleted before the payoff of the debt </div>

				<script>
					var x = document.getElementById("snackbar")
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
			<% }%>
			
        <% } %>
		
    </body>
	
</html>
