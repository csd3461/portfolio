<%-- 
    Document   : customers
    Created on : 22 Ιαν 2017, 10:31:58 μμ
    Author     : NickSyn
--%>

<%@page import="model.*"%>
<%@page import="db.AccountsDB"%>
<%@page import="model.Account"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Customers</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
		
		<% String name = request.getParameter("name"); %>
		
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
						<a class="navbar-brand">CCC</a>
					</div>
					
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
						<li><a href="good_customers.jsp?name=<%=name%>">Good Customers</a></li>
						<li><a href="bad_customers.jsp?name=<%=name%>">Bad Customers</a></li>
						<li title = "Reward The Merchant Of The Month"><a href="reward.jsp?name=<%=name%>">Reward</a></li>
						<li class="active"><a>Customers</a></li>
						<li><a href="all_transactions.jsp?name=<%=name%>">Transactions</a></li>
					</ul>
			</div>
		</nav>
						
        <% 
			ArrayList<Company> coms = AccountsDB.getCompanies();
			ArrayList<Individual> inds = AccountsDB.getIndividuals();
			ArrayList<Merchant> merches = AccountsDB.getMerchants();
			
			if((coms.size() + inds.size() + merches.size()) == 0) {
		%>
				<div id="snackbar" style ="color:red">No Available Customers</div>

				<script>
					var x = document.getElementById("snackbar")
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<% } else { %>
		
			<% if(inds.size() > 0) { %>
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<th colspan="3">Individuals</th>
						</tr>

						<tr>
							<th>Name</th>
							<th>Account ID</th>
							<th>Debt</th>
						</tr>

						<%	for(int i = 0; i < inds.size(); i++) { %>
							<tr>
								<td><%=inds.get(i).getName()%> </td>
								<td><%=inds.get(i).getAccountId()%></td>
								<td><%=inds.get(i).getDebt()%></td>
							</tr>
						<%  } %>
					 </tbody>
				</table>
			<%  }  %>		
			
			<% if(coms.size() > 0) { %>
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<th colspan="3">Companies</th>
						</tr>

						<tr>
							<th>Name</th>
							<th>Account ID</th>
							<th>Debt</th>
						</tr>

						<%	for(int i = 0; i < coms.size(); i++) { %>
							<tr>
								<td><%=coms.get(i).getName()%> </td>
								<td><%=coms.get(i).getAccountId()%></td>
								<td><%=coms.get(i).getDebt()%></td>
							</tr>
						<%  } %>
					 </tbody>
				</table>
			<%  }  %>	
			
			<% if(merches.size() > 0) { %>
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<th colspan="3">Merchants</th>
						</tr>

						<tr>
							<th>Name</th>
							<th>Account ID</th>
							<th>Debt</th>
						</tr>

						<%	for(int i = 0; i < merches.size(); i++) { %>
							<tr>
								<td><%=merches.get(i).getName()%> </td>
								<td><%=merches.get(i).getAccountId()%></td>
								<td><%=merches.get(i).getDebt()%></td>
							</tr>
						<%  } %>
					 </tbody>
				</table>
			<%  }  %>	
			
		<%  }  %>
    </body>
	
</html>

