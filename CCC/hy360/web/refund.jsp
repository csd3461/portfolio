<%-- 
    Document   : getrefund
    Created on : Jan 21, 2017, 7:37:42 AM
    Author     : skerdbash
--%>

<%@page import="model.Table"%>
<%@page import="db.AccountsDB"%>
<%@page import="model.Transaction"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Get A Refund</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>


		<%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			String date = request.getParameter("date");
			Table table_name = AccountsDB.getTableName(name);
			boolean refund = false;
			if(date != null) {
				if((refund = AccountsDB.makeRefund(date)) == true) { 
		%>
					<div id="snackbar">Refund was successful</div>

					<script>
						var x = document.getElementById("snackbar");
						x.className = "show";
						setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
					</script>
			<%  } %>
			
		<%  } %>

		<nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" >CCC</a>
                </div>
				
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Log Out</a></li>
                    <li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
                    <li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
                    <li class="active"><a>Get A Refund</a></li>
					<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
                    <li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
                    <li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
					<% if(table_name == Table.EMPLOYEE) {%>
						<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
					<% } %>

                </ul>
            </div>
        </nav>
					
		<% 
			ArrayList<Transaction> trans = AccountsDB.getTransactionsOf(name);
			if(trans != null && trans.size() > 0) {
		%>
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<th colspan="5">Transactions</th>
					</tr>

					<tr>
						<th>Merchant</th>
						<th>Date</th>
						<th>Amount</th>
						<th>Type</th>
						<th>Action</th>
					</tr>

					<% for(int i = 0; i < trans.size(); i++) { %>
						<tr>
							<td><%=trans.get(i).getMerchantName()%> </td>
							<td><%=trans.get(i).getDate()%></td>
							<td><%=trans.get(i).getAmount()%></td>
							<td><%=trans.get(i).getType()%></td>
							<td><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>&date=<%=trans.get(i).getDate()%>">Refund</a></td>
						</tr>
					<% } %>
				
				 </tbody>
			</table>
	    <% } else if(refund == false) { %>
			<div id="snackbar" style = "color:red" >You haven't made any new transactions </div>

			<script>
				var x = document.getElementById("snackbar");
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
		<% }%>
		
    </body>
</html>
