<%--
    Document   : newtransaction
    Created on : 13 Ιαν 2017, 3:20:50 μμ
    Author     : NickSyn
--%>

<%@page import="db.*" %>
<%@page import="model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Pay Back</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>

		<%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = Table.valueOf(account_type);
			String amount_str = request.getParameter("amount");
			int amount = 0;
			int debt = AccountsDB.getDebt(name);
			int credit;
			if(table_name == Table.MERCHANT) {
				credit = AccountsDB.getProfit(name);
			}else {
				credit = AccountsDB.getCredit(name);
			}
			if(amount_str != null) {
				amount = Integer.parseInt(amount_str);
			}
			
		%>

		<nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" >CCC</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Log Out</a></li>
                    <li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
                    <% if(table_name != Table.MERCHANT) {%>
                        <li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
						<li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
                    <% } %>
                    <li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
                    <li class="active"><a>Pay Debt</a></li>
                    <li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
					<% if(table_name == Table.EMPLOYEE) {%>
						<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
					<% } %>
                </ul>
            </div>
        </nav>

		<%
			if((amount > 0) && (amount <= debt) && (amount <= credit)) {
				AccountsDB.payDebt(name, amount);
				debt = AccountsDB.getDebt(name);
				
				credit = AccountsDB.getCredit(name);
		%>
		
				<div id="snackbar">The payoff was completed</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<%  } %>

		<% if(debt != 0) { %>
			<table class="table table-striped table-bordered">
			   <tbody>
					<tr>
						<th colspan="2">Details</th>
					</tr>

					<tr>
						<% if(table_name == Table.MERCHANT) { %>
							<th>Profit</th>
						<% }else {%>
							<th>Credit</th>
						<% } %>
						<th>Debt</th>
					</tr>

					<tr>
						<td><%=credit%></td>
						<td><%=debt%></td>
					</tr>

				</tbody>
		   </table>

			<form action="pay_debt.jsp" name="paydebt" method="GET">
				<input type="text" name="name" value = "<%=name%>" hidden>
				<input type="text" name="account_type" value = "<%=account_type%>" hidden>
					<div class="form-inline">
						<div class="input-group">
							<div class="input-group-addon">€</div>
							<input id = "amount" class="form-control" type="number" name="amount" style="width: 7em" oninput="if(this.value > <%=debt%>) this.value= <%=debt%>" min="1" max="<%=credit%>" placeholder = "Amount" required>
							<div class="input-group-addon">.00</div>
						</div>
						<input class="btn btn-primary" style="margin:1em;" type="submit" value="Payoff">
					</div>
			</form>
		<%  } else { %>
				<div id="snackbar" style = "color:red" >There is not any debt <br /> Pay Debt option is not available </div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<% } %>
		
    </body>
</html>
