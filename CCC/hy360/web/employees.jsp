<%-- 
    Document   : employees
    Created on : 22 Ιαν 2017, 10:24:19 πμ
    Author     : NickSyn
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="db.*" %>
<%@page import="model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Employees </title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>

		<%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = Table.valueOf(account_type);
			String employee = request.getParameter("employee");
			
			if(employee != null) {
				AccountsDB.deleteAccount(employee, Table.EMPLOYEE);
		%>
				<div id="snackbar">The employee with name <%= employee %> has been removed</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<% } %>

		<% 
			String newemployee = request.getParameter("newemployee");
			if(newemployee != null && (AccountsDB.getEmployee(AccountsDB.getEmployee(name).getCompanyName() + "_"  + newemployee) != null)) {
		%>
				<div id="snackbar" style = "color:red">This employee has already access</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<% 
			} else if(newemployee != null) {
				AccountsDB.addEmployee(name.substring(0, name.length() - 6), newemployee);
		%>

				<div id="snackbar">Employee <%=newemployee%> has access to this company's account</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<%  } %>

		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" >CCC</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="index.html">Log Out</a></li>
					<li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
					<li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
					<li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
					<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
					<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
					<li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
					<li class="active"><a>Employees</a></li>
				</ul>
			</div>
		</nav>

		<% if(!name.substring(name.length() - 6).equals("_admin")) { %>
			<div id="snackbar" style = "color:red" >No access</div>

			<script>
				var x = document.getElementById("snackbar");
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
		<%
			} else { 
				
				ArrayList<Employee> emps = AccountsDB.getEmployeesByCompanyName(name.substring(0, name.length() - 6));
				if(emps == null || emps.size() == 0) { 
			%>
					<div id="snackbar" style = "color:red" >No Access</div>

					<script>
						var x = document.getElementById("snackbar");
						x.className = "show";
						setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
					</script>
				<% } else { %> 
			
					<table class="table table-striped table-bordered">
						<tbody>

							<tr>
								<th colspan="3">Employees</th>
							</tr>

							<tr>
								<th> Name </th>
								<th> ID </th>
								<th> Action </th>
							</tr>


							<% for(int i = 0; i < emps.size(); i++) { %>
								<tr>
									<td> <%=emps.get(i).getName()%> </td>
									<td> <%=emps.get(i).getId()%> </td>
									<td> 
										<% if(!emps.get(i).getName().equals(name)) { %>
											<a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>&employee=<%=emps.get(i).getName()%>">Remove Account Access</a>
										<% } %>
									</td>
								</tr>
							<% } %>

						</tbody>
					</table>
				<% } %>
				
				
				<div class="container">
					<form action="employees.jsp" name="login" method="GET">
						<div class="form-group">
							<input type="text" name="name" value ="<%=name%>" hidden>
							<input type="text" name="account_type" value ="<%=account_type%>" hidden>
							<label for="name">Give access to a new employee</label>
							<input id="newemployee" maxlength="6" type="text" style="width:20em;" class="form-control" name="newemployee" placeholder="Enter a name" required>
							<button class="btn btn-primary" style="margin:1em;" type="submit">DONE</button>
						</div>
					</form>
				</div>

		<% } %>
    </body>
</html>

