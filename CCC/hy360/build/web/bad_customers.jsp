<%-- 
    Document   : bad_customers
    Created on : 21 Ιαν 2017, 10:50:47 πμ
    Author     : NickSyn
--%>

<%@page import="db.AccountsDB"%>
<%@page import="model.Account"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Bad Customers</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
		
		<% String name = request.getParameter("name"); %>
		
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
						<a class="navbar-brand">CCC</a>
					</div>
					
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
						<li><a href="good_customers.jsp?name=<%=name%>">Good Customers</a></li>
						<li class="active"><a href="bad_customers.jsp?name=<%=name%>">Bad Customers</a></li>
						<li title = "Reward The Merchant Of The Month"><a href="reward.jsp?name=<%=name%>">Reward</a></li>
						<li><a href="customers.jsp?name=<%=name%>">Customers</a></li>
						<li><a href="all_transactions.jsp?name=<%=name%>">Transactions</a></li>
					</ul>
			</div>
		</nav>
						
        <% 
			ArrayList<Account> accounts = AccountsDB.getBadCustomers(); 
			if(accounts.size() > 0) {
		%>
			
			<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<th colspan="3">Transactions</th>
						</tr>

						<tr>
							<th>Customer</th>
							<th>Account ID</th>
							<th>Debt</th>
						</tr>

						<% for(int i = 0; i < accounts.size(); i++) { %>
							<tr>
								<td><%=accounts.get(i).getName()%> </td>
								<td><%=accounts.get(i).getAccountId()%></td>
								<td><%=accounts.get(i).getDebt()%></td>
							</tr>
						<% } %>

					 </tbody>
				</table>
		
		<%  } else { %>
			<div id="snackbar" style ="color:red">No Available Customers</div>

			<script>
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			</script>
		<%  }  %>
    </body>
	
</html>

