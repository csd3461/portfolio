<%-- 
    Document   : all_transactions
    Created on : 22 Ιαν 2017, 10:32:25 μμ
    Author     : NickSyn
--%>

<%@page import="model.*"%>
<%@page import="db.AccountsDB"%>
<%@page import="model.Account"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Transactions</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>
		
		<% String name = request.getParameter("name"); %>
		
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
						<a class="navbar-brand">CCC</a>
					</div>
					
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
						<li><a href="good_customers.jsp?name=<%=name%>">Good Customers</a></li>
						<li><a href="bad_customers.jsp?name=<%=name%>">Bad Customers</a></li>
						<li title = "Reward The Merchant Of The Month"><a href="reward.jsp?name=<%=name%>">Reward</a></li>
						<li><a href="customers.jsp?name=<%=name%>">Customers</a></li>
						<li class="active"><a>Transactions</a></li>
					</ul>
			</div>
		</nav>
						
        <% 
			ArrayList<Transaction> trans = AccountsDB.getTransactions();
			if(trans.size() == 0) {
		%>
				<div id="snackbar" style ="color:red">No Available Transaction</div>

				<script>
					var x = document.getElementById("snackbar")
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
		<% } else { %>
		
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<th colspan="5">Transactions</th>
					</tr>

					<tr>
						<th>Customer</th>
						<th>Merchant</th>
						<th>Date</th>
						<th>Amount</th>
						<th>Type</th>
					</tr>

					<%for(int i = 0; i < trans.size(); i++) { %>
						<tr>
							<td><%=trans.get(i).getCustomerName()%> </td>
							<td><%=trans.get(i).getMerchantName()%></td>
							<td><%=trans.get(i).getDate()%></td>
							<td><%=trans.get(i).getAmount()%></td>
							<td><%=trans.get(i).getType()%></td>
						</tr>
				<%  } %>

				</tbody>
			</table>
		
		<%  }  %>
    </body>
	
</html>

