<%--
    Document   : create_account.jsp
    Created on : Jan 11, 2017, 12:59:32 AM
    Author     : skerdbash
--%>

<%@page import="db.*" %>
<%@page import="model.*"%> 
<%@page import="java.util.Date"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - New Account</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>

		<% 
			String login_register = request.getParameter("login_register");
			String name = request.getParameter("name");
			Table table_name = AccountsDB.getTableName(name);
			String account_type = table_name.toString();
			boolean acountAdded = false;

			if(login_register.equals("register")) {
				if(table_name == Table.NONE) {
					account_type = request.getParameter("account_type");
					table_name = Table.valueOf(account_type);
					acountAdded = AccountsDB.addAccount(name, table_name);
				}
			}
			
			if(table_name == Table.COMPANY) {
				table_name = Table.EMPLOYEE;
				account_type = table_name.toString();
				name = name+"_admin";
			}
		%>
		
		<% if(name.equals("CCC")) { %>
			<nav class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand">CCC</a>
					</div>
					
					<ul class="nav navbar-nav">
						<li><a href="index.html">Log Out</a></li>
						<li><a href="good_customers.jsp?name=<%=name%>">Good Customers</a></li>
						<li><a href="bad_customers.jsp?name=<%=name%>">Bad Customers</a></li>
						<li title = "Reward The Merchant Of The Month"><a href="reward.jsp?name=<%=name%>">Reward</a></li>
						<li><a href="customers.jsp?name=<%=name%>">Customers</a></li>
						<li><a href="all_transactions.jsp?name=<%=name%>">Transactions</a></li>
					</ul>
				</div>
			</nav>
						
		<%
		   } else if((login_register.equals("register") && acountAdded == true) || 
				      login_register.equals("login") && table_name != Table.NONE) {
		%>
		
			<% if(acountAdded == true) { %> 
				<div id="snackbar">You have registered successfully</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>
				
			<% } else { %>
				<div id="snackbar">You have logged in successfully</div>

				<script>
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				</script>

			<% } %>
			
				<nav class="navbar navbar-inverse">
					<div class="container">
						<div class="navbar-header">
							<a class="navbar-brand">CCC</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="index.html">Log Out</a></li>
							<li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
							<% if(table_name != Table.MERCHANT) {%>
								<li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
								<li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
							<% } %>
								<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
								<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
							<li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
							<% if(table_name == Table.EMPLOYEE) {%>
								<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
							<% } %>
						</ul>
					</div>
				</nav>
						
			<%
				} else {
					if(login_register.equals("register")) {
			%>
			
						<div id="snackbar" style ="color:red">This account exist already</div>

						<script>
							var x = document.getElementById("snackbar")
							x.className = "show";
							setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
						</script>
				
					<% } else { %>
					
						<div id="snackbar" style ="color:red">There is no account with such name</div>

						<script>
							var x = document.getElementById("snackbar")
							x.className = "show";
							setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
						</script>

					<% } %>
				
				<nav class="navbar navbar-inverse">
					<div class="container">
						<div class="navbar-header">
							<a class="navbar-brand" href="index.html">CCC</a>
						</div>
						
						<div class="navbar-collapse collapse" >
							<ul class="nav navbar-nav">
								<li><a href="index.html">Log in</a></li>
								<li><a href="register.html">Register</a></li>
							</ul>
						</div>
					</div>
			    </nav> 
			<% } %>
			
			<center> <img src ="http://www.chaparral.org/files/Logos/CCC%20only%20Transparent.png" /> </center>
		
    </body>
</html>
