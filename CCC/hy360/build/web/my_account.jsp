<%--
    Document   : create_account.jsp
    Created on : Jan 11, 2017, 12:59:32 AM
    Author     : skerdbash
--%>


<%@page import="db.*" %>
<%@page import="model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - My Account </title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>


		<%
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = Table.valueOf(account_type);
		%>

		<nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" >CCC</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Log Out</a></li>
                    <li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
                    <% if(table_name != Table.MERCHANT) {%>
                        <li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
                        <li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
                    <% } %>
					<li><a href="transactions.jsp?name=<%=name%>&account_type=<%=account_type%>">Transactions</a></li>
					<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
					<li class="active"><a>My Account</a></li>
					<% if(table_name == Table.EMPLOYEE) {%>
						<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
					<% } %>
                </ul>
            </div>
        </nav>
		
		<table class="table table-striped table-bordered">
			<tbody>
				
				<tr>
					<th colspan="2">My Account</th>
				</tr>
				
				<tr>
					<td> Account Type </td>
					<td> <%= account_type %> </td>
				</tr>
				
				<% 
					if(table_name == Table.INDIVIDUAL) {
						Individual ind = AccountsDB.getIndividual(name); 
				%>	

					<tr>
						<td> Name </td>
						<td> <%= ind.getName()%> </td>
					</tr>

					<tr>
						<td> Account ID </td>
						<td> <%= ind.getAccountId()%>  </td>
					</tr>

					<tr>
						<td> Debt </td>
						<td> <%= ind.getDebt() %>  </td>
					</tr>

					<tr>
						<td> Expiration Date </td>
						<td> <%= ind.getExpirationDate() %>  </td>
					</<tr>

					<tr>
						<td> Credit </td>
						<td> <%= ind.getCredit() %>  </td>
					</tr>

					<tr>
						<td> Credit Limit </td>
						<td> <%= ind.getCreditLimit() %>  </td>
					</tr>


				<%
					} else if(table_name == Table.EMPLOYEE) {
						Employee emp = AccountsDB.getEmployee(name);
						Company com = AccountsDB.getCompany(emp.getCompanyName()); 
				%>	
				
					<tr>
						<td> Name </td>
						<td> <%= emp.getName()%> </td>
					</tr>
					
					<tr>
						<td> ID </td>
						<td> <%= emp.getId()%> </td>
					</tr>
					
					<tr>
						<td> Company </td>
						<td> <%= com.getName()%> </td>
					</tr>

					<tr>
						<td> Account No. </td>
						<td> <%= com.getAccountId()%>  </td>
					</tr>

					<tr>
						<td> Debt </td>
						<td> <%= com.getDebt() %>  </td>
					</tr>

					<tr>
						<td> Expiration Date </td>
						<td> <%= com.getExpirationDate() %>  </td>
					</<tr>

					<tr>
						<td> Credit </td>
						<td> <%= com.getCredit() %>  </td>
					</tr>

					<tr>
						<td> Credit Limit </td>
						<td> <%= com.getCreditLimit() %>  </td>
					</tr>



				<%
					}else if(table_name == Table.MERCHANT) {
						Merchant merch = AccountsDB.getMerchant(name); 
				%>	
					<tr>
						<td> Name </td>
						<td> <%= merch.getName()%> </td>
					</tr>

					<tr>
						<td> Account No. </td>
						<td> <%= merch.getAccountId()%>  </td>
					</tr>

					<tr>
						<td> Debt </td>
						<td> <%= merch.getDebt() %>  </td>
					</tr>

					<tr>
						<td> Profit </td>
						<td> <%= merch.getProfit() %>  </td>
					</<tr>

					<tr>
						<td> Commission </td>
						<td> <%= merch.getCommision() %>  </td>
					</tr>

				<% } %>
			
			</tbody>
		</table>
    </body>
</html>
