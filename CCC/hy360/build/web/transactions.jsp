<%-- 
    Document   : transactions
    Created on : 20 Ιαν 2017, 1:16:57 μμ
    Author     : NickSyn
--%>

<%@page import="model.Table.*"%>
<%@page import="db.*" %>
<%@page import="model.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CCC - Transactions</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="shortcut icon" href="http://i.imgur.com/qLdDHLz.gif">
        <link rel="stylesheet" href="style.css" />
    </head>
	
    <body>

		<%
			String interval = request.getParameter("interval");
			int INTERVAL = 0;
			if(interval != null){
				INTERVAL = Integer.parseInt(interval);
			}
			String name = request.getParameter("name");
			String account_type = request.getParameter("account_type");
			Table table_name = AccountsDB.getTableName(name);
			
			String tmp = name;
		%>

		<nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" >CCC</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Log Out</a></li>
                    <li><a href="delete_account.jsp?name=<%=name%>&account_type=<%=account_type%>">Delete Account</a></li>
                    <% if(table_name != Table.MERCHANT) {%>
                        <li><a href="new_transaction.jsp?name=<%=name%>&account_type=<%=account_type%>">New Transaction</a></li>
                        <li><a href="refund.jsp?name=<%=name%>&account_type=<%=account_type%>">Get A Refund</a></li>
                    <% } %>
					<li class="active"><a>Transactions</a></li>
					<li><a href="pay_debt.jsp?name=<%=name%>&account_type=<%=account_type%>">Pay Debt</a></li>
                    <li><a href="my_account.jsp?name=<%=name%>&account_type=<%=account_type%>">My Account</a></li>
					<% if(table_name == Table.EMPLOYEE) {%>
						<li><a href="employees.jsp?name=<%=name%>&account_type=<%=account_type%>">Employees</a></li>
					<% } %>

                </ul>
            </div>
        </nav>
		<% if(INTERVAL != 0) { %>
		
			<% if(table_name == Table.MERCHANT) { %>

				<% 
					ArrayList<Transaction> trans = AccountsDB.getTransactionsOf(name, INTERVAL);
					if(trans == null || trans.size() == 0) {
				%>
						<div id="snackbar" style = "color:red" >You haven't made any transaction in the last <%=INTERVAL%> minutes</div>

						<script>
							var x = document.getElementById("snackbar");
							x.className = "show";
							setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
						</script>
				<%  } %>

				<% if(trans != null && trans.size() > 0) { %>
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<th colspan="4">Transactions</th>
								</tr>

								<tr>
									<th>Customer</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Type</th>
								</tr>

								<% for(int i = 0; i < trans.size(); i++) { %>
									<tr>
										<td><%=trans.get(i).getCustomerName()%> </td>
										<td><%=trans.get(i).getDate()%></td>
										<td><%=trans.get(i).getAmount()%></td>
										<td><%=trans.get(i).getType()%></td>
									</tr>
								<% } %>

							</tbody>
						</table>

				<% } %>

			<% } else { %>

				<%	
					ArrayList<Transaction> trans = AccountsDB.getTransactionsOf(name, INTERVAL);

					if(request.getParameter("employee") != null) {
						name = request.getParameter("employee");
						trans = AccountsDB.getTransactionsOf(name, INTERVAL);
					}

					if(trans == null || trans.size() == 0) {
				%>
					<div id="snackbar" style = "color:red" >You haven't made any transactions in the last <%=INTERVAL%> minutes</div>

					<script>
						var x = document.getElementById("snackbar");
						x.className = "show";
						setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
					</script>
				<% } %>

				<% if(trans != null && trans.size() > 0) { %>
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<th colspan="<%=(table_name == Table.EMPLOYEE)?5:4%>">Transactions</th>
								</tr>

								<tr>
									<% if(table_name == Table.EMPLOYEE) { %>
											<th>Employee</th>
									<%  } %>
									<th>Merchant</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Type</th>
								</tr>

								<% for(int i = 0; i < trans.size(); i++) { %>
									<tr>
										<% if(table_name == Table.EMPLOYEE) { %>
												<td><%=trans.get(i).getCustomerName()%></td>
										<%  } %>
										<td><%=trans.get(i).getMerchantName()%> </td>
										<td><%=trans.get(i).getDate()%></td>
										<td><%=trans.get(i).getAmount()%></td>
										<td><%=trans.get(i).getType()%></td>
									</tr>
								<% } %>

							 </tbody>
						</table>
				<%  } %>			

			<% } %>
		
		<% } %>
		
		<% name = tmp; %>
		<form action="transactions.jsp" name=transactions method="GET">
			<input type="text" name="name" value = "<%=name%>" hidden>
			<input type="text" name="account_type" value = "<%=account_type%>" hidden>
				<div class="form-inline">
					<% 
						if(name.length() - 6 > 0 && name.substring(name.length() - 6).equals("_admin")) { %>
						<select class="form-control" id = "employee" name = "employee" required>
								<option value="" selected>Select Employee</option>
								<option value="<%=AccountsDB.getOwnerAccountName(name)%>">All</option>
								<%  
									ArrayList<Employee> emps = AccountsDB.getEmployeesByCompanyName(name.substring(0, name.length() - 6));
									for(int i = 0; i < emps.size(); i++) { %>
										<option value="<%= emps.get(i).getName()%>"><%=emps.get(i).getName() %></option>
								<% } %>
						</select>
					<%  }  %>
					<select class="form-control" id = "interval" name = "interval" required>
							<option value="" selected>Minutes Passed</option>
							<option value="1">1 Minute</option>
							<option value="5">5 Minutes</option>
							<option value="10">10 Minutes</option>
							<option value="375">40 + 45 * e^2 + log(150) Minutes</option>
					</select>
					<input class="btn btn-primary" style="margin:1em;" type="submit" value="DONE">
				</div>
		</form>	
	
    </body>
</html>
