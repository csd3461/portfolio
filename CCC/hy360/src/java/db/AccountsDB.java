package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;

/**
 *
 * @author skerdbash
 */
public class AccountsDB {
	
	private final static int INDIVIDUAL_CREDIT_LIMIT = 4000;
	private final static int INITIAL_INDIVIDUAL_CREDIT = INDIVIDUAL_CREDIT_LIMIT;
	private final static int COMPANY_CREDIT_LIMIT = 20000;
    private	final static int INITIAL_COMPANY_CREDIT = COMPANY_CREDIT_LIMIT;
	private final static int COMMISION = 20;
	private final static int INITIAL_DEBT = 0;
	private final static int INITIAL_PROFIT = 0;
	
	
	/**
	 * 
	 * Adds new Customer to database
	 * Account name shouldn't already exist in the database
	 * @param name
	 * @param table_name
	 * @return True if addition is successful, else returns false
	 * @throws java.lang.ClassNotFoundException 
	 * @throws java.sql.SQLException 
	 */
	public static boolean addAccount(String name, Table table_name) throws ClassNotFoundException, SQLException {
		
		/* Generating account_id */
		Random rand = new Random(); 
		int id = rand.nextInt(900000000) + 1000000000;
		String account_id = "CCC" + id;

		/* Generating expiration_date */
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 5);
		Date date = cal.getTime();
		String expiration_date = dateFormat.format(date);
		
		try {
			try (Connection con = DB.getConnection();
					Statement stmt = con.createStatement()) {

				StringBuilder insQuery = new StringBuilder();

				switch (table_name) {
					case INDIVIDUAL:
						Individual indi = new Individual(name, account_id,
								expiration_date, INDIVIDUAL_CREDIT_LIMIT,
								INITIAL_INDIVIDUAL_CREDIT, INITIAL_DEBT);
						insQuery.append("INSERT INTO ")
								.append(" INDIVIDUAL (NAME, ACCOUNT_ID, EXPIRATION_DATE, CREDIT_LIMIT, CREDIT, DEBT)")
								.append(" VALUES (")
								.append("'").append(indi.getName()).append("',")
								.append("'").append(indi.getAccountId()).append("',")
								.append("'").append(indi.getExpirationDate()).append("',")
								.append("'").append(indi.getCreditLimit()).append("',")
								.append("'").append(indi.getCredit()).append("',")
								.append("'").append(indi.getDebt()).append("');");
						stmt.executeUpdate(insQuery.toString());
						break;
					case COMPANY:
						Company comp = new Company(name, account_id,
								expiration_date, COMPANY_CREDIT_LIMIT,
								INITIAL_COMPANY_CREDIT, INITIAL_DEBT);
						insQuery.append("INSERT INTO ")
								.append(" COMPANY (NAME, ACCOUNT_ID, EXPIRATION_DATE, CREDIT_LIMIT, CREDIT, DEBT)")
								.append(" VALUES (")
								.append("'").append(comp.getName()).append("',")
								.append("'").append(comp.getAccountId()).append("',")
								.append("'").append(comp.getExpirationDate()).append("',")
								.append("'").append(comp.getCreditLimit()).append("',")
								.append("'").append(comp.getCredit()).append("',")
								.append("'").append(comp.getDebt()).append("');");
						stmt.executeUpdate(insQuery.toString());
						rand = new Random();
						id = rand.nextInt(900000000) + 1000000000;
						String personal_id = "PID" + id;
						Employee emp = new Employee(comp.getName() + "_admin", personal_id, comp.getName());
						insQuery = new StringBuilder();
						insQuery.append("INSERT INTO ")
								.append(" EMPLOYEE (NAME, ID, COMPANY_NAME)")
								.append(" VALUES (")
								.append("'").append(emp.getName()).append("',")
								.append("'").append(emp.getId()).append("',")
								.append("'").append(emp.getCompanyName()).append("');");
						stmt.executeUpdate(insQuery.toString());
						rand = new Random();
						id = rand.nextInt(900000000) + 1000000000;
						personal_id = "PID" + id;
						emp = new Employee(comp.getName() + "_emp1", personal_id, comp.getName());
						insQuery = new StringBuilder();
						insQuery.append("INSERT INTO ")
								.append(" EMPLOYEE (NAME, ID, COMPANY_NAME)")
								.append(" VALUES (")
								.append("'").append(emp.getName()).append("',")
								.append("'").append(emp.getId()).append("',")
								.append("'").append(emp.getCompanyName()).append("');");
						stmt.executeUpdate(insQuery.toString());
						break;
					case MERCHANT:
						Merchant merch = new Merchant(name, account_id,
								COMMISION, INITIAL_PROFIT, INITIAL_DEBT);
						insQuery.append("INSERT INTO ")
								.append(" MERCHANT (NAME, ACCOUNT_ID, COMMISION, PROFIT, DEBT)")
								.append(" VALUES (")
								.append("'").append(merch.getName()).append("',")
								.append("'").append(merch.getAccountId()).append("',")
								.append("'").append(merch.getCommision()).append("',")
								.append("'").append(merch.getProfit()).append("',")
								.append("'").append(merch.getDebt()).append("');");
						stmt.executeUpdate(insQuery.toString());
						break;
					default:
						break;
				}

				System.out.println("#DB: The member was successfully added in the database.");

				// Close connection
				stmt.close();
				con.close();

			}

		} catch (SQLException ex) {
			// Log exception
			Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		
		return true;
		
    }
	
	/**
	 * 
	 * Adds new Employee to database
	 * Account name shouldn't already exist in the database
	 * @param company_name
	 * @param employee_name
	 * @return True if addition is successful, else returns false
	 * @throws java.lang.ClassNotFoundException 
	 * @throws java.sql.SQLException 
	 */
	public static boolean addEmployee(String company_name, String employee_name) throws ClassNotFoundException, SQLException {
		
		try {
			try (Connection con = DB.getConnection();
					Statement stmt = con.createStatement()) {

				StringBuilder insQuery;
				Random rand = new Random(); 
				int id = rand.nextInt(900000000) + 1000000000;
				String personal_id = "PID" + id;
				Employee emp = new Employee(company_name + "_" + employee_name, personal_id, company_name);
				insQuery = new StringBuilder();
				insQuery.append("INSERT INTO ")
						.append(" EMPLOYEE (NAME, ID, COMPANY_NAME)")
						.append(" VALUES (")
						.append("'").append(emp.getName()).append("',")
						.append("'").append(emp.getId()).append("',")
						.append("'").append(emp.getCompanyName()).append("');");
				stmt.executeUpdate(insQuery.toString());
				
				System.out.println("#DB: The member was successfully added in the database.");

				// Close connection
				stmt.close();
				con.close();

			}

		} catch (SQLException ex) {
			// Log exception
			Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		
		return true;
		
    }
	
	/**
	 * 
	 * Deletes Customer from database
	 * @param name
	 * @return True if deletion is successful, else returns false
	 * @throws java.lang.ClassNotFoundException
	 */
	public static boolean deleteAccount(String name) throws ClassNotFoundException  {

        return deleteAccount(getOwnerAccountName(name), getOwnerTableName(name));
		
    }
	
	/**
	 * 
	 * Deletes Customer from database
	 * @param name
	 * @param table_name
	 * @return True if deletion is successful, else returns false
	 * @throws java.lang.ClassNotFoundException
	 */
	public static boolean deleteAccount(String name, Table table_name) throws ClassNotFoundException {

        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;
				
				if(getDebt(name, table_name) != 0) {
					return false;
				}
				
				if(table_name == Table.COMPANY) {
					deleteEmployees(name);
				}

				insQuery = new StringBuilder();
                insQuery.append("DELETE FROM ")
						.append(table_name)
                        .append(" WHERE ")
                        .append(" NAME = ").append("'").append(name).append("';");
				stmt.executeUpdate(insQuery.toString());
				
                System.out.println("#DB: The member was successfully deleted from the database.");

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(AccountsDB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
    }
	
	/**
	 * 
	 * Deletes all the employees of the company from the database
	 * @param company_name
	 * @return True if deletion is successful, else false
	 * @throws ClassNotFoundException 
	 */
	private static boolean deleteEmployees(String company_name) throws ClassNotFoundException {
		
		try {
			try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;

				insQuery = new StringBuilder();
                insQuery.append("DELETE FROM EMPLOYEE")
                        .append(" WHERE ")
                        .append(" COMPANY_NAME = ").append("'").append(company_name).append("';");
				stmt.executeUpdate(insQuery.toString());

                System.out.println("#DB: The member was successfully deleted from the database.");

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(AccountsDB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		 
		return true;
		
	}
	
	/**
	 * 
	 * Returns an Individual by that name, if he exists in the database
	 * @param name
	 * @return An Individual by that name, if he exists in the database, else null
	 * @throws ClassNotFoundException 
	 */
	public static Individual getIndividual(String name) throws ClassNotFoundException {
		
		Individual indi = null;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM INDIVIDUAL ")
                        .append(" WHERE ")
                        .append(" NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if (res.next() == true) {
					indi = new Individual(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getString("EXPIRATION_DATE"), res.getInt("CREDIT_LIMIT"),
							res.getInt("CREDIT"), res.getInt("DEBT"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }

        return indi;
		
	}
	
	/**
	 * 
	 * Returns a Company by that name, if it exists in the database
	 * @param name
	 * @return A Company by that name, if it exists in the database, else null
	 * @throws ClassNotFoundException 
	 */
	public static Company getCompany(String name) throws ClassNotFoundException {
		
		Company comp = null;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM COMPANY ")
                        .append(" WHERE ")
                        .append(" NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if (res.next() == true) {
					comp = new Company(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getString("EXPIRATION_DATE"), res.getInt("CREDIT_LIMIT"),
							res.getInt("CREDIT"), res.getInt("DEBT"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }

        return comp;
		
	}
	
	/**
	 * 
	 * Returns an Employee by that name, if he exists in the database
	 * @param name
	 * @return An Employee by that name, if he exists in the database, else null
	 * @throws ClassNotFoundException 
	 */
	public static Employee getEmployee(String name) throws ClassNotFoundException {
		
		Employee emp = null;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM EMPLOYEE ")
                        .append(" WHERE ")
                        .append(" NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if (res.next() == true) {
					emp = new Employee(res.getString("NAME"), res.getString("ID"),
							res.getString("COMPANY_NAME"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }

        return emp;
		
	}

	/**
	 * 
	 * Returns a Merchant by that name, if he exists in the database
	 * @param name
	 * @return A Merchant by that name, if he exists in the database, else null
	 * @throws ClassNotFoundException 
	 */
	public static Merchant getMerchant(String name) throws ClassNotFoundException {
		
		Merchant merch = null;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM MERCHANT ")
                        .append(" WHERE ")
                        .append(" NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if (res.next() == true) {
					merch = new Merchant(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getInt("COMMISION"), res.getInt("PROFIT"), res.getInt("DEBT"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }

        return merch;
		
	}
	
	/**
	 * 
	 * Returns a list with all the existing Individuals in the database
	 * @return A list with all the individuals
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Individual> getIndividuals() throws ClassNotFoundException {
		
		ArrayList<Individual>  individuals = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM INDIVIDUAL;");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					individuals.add(new Individual(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getString("EXPIRATION_DATE"), res.getInt("CREDIT_LIMIT"),
							res.getInt("CREDIT"), res.getInt("DEBT")));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return individuals;
		
	}
	
	/**
	 * 
	 * Returns a list with all the existing Companies in the database
	 * @return A list with all the companies
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Company> getCompanies() throws ClassNotFoundException {
		
		ArrayList<Company>  companies = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM COMPANY;");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					companies.add(new Company(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getString("EXPIRATION_DATE"), res.getInt("CREDIT_LIMIT"),
							res.getInt("CREDIT"), res.getInt("DEBT")));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return companies;
		
	}
	
	/**
	 * 
	 * Returns a list with all the existing Employees in the database
	 * @return A list with all the employees
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Employee> getEmployees() throws ClassNotFoundException {
		
		ArrayList<Employee>  employees = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM EMPLOYEE;");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					employees.add(new Employee(res.getString("NAME"), res.getString("ID"),
							res.getString("COMPANY_NAME")));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return employees;
		
	}
	
	/**
	 * 
	 * Returns a list with all the Employees of a specific Company
	 * @param name 
	 * @return A list with all the employees 
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Employee> getEmployeesByCompanyName(String name) throws ClassNotFoundException {
		
		ArrayList<Employee>  employees = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM EMPLOYEE")
						.append(" WHERE ")
                        .append(" COMPANY_NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					employees.add(new Employee(res.getString("NAME"), res.getString("ID"),
							res.getString("COMPANY_NAME")));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return employees;
		
	}
	
	/**
	 * 
	 * Returns a list with all the existing Merchants in the database
	 * @return A list with all the merchants
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Merchant> getMerchants() throws ClassNotFoundException {
		
		ArrayList<Merchant>  merchants = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM MERCHANT;");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					merchants.add(new Merchant(res.getString("NAME"),
							res.getString("ACCOUNT_ID"), res.getInt("COMMISION"),
							res.getInt("PROFIT"), res.getInt("DEBT")));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return merchants;
		
	}
	
	/**
	 * 
	 * Returns a list with all the existing Transactions in the database
	 * @return A list with all the transactions
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static ArrayList<Transaction> getTransactions() throws ClassNotFoundException {
		
		ArrayList<Transaction>  transactions = new ArrayList<>();
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("SELECT * FROM TRANSACTION;");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                while(res.next() == true) {
					transactions.add(new Transaction(res.getString("CUSTOMER_NAME"),
							res.getString("MERCHANT_NAME"), res.getString("DATE"),
							res.getInt("AMOUNT"), Payment.valueOf(res.getString("TYPE"))));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return transactions;
		
	}
	
	// TODO - OK UNTIL HERE
	
	/**
	 * 
	 * Returns true if the purchase is successful, else returns false
	 * @param customer_name
	 * @param merchant_name
	 * @param amount
	 * @param type
	 * @return Returns true if purchase is successful, else false 
	 * @throws ClassNotFoundException 
	 */
	public static boolean makePurchase(String customer_name, String merchant_name, 
			int amount, Payment type) throws ClassNotFoundException {
		
		return makePurchase(customer_name, merchant_name, amount, type, getOwnerTableName(customer_name));
		
	}
	
	/**
	 * 
	 * Returns true if the purchase is successful, else returns false
	 * @param customer_name
	 * @param merchant_name
	 * @param amount
	 * @param type
	 * @param customer_table
	 * @return True if the purchase is successful, else false
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static boolean makePurchase(String customer_name, String merchant_name, 
			int amount, Payment type, Table customer_table) throws ClassNotFoundException {

		/* Generating expiration_date */
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		String transaction_date = dateFormat.format(date);
		
		String customer_account = customer_name;
		
		try {
			try (Connection con = DB.getConnection();
					Statement stmt = con.createStatement()) {

				StringBuilder insQuery;
				
				if(customer_table == Table.COMPANY) {
					customer_account = getEmployee(customer_name).getCompanyName();
				}
				
				if(getCredit(customer_account, customer_table) < amount) {
					return false;
				} 

				/* Update customers data */
				if(type == Payment.CREDIT) {
					changeDebt(customer_account, amount, customer_table);
				}else {
					changeCredit(customer_account, -amount, customer_table);
				}
				
				/* Update merchants data */
				changeProfit(merchant_name, amount);
				changeDebt(merchant_name, getMerchant(merchant_name).getCommision(), Table.MERCHANT);
				
				Transaction trans = new Transaction(customer_name, merchant_name,
						transaction_date, amount, type);

				/* Adds new transaction in the TRANSACTION table */
				insQuery = new StringBuilder();
				insQuery.append("INSERT INTO ")
						.append(" TRANSACTION (CUSTOMER_NAME, MERCHANT_NAME, DATE, AMOUNT, TYPE)")
						.append(" VALUES (")
						.append("'").append(trans.getCustomerName()).append("',")
						.append("'").append(trans.getMerchantName()).append("',")
						.append("'").append(trans.getDate()).append("',")
						.append("'").append(trans.getAmount()).append("',")
						.append("'").append(trans.getType()).append("');");

				stmt.executeUpdate(insQuery.toString());
				System.out.println("#DB: The member was successfully added in the database.");

				// Close connection
				stmt.close();
				con.close();

			}

		} catch (SQLException ex) {
			// Log exception
			Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * Returns true if the refund is successful, else returns false
	 * @param date
	 * @return True if the refund is successful, else false
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static boolean makeRefund(String date) throws ClassNotFoundException {
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;
				ResultSet res;
						
				insQuery = new StringBuilder();
				insQuery.append("SELECT * FROM TRANSACTION ")
                        .append(" WHERE DATE = ").append("'").append(date).append("';");

                stmt.execute(insQuery.toString());
				res = stmt.getResultSet();
				
				if(res.next() == true) {
					
					Transaction trans = new Transaction(res.getString("CUSTOMER_NAME"),
							res.getString("MERCHANT_NAME"), res.getString("DATE"),
							res.getInt("AMOUNT"), Payment.valueOf(res.getString("TYPE")));
					
					makePurchase(trans.getCustomerName(), trans.getMerchantName(), -trans.getAmount(), trans.getType());
					
				}
				
				deleteTransaction(date);

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
	}
	
	/**
	 * Deletes the transaction with the given date from the database
	 * @param date
	 * @return True if deletion is successful, else false
	 * @throws ClassNotFoundException 
	 */
	private static boolean deleteTransaction(String date) throws ClassNotFoundException {
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;

				insQuery = new StringBuilder();
                insQuery.append("DELETE FROM TRANSACTION ")
                        .append(" WHERE DATE = ").append("'").append(date).append("';");
				stmt.executeUpdate(insQuery.toString());
				
                System.out.println("#DB: The member was successfully deleted from the database.");

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(AccountsDB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
	}
	
	/**
	 * 
	 * Returns true if the payment is successful, else returns false
	 * @param name
	 * @param amount
	 * @return True if the payment is successful, else false
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static boolean payDebt(String name, int amount) throws ClassNotFoundException {

        return (changeDebt(name, -amount) && changeCredit(name, -amount));
		
    }
	
	/**
	 * 
	 * Returns true if the payment is successful, else returns false
	 * @param name
	 * @param amount
	 * @param table_name
	 * @return True if the payment is successful, else false
	 * @throws java.lang.ClassNotFoundException 
	 */
	public static boolean payDebt(String name, int amount, Table table_name) throws ClassNotFoundException {

        return (changeDebt(name, -amount, table_name) && changeCredit(name, -amount, table_name));
		
    }
	
	/**
	 * 
	 * Returns all the transactions made by the account with that name
	 * @param name
	 * @return A list of all the Transactions made by the account with that name
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Transaction> getTransactionsOf(String name) throws ClassNotFoundException {
		
		return getTransactionsOf(name, 0);
		
	}
	
	
	/**
	 * 
	 * Returns all the transactions made by the account with that name, in the given period
	 * @param name
	 * @param interval
	 * @return A list of all the Transactions made by the account with that name, in the given period
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Transaction> getTransactionsOf(String name, int interval) throws ClassNotFoundException {
		
		return getTransactionsOf(name, interval, getTableName(name));
		
	}
	
	/**
	 * 
	 * Returns all the transactions made by the account with that name, in the given period
	 * @param name
	 * @param interval
	 * @param table_name
	 * @return A list of all the Transactions made by the account with that name, in the given period
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Transaction> getTransactionsOf(String name, int interval, Table table_name) throws ClassNotFoundException {
		
		ArrayList<Transaction> transactions = new ArrayList<>();
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;
				
				switch (table_name) {
					case COMPANY:
						insQuery = new StringBuilder();
						insQuery.append("SELECT * FROM TRANSACTION, EMPLOYEE")
								.append(" WHERE EMPLOYEE.COMPANY_NAME = ").append("'").append(name).append("'")
								.append(" AND EMPLOYEE.NAME = TRANSACTION.CUSTOMER_NAME");
						break;
					case MERCHANT:
						insQuery = new StringBuilder();
						insQuery.append("SELECT * FROM TRANSACTION ")
								.append(" WHERE MERCHANT_NAME = ").append("'").append(name).append("'");
						break;
					case INDIVIDUAL:
					case EMPLOYEE:
						insQuery = new StringBuilder();
						insQuery.append("SELECT * FROM TRANSACTION ")
								.append(" WHERE CUSTOMER_NAME = ").append("'").append(name).append("'");
						break;
					default:
						return null;
				}
				
				if(interval == 0) {
					insQuery.append(" AND TRANSACTION.AMOUNT > 0;");
				}else {
					insQuery.append(" AND TRANSACTION.DATE > NOW() - INTERVAL ")
					.append(interval).append(" MINUTE;");
				}

				stmt.execute(insQuery.toString());
				
				ResultSet res = stmt.getResultSet();
				
				while(res.next() == true) {
					System.out.println("OK");
					transactions.add(new Transaction(res.getString("CUSTOMER_NAME"),
							res.getString("MERCHANT_NAME"), res.getString("DATE"),
							res.getInt("AMOUNT"), Payment.valueOf(res.getString("TYPE"))));
				}
				
                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return transactions;
		
	}
	
	/**
	 * 
	 * Returns all the accounts that have no debt 
	 * @return A list of all the accounts with no debt
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Account> getGoodCustomers() throws ClassNotFoundException {
		
		ArrayList<Account> accounts = new ArrayList<>();
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("SELECT NAME, ACCOUNT_ID FROM INDIVIDUAL WHERE DEBT = 0 ")
                        .append(" UNION ALL SELECT NAME, ACCOUNT_ID FROM COMPANY WHERE DEBT = 0 ")
						.append(" UNION ALL SELECT NAME, ACCOUNT_ID FROM MERCHANT WHERE DEBT = 0;");

                stmt.execute(insQuery.toString());
				
				ResultSet res = stmt.getResultSet();
				
				while(res.next() == true) {
					accounts.add(new Account(res.getString("NAME"), res.getString("ACCOUNT_ID"), 0));
				}

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return accounts;
		
	}
	
	/**
	 * 
	 * Returns all the accounts that have debt, by descending order 
	 * @return A list of all the accounts that have debt, by descending order
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Account> getBadCustomers() throws ClassNotFoundException {
		
		ArrayList<Account> accounts = new ArrayList<>();
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("SELECT * FROM (")
						.append(" SELECT NAME, ACCOUNT_ID, DEBT FROM INDIVIDUAL WHERE DEBT > 0 ")
                        .append(" UNION ALL SELECT NAME, ACCOUNT_ID, DEBT FROM COMPANY WHERE DEBT > 0 ")
						.append(" UNION ALL SELECT NAME, ACCOUNT_ID, DEBT FROM MERCHANT WHERE DEBT > 0")
						.append(" ) ACCOUNT ORDER BY DEBT DESC;");

                stmt.execute(insQuery.toString());
				
				ResultSet res = stmt.getResultSet();
				
				while(res.next() == true) {
					accounts.add(new Account(res.getString("NAME"), res.getString("ACCOUNT_ID"), res.getInt("DEBT")));
				}

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return accounts;
		
	}
	
	
	// TODO - CHECK THIS IN THE END
	
	/**
	 * 
	 * Returns the Merchants with the most transactions for the month
	 * @return A list of the merchants of the month
	 * @throws ClassNotFoundException 
	 */
	public static ArrayList<Merchant> getMerchantsOfTheMonth() throws ClassNotFoundException {
		
		ArrayList<Merchant> merchants = new ArrayList<>();
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("SELECT * FROM")
						.append(" (SELECT * , COUNT(MERCHANTS.NAME) AS TRANSACTIONS")
						.append(" FROM (")
						.append(" SELECT MERCHANT.* FROM TRANSACTION, MERCHANT ")
						.append(" WHERE MERCHANT.NAME = TRANSACTION.MERCHANT_NAME")
						.append(" AND TRANSACTION.DATE > NOW() - INTERVAL 1 MONTH")
						.append(" ) MERCHANTS GROUP BY MERCHANTS.NAME) TOP_MERCHANTS")
						.append(" WHERE TOP_MERCHANTS.TRANSACTIONS = ")
						.append(" (SELECT COUNT(MERCHANTS.NAME) AS TRANSACTIONS")
						.append(" FROM (")
						.append(" SELECT MERCHANT.* FROM TRANSACTION, MERCHANT ")
						.append(" WHERE MERCHANT.NAME = TRANSACTION.MERCHANT_NAME")
						.append(" AND TRANSACTION.DATE > NOW() - INTERVAL 1 MONTH")
						.append(" ) MERCHANTS GROUP BY MERCHANTS.NAME ORDER BY TRANSACTIONS DESC LIMIT 1)");
						
                stmt.execute(insQuery.toString());
				
				ResultSet res = stmt.getResultSet();
				
				while(res.next() == true) {
					merchants.add(new Merchant(res.getString("NAME"), res.getString("ACCOUNT_ID"),
							res.getInt("COMMISION"), res.getInt("PROFIT"), res.getInt("DEBT")));
				}

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return merchants;
		
	}
	
	/**
	 * 
	 * Rewards the Merchant of the month by decreasing his debt by 5%
	 * @param name
	 * @return True if the action is successful, else false
	 * @throws ClassNotFoundException 
	 */
	public static boolean rewardMerchantOfTheMonth(String name) throws ClassNotFoundException {
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				int debt = getMerchant(name).getDebt();
				
				if(debt < 20) {
					insQuery.append("UPDATE MERCHANT SET DEBT = 0 ")
						.append(" WHERE NAME =").append("'").append(name).append("';");
				}else if(debt == 0) {
					insQuery.append("UPDATE MERCHANT SET PROFIT = PROFIT + 10 ")
						.append(" WHERE NAME =").append("'").append(name).append("';");
				}else {
					insQuery.append("UPDATE MERCHANT SET DEBT = DEBT - DEBT/20 ")
						.append(" WHERE NAME =").append("'").append(name).append("';");
				}
				
                stmt.executeUpdate(insQuery.toString());

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
	}
	
	/**
	 * 
	 * Changes the profit of the merchant with that name by adding the given amount
	 * @param name
	 * @param amount
	 * @return True if change is successful, else false
	 * @throws ClassNotFoundException 
	 */
	public static boolean changeProfit(String name, int amount) throws ClassNotFoundException {

        try {
            try (Connection con = DB.getConnection();
					Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("UPDATE MERCHANT ")
                        .append(" SET ")
                        .append(" PROFIT = PROFIT+").append(amount)
                        .append(" WHERE NAME = ").append("'").append(name).append("';");

                stmt.executeUpdate(insQuery.toString());

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
    }
	
	/**
	 * 
	 * Returns the profit of the merchant by that name
	 * @param name
	 * @return If an account by that name exists, returns it's debt, else returns 0
	 * @throws ClassNotFoundException 
	 */
	public static int getProfit(String name) throws ClassNotFoundException {
		
		int profit = 0;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("SELECT PROFIT FROM MERCHANT")
						.append(" WHERE ")
						.append(" NAME = ").append("'").append(name).append("';");	

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if(res.next() == true) {
					profit  = Integer.parseInt(res.getString("PROFIT"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return profit;
	}
	
	/**
	 * 
	 * Changes the debt of the account with that name by adding the given amount
	 * @param name
	 * @param amount
	 * @return True if change is successful, else false
	 * @throws ClassNotFoundException 
	 */
	public static boolean changeDebt(String name, int amount) throws ClassNotFoundException {
        
		return changeDebt(getOwnerAccountName(name), amount, getOwnerTableName(name));
		
    }
	
	/**
	 * 
	 * Changes the debt of the account with that name by adding the given amount
	 * @param name
	 * @param amount
	 * @param table_name
	 * @return True if change is successful, else false
	 * @throws ClassNotFoundException 
	 */
	private static boolean changeDebt(String name, int amount, Table table_name) throws ClassNotFoundException {

        try {
            try (Connection con = DB.getConnection();
					Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("UPDATE ")
						.append(table_name)
                        .append(" SET ")
                        .append(" DEBT = DEBT+").append(amount)
                        .append(" WHERE NAME = ").append("'").append(name).append("';");

                stmt.executeUpdate(insQuery.toString());

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
    }
	
	/**
	 * 
	 * Returns the debt of the account by that name
	 * @param name
	 * @return If an account by that name exists, returns it's debt, else returns 0
	 * @throws ClassNotFoundException 
	 */
	public static int getDebt(String name) throws ClassNotFoundException {
			
		return getDebt(getOwnerAccountName(name), getOwnerTableName(name));
		
	}
	
	/**
	 * 
	 * Returns the debt of the account by that name
	 * @param name
	 * @param table_name
	 * @return If an account by that name exists, returns it's debt, else returns 0
	 * @throws ClassNotFoundException 
	 */
	private static int getDebt(String name, Table table_name) throws ClassNotFoundException {
		
		int debt = 0;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("SELECT DEBT FROM ")
						.append(table_name)
						.append(" WHERE ")
						.append(" NAME = ").append("'").append(name).append("';");	

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();

                if(res.next() == true) {
					debt  = Integer.parseInt(res.getString("DEBT"));
                }

                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
            // Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        return debt;
	}
	
	/**
	 * 
	 * Changes the credit of the account with that name by adding the given amount
	 * @param name
	 * @param amount
	 * @return True if change is successful, else false
	 * @throws ClassNotFoundException 
	 */
	public static boolean changeCredit(String name, int amount) throws ClassNotFoundException {

		return changeCredit(getOwnerAccountName(name), amount, getOwnerTableName(name));
		
    }
	
	/**
	 * 
	 * Changes the credit of the account with that name by adding the given amount
	 * @param name
	 * @param amount
	 * @param table_name
	 * @return True if change is successful, else false
	 * @throws ClassNotFoundException 
	 */
	private static boolean changeCredit(String name, int amount, Table table_name) throws ClassNotFoundException {

        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();
				
				insQuery.append("UPDATE ")
						.append(table_name)
                        .append(" SET ")
                        .append(" CREDIT = CREDIT+").append(amount)
                        .append(" WHERE NAME = ").append("'").append(name).append("';");

                stmt.executeUpdate(insQuery.toString());

                // Close connection
                stmt.close();
                con.close();
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
        }
		
		return true;
		
    }
	
	/**
	 * 
	 * Returns the credit of the account by that name
	 * @param name
	 * @return If an account by that name exists, returns it's credit, else returns 0
	 * @throws ClassNotFoundException 
	 */
	public static int getCredit(String name) throws ClassNotFoundException {
		
		return getCredit(getOwnerAccountName(name), getOwnerTableName(name));
		
	}
	
	
	/**
	 * 
	 * Returns the credit of the account by that name
	 * @param name
	 * @param table_name
	 * @return If an account by that name exists, returns it's credit, else returns 0
	 * @throws ClassNotFoundException 
	 */
	private static int getCredit(String name, Table table_name) throws ClassNotFoundException {
		
		int credit = 0;
		
        try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery = new StringBuilder();

				insQuery.append("SELECT CREDIT FROM ")
						.append(table_name)
						.append(" WHERE ")
						.append(" NAME = ").append("'").append(name).append("';");

                stmt.execute(insQuery.toString());

                ResultSet res = stmt.getResultSet();
		
				if(res.next() == true) {
					credit  = Integer.parseInt(res.getString("CREDIT"));
                }
				
                // Close connection
                stmt.close();
                con.close(); 
				
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return credit;
		
	}
	
	/**
	 * 
	 * Returns the table in which the given record exists
	 * @param name
	 * @return The table name of the given record
	 * @throws ClassNotFoundException 
	 */
	public static Table getTableName(String name) throws ClassNotFoundException {
		
		Table table_name = Table.NONE;
		
		try {
            try (Connection con = DB.getConnection();
                    Statement stmt = con.createStatement()) {

                StringBuilder insQuery;
				ResultSet res;
				table_name = Table.INDIVIDUAL;
				
				while(table_name != Table.NONE) {
					insQuery = new StringBuilder();
					insQuery.append("SELECT NAME FROM ")
						.append(table_name)
						.append(" WHERE ")
						.append("NAME = ").append("'").append(name).append("';");
					stmt.execute(insQuery.toString());
					res = stmt.getResultSet();
					if(res.next() == true) {
						break;
					}
					table_name = Table.values()[table_name.ordinal() + 1];
				}
				
                // Close connection
                stmt.close();
                con.close();
				
            }

        } catch (SQLException ex) {
			// Log exception
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		return table_name;
		
	}
	
	/**
	 * 
	 * Returns the parent table name of the given record
	 * @param name
	 * @return The parent table name of the given record
	 * @throws ClassNotFoundException 
	 */
	public static Table getOwnerTableName(String name) throws ClassNotFoundException {
		
		Table table_name = getTableName(name);
		
		if(table_name == Table.EMPLOYEE) {
			return Table.COMPANY;
		}else {
			return table_name;
		}
		
	}
	
	/**
	 * Return the name of the parent record 
	 * @param name
	 * @return The name of the parent record
	 * @throws ClassNotFoundException 
	 */
	public static String getOwnerAccountName(String name) throws ClassNotFoundException {
		
		if(getTableName(name) != Table.EMPLOYEE) {
			return name;
		}else {
			return getEmployee(name).getCompanyName();
		}
		
	}
	
}