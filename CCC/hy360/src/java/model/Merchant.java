package model;

/**
 *
 * @author skerdbash
 */
public class Merchant extends Account {
	
	private int commision;
	private int profit;
	
	public Merchant(String name, String account_id, int commision) {
		super(name, account_id, 0);
		this.commision = commision;
		profit = 0;
	}

	public Merchant(String name, String account_id, int commision, int profit, int debt) {
		super(name, account_id, debt);
		this.commision = commision;
		this.profit = profit;
	}

	public int getCommision() {
		return commision;
	}

	public void setCommision(int commision) {
		this.commision = commision;
	}

	public int getProfit() {
		return profit;
	}

	public void setProfit(int profit) {
		this.profit = profit;
	}
	
}
