package model;

import java.util.ArrayList;

/**
 *
 * @author skerdbash
 */
public class Company extends Account {
	
	private String expiration_date;
	private int credit_limit;
	private int credit;
	private ArrayList<Employee> employees;
	
	public Company(String name, String account_id, String expiration_date, int credit_limit, int credit) {
		super(name, account_id, 0);
		this.expiration_date = expiration_date;
		this.credit_limit = credit_limit;
		this.credit = credit;
	}

	public Company(String name, String account_id, String expiration_date, int credit_limit, int credit, int debt) {
		super(name, account_id, debt);
		this.expiration_date = expiration_date;
		this.credit_limit = credit_limit;
		this.credit = credit;
	}

	public Company(String name, String account_id, String expiration_date, int credit_limit, int credit, int debt, ArrayList<Employee> employees) {
		super(name, account_id, debt);
		this.expiration_date = expiration_date;
		this.credit_limit = credit_limit;
		this.credit = credit;
		this.employees = employees;
	}

	public String getExpirationDate() {
		return expiration_date;
	}

	public void setExpirationDate(String expiration_date) {
		this.expiration_date = expiration_date;
	}

	public int getCreditLimit() {
		return credit_limit;
	}

	public void setCreditLimit(int credit_limit) {
		this.credit_limit = credit_limit;
	}
	
	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	
}

