package model;

/**
 *
 * @author skerdbash
 */
public class Employee {
	
	private String name;
	private String id;
	private String company_name;

	public Employee(String name, String id, String company_name) {
		this.name = name;
		this.id = id;
		this.company_name = company_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCompanyName() {
		return company_name;
	}

	public void setCompanyName(String company_name) {
		this.company_name = company_name;
	}
	
}
