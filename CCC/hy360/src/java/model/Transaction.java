package model;

/**
 *
 * @author skerdbash
 */
public class Transaction {
	
	private String customer_name;
	private String merchant_name;
	private Payment type;
	private String date;
	private int amount;

	public Transaction(String customer_name, String merchant_name, String date, int amount, Payment type) {
		this.customer_name = customer_name;
		this.merchant_name = merchant_name;
		this.type = type;
		this.date = date;
		this.amount = amount;
	}

	public String getCustomerName() {
		return customer_name;
	}

	public void setCustomerName(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getMerchantName() {
		return merchant_name;
	}

	public void setMerchantName(String merchant_name) {
		this.merchant_name = merchant_name;
	}

	public Payment getType() {
		return type;
	}

	public void setType(Payment type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}
