package model;

/**
 *
 * @author skerdbash
 */
public class Account {
	
	private String name;
	private String account_id;
	private int debt;
	
	public Account(String name, String account_id, int debt) {
		this.name = name;
		this.account_id = account_id;
		this.debt = debt;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}	

	public String getAccountId() {
		return account_id;
	}

	public void setAccountId(String account_id) {
		this.account_id = account_id;
	}

	public int getDebt() {
		return debt;
	}

	public void setDebt(int debt) {
		this.debt = debt;
	}
	
}
