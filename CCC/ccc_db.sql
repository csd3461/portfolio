-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 19 Φεβ 2017 στις 19:27:46
-- Έκδοση διακομιστή: 10.1.9-MariaDB
-- Έκδοση PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `ccc_db`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `company`
--

CREATE TABLE `company` (
  `NAME` varchar(20) NOT NULL,
  `ACCOUNT_ID` varchar(20) NOT NULL,
  `EXPIRATION_DATE` varchar(20) NOT NULL,
  `CREDIT_LIMIT` int(11) NOT NULL,
  `CREDIT` int(11) NOT NULL,
  `DEBT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `employee`
--

CREATE TABLE `employee` (
  `NAME` varchar(26) NOT NULL,
  `ID` varchar(20) NOT NULL,
  `COMPANY_NAME` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `individual`
--

CREATE TABLE `individual` (
  `NAME` varchar(20) NOT NULL,
  `ACCOUNT_ID` varchar(20) NOT NULL,
  `EXPIRATION_DATE` varchar(20) NOT NULL,
  `CREDIT_LIMIT` int(11) NOT NULL,
  `CREDIT` int(11) NOT NULL,
  `DEBT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `merchant`
--

CREATE TABLE `merchant` (
  `NAME` varchar(20) NOT NULL,
  `ACCOUNT_ID` varchar(20) NOT NULL,
  `COMMISION` int(11) NOT NULL,
  `PROFIT` int(11) NOT NULL,
  `DEBT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `transaction`
--

CREATE TABLE `transaction` (
  `MERCHANT_NAME` varchar(20) NOT NULL,
  `CUSTOMER_NAME` varchar(26) NOT NULL,
  `DATE` varchar(20) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `TYPE` enum('CHARGE','CREDIT') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`NAME`),
  ADD UNIQUE KEY `NAME` (`NAME`),
  ADD UNIQUE KEY `ACCOUNT_ID` (`ACCOUNT_ID`);

--
-- Ευρετήρια για πίνακα `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`NAME`),
  ADD UNIQUE KEY `NAME` (`NAME`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Ευρετήρια για πίνακα `individual`
--
ALTER TABLE `individual`
  ADD PRIMARY KEY (`NAME`),
  ADD UNIQUE KEY `NAME` (`NAME`),
  ADD UNIQUE KEY `ACCOUNT_ID` (`ACCOUNT_ID`);

--
-- Ευρετήρια για πίνακα `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`NAME`),
  ADD UNIQUE KEY `NAME` (`NAME`),
  ADD UNIQUE KEY `ACCOUNT_ID` (`ACCOUNT_ID`);

--
-- Ευρετήρια για πίνακα `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`DATE`),
  ADD UNIQUE KEY `DATE` (`DATE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
