/*
	Full Name: Simidalas Nikolaos
	Assignment: 4st
	File Name: sudoku.h
*/

#ifndef __SUDOKU_H__
#include "grid.h"

void sudoku_Print(FILE *s, Grid_T g);
Grid_T sudoku_read(void);
int sudoku_is_correct(Grid_T g);
void sudoku_print_errors(Grid_T g);
int sudoku_solution_is_unique(Grid_T g);
Grid_T sudoku_solve(Grid_T g);
Grid_T sudoku_generate(int nelts);

#define __SUDOKU_H__
#endif
