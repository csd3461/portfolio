/*
	Full Name: Simidalas Nikolaos
	Assignment: 4st
	File Name: grid.h
*/
#ifndef __GRID_H__

typedef struct grid_s {
  struct { 
    int val;       /* value of element i,j */
  } elts[9][9];    /* sudoku grid elements; 0<=i,j<=9 */
} Grid_T;

int sudoku_has_empty_chells(Grid_T g);
int sudoku_has_double_symbols(Grid_T g);
void sudoku_print(Grid_T g);
void findChoices(Grid_T g, int x, int y, int *Array);
int sudoku_count_choices_cell(int *Array);
int sudoku_mark_unique_Choice(int *Array);
int sudoku_is_completed(Grid_T g);
Grid_T sudoku_fill_one_choice(Grid_T g);
Grid_T sudoku_generate_complete(void);
int sudoku_is_wrong(Grid_T g);

#define __GRID_H__
#endif