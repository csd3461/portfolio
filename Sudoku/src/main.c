/*
	Full Name: Simidalas Nikolaos
	Assignment: 4st
	File Name: main.c
*/

#include <stdio.h>
#include "sudoku.h"
#include <stdlib.h>

/*  It's the main function.
	It provide(with the suitable arguments) us new 
	sudoku Puzzles with as empty cell as we want, 
	solves the puzzle which we want or produces 
	new Puzzles and solves them itself
*/
int main(int argc, char *argv[]) {
	int i;
	Grid_T g;
	int nelts;
	i = 0;

	if(argc == 1) {
		g = sudoku_read();
		printf("New Puzzle\n");
		sudoku_print(g);

		if(sudoku_is_wrong(g) == 1) {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
				return 0;
		} else if(sudoku_solution_is_unique(g) == 1) {
			printf("Puzzle has a (unique choice) solution:\n");
		} else {
			printf("A possible solution:\n");
		}
		g = sudoku_solve(g);
		sudoku_print(g);

	} else if(argc == 2) {
		if(argv[1][0] == '-' && argv[1][1] == 'g') {
			printf("There is no input of nelts\n"); 
		} else if(argv[1][0] == '-' && argv[1][1] == 'c') {
			printf("New puzzle:\n");
			g = sudoku_read();
			sudoku_print(g);
			if(sudoku_has_empty_chells(g) == 1) {
				printf("The Puzzle is uncompleted\n");
				return 0;
			}
			if(sudoku_is_correct(g) == 1) {
				printf("The Puzzle is correct\n");
			} else {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
			}
		}

	} else if(argc == 3) {
		if(argv[1][0] == '-' && argv[1][1] == 'g') {
			nelts = atoi(argv[2]); 
			if(nelts <= 81 && nelts >= 0) {
				g = sudoku_generate(nelts);
				printf("New puzzle:\n");
				sudoku_print(g);
			} else {
				printf("Invalide input of nelts. Must be between 0 and 81\n");
			}
		}
		
	} else if(argc == 4) {
		if(argv[1][0] == '-' && argv[1][1] == 'c' && argv[2][0] == '-' && argv[2][1] == 'g' && argv) i = 3;
		if(argv[3][0] == '-' && argv[3][1] == 'c' && argv[1][0] == '-' && argv[1][1] == 'g' && argv) i = 2;
		if(i != 0) {
			nelts = atoi(argv[i]); 
			if(nelts <= 81 && nelts >= 0) {
				printf("New puzzle:\n");
				g = sudoku_generate(nelts);
			} else {
				printf("Invalide input of nelts. Must be between 0 and 81\n");
				return 0;
			}
			sudoku_print(g);
			if(sudoku_is_wrong(g) == 1) {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
				return 0;
			} else if(sudoku_solution_is_unique(g) == 1) {
				printf("Puzzle has a (unique choice) solution:\n");
			} else {
				printf("A possible solution:\n");
			}
			g = sudoku_solve(g);
			sudoku_print(g);
		} 
	}
	return 1;
}