/*
	Full Name: Simidalas Nikolaos
	Assignment: 4st
	File Name: grid.c
*/

#include <stdio.h>
#include <assert.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>

#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KNRM  "\x1B[0m"

typedef struct grid_s {
  struct { 
    int val;       /* value of element i,j */
  } elts[9][9];    /* sudoku grid elements; 0<=i,j<=9 */
} Grid_T;

/*  Check if there are empty cells.
	Return: 1 if exist
			o if don't exist
*/
int sudoku_has_empty_chells(Grid_T g) {
	int i, j;
	 for( i = 0; i < 9; i++) {
	    for( j = 0; j < 9; j++) {
	    	if(g.elts[i][j].val == 0) return 1;
	    }
	}
	return 0;
}

/*  Check if there are double symbols into the same column, row or block.
	Return: 1 if exist
			o if don't exist
*/
int sudoku_has_double_symbols(Grid_T g) {
	int i, j, k;
	int m, l, n;

	/* Here we find what row have wrong. */
	for ( k = 0; k < 9; k++){
	    for( i = 0; i < 9; i++) {
		    for( j = i + 1; j < 9; j++) {
			    if((g.elts[k][i].val == g.elts[k][j].val && g.elts[k][j].val != 0)) {
					return 1;
			    }
		    }
	    }
    }

	/* Here we find what column have wrong. */
	for ( k = 0; k < 9; k++){
	    for( i = 0; i < 9; i++) {
		    for( j = i + 1; j < 9; j++) {
			    if((g.elts[i][k].val == g.elts[j][k].val && g.elts[j][k].val != 0)) {
				    return 1;
			    }
		    }
	    }
    }

	/* 
	Here we find what row have wrong.
	The first two iterations changed blocks.
	The next two are used to choose an element of the block.
	And the other two for to we compare each element with each element of the block .
	*/
     for( m = 0; m < 9; m = m + 3) {
		 for( n = 0; n < 9; n = n + 3) {
			 
             for( k = m; k < m + 3; k++) {
		         for( l = n; l < n + 3; l++) {
					 
			         for( i = m; i < m + 3; i++) {
				         for( j = n; j < n + 3; j++) {
							if((g.elts[k][l].val == g.elts[i][j].val && g.elts[i][j].val != 0 && (k != i || l != j))){
						       return 1;
						    }
					     }
				     }
			     }
		     }
	      }
     }
     return 0;
}


/*  Print the sudoku Pazzle*/
void sudoku_print(Grid_T g) {
	int i, j;

	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
	 		printf ("%s%d %s", KGRN, g.elts[i][j].val, KNRM);
	 	}
	 	printf("\n");
	}
}

/*  Take as parameters an array-pointer to integer, 
	the grid of the sudoku and the coordinates of 
	one (empty) cell check if there are possible choice. 
	If there are, then write the number one(1) on the
	suitable position of the array.
*/
void findChoices(Grid_T g, int x, int y, int *Array) {
	int i, j;

	for( i = 0; i < 9; i++) {
		Array[i] = 1;
	}

	for( i = x; i > -1; i--) {
		if(g.elts[i][y].val != 0) Array[g.elts[i][y].val - 1] = 0;
	}
	for( i = x; i < 9; i++) {
		if(g.elts[i][y].val != 0) Array[g.elts[i][y].val - 1] = 0;
	}

	for( i = y; i > -1; i--) {
		if(g.elts[x][i].val != 0) Array[g.elts[x][i].val - 1] = 0;

	}
	for( i = y; i < 9; i++) {
		if(g.elts[x][i].val != 0) Array[g.elts[x][i].val - 1] = 0;
	}

	if(x % 3 == 0) {
		if(y % 3 == 0) {
			for( i = x; i < x + 3; i++) {
				for ( j = y; j < y + 3; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 1) % 3 == 0) {
			for( i = x; i < x + 3; i++) {
				for ( j = y - 1; j < y + 2; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 2) % 3 == 0) {
			for( i = x; i < x + 3; i++) {
				for ( j = y - 2; j < y + 1; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		}
	} else if((x - 1) % 3 == 0) {
		if(y % 3 == 0) {
			for( i = x - 1; i < x + 2; i++) {
				for ( j = y; j < y + 3; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 1) % 3 == 0) {
			for( i = x - 1; i < x + 2; i++) {
				for ( j = y - 1; j < y + 2; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 2) % 3 == 0) {
			for( i = x - 1; i < x + 2; i++) {
				for ( j = y - 2; j < y + 1; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		}
	} else if((x - 2) % 3 == 0) {
		if(y % 3 == 0) {
			for( i = x - 2; i < x + 1; i++) {
				for ( j = y; j < y + 3; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 1) % 3 == 0) {
			for( i = x - 2; i < x + 1; i++) {
				for ( j = y - 1; j < y + 2; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		} else if((y - 2) % 3 == 0) {
			for( i = x - 2; i < x + 1; i++) {
				for ( j = y - 2; j < y + 1; j++) {
					if(g.elts[i][j].val != 0) Array[g.elts[i][j].val - 1] = 0;
				}
			}
		}
	}

}


/*  Count how many possible numbers suit on a cell.
	Return: The host of them.
*/
int sudoku_count_choices_cell(int *Array) {
	int count = 0, i;
	for( i = 0; i < 9; i++) {
		if(Array[i] == 1) count++;
	}
	return count;
}

/*  Check if there is unique choice
	Return: If exists, then return the number of 
		the unique choice or zero if it doesn't exist.
*/
int sudoku_mark_unique_Choice(int *Array) {
	int i, flag = 0;
	for( i = 0; i < 9; i++) {
		if(Array[i] != 0 && flag == 0) {
			flag = i + 1;
		} else if(Array[i] != 0 && flag != 0) return 0;
	}
	return flag;
}

/*  Check if the sudoku is completed (not necessary 
    correct, just if the cell are not empty).
	Return: 1 if it is completed
			O otherwise(if exists at least one empty cell)
*/
int sudoku_is_completed(Grid_T g) {
	int i, j;
	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
			if(g.elts[i][j].val == 0) return 0;
		}
	}
	return 1;
}

/*  Fill in the sudoku grid the unique value all of the possible cells which have only one.
	Return: The refreshed grid
*/
Grid_T sudoku_fill_one_choice(Grid_T g) {
	int i, j, marked;
	int flag = 0;
	int *Array = (int *)malloc(9 * sizeof(int *));

loopwhile:	
	while(flag == 0) {
		flag = 1;
		for( i = 0; i < 9; i++) {
			for ( j = 0; j < 9; j++) {
				if(g.elts[i][j].val == 0) { 
					findChoices(g, i, j, Array);
					marked = sudoku_mark_unique_Choice(Array);
					if(marked != 0) {
						g.elts[i][j].val = marked;
						flag = 0;
						goto loopwhile;
					}
				}
			}
		}
	}
	return g;
}

/*  Produce a new full completed sudoku Puzzle
	Return: The new sudoku Puzzle
*/
Grid_T sudoku_generate_complete(void) {
	int i, j, v;
	Grid_T g;
	int *Array = (int *)malloc(9 * sizeof(int *));
	srand(time(NULL));

	do {
		for( i = 0; i < 9; i++) {
			for ( j = 0; j < 9; j++) {
				g.elts[i][j].val = 0;
			}
		}

		for( i = 0; i < 9; i++) {
			for ( j = 0; j < 9; j++) {
				findChoices(g, i, j, Array);
				if(sudoku_count_choices_cell(Array) != 0) {
					do {
						v = rand() % 9;
					} while(Array[v] == 0);
					g.elts[i][j].val = v + 1;
				}
			}
		}
	} while(sudoku_is_correct(g) == 0);
	return g;
}

/*  Check if the sudoku is wrong.
	That caused when there double numbers 
	into the same column, row or block or
	when there is an empty cell which has
	no possible choices.
	Return: 1 if it's wrong
			0 otherwise
*/
int sudoku_is_wrong(Grid_T g) {
	int i, j;
	int *Array = (int *)malloc(9 * sizeof(int *));
	if(sudoku_has_double_symbols(g) == 1) return 1;
	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
			if(g.elts[i][j].val == 0) {
				findChoices(g, i, j, Array);
				if(sudoku_count_choices_cell(Array) == 0) return 1;
			}
		}
	}
	return 0;
}