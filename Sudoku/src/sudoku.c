/*
	Full Name: Simidalas Nikolaos
	Assignment: 4st
	File Name: sudoku.c
*/

#include <stdio.h>
#include <assert.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include "grid.h"

#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KNRM  "\x1B[0m"


void sudoku_Print(FILE *s, Grid_T g) {
	int i, j;
	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
			fprintf (s, "%d ", g.elts[i][j].val);
	 	}
	 	printf("\n");
	}
}

/*  Read a new sudoku grid */
Grid_T sudoku_read(void) {
	Grid_T g;
	int i, j;
	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
			scanf("%d", &g.elts[i][j].val);
		}
	}
	return g;
}

/*  Check if the sudoku is correct completed.
	Return: 1 if exist
			o if don't exist
*/
int sudoku_is_correct(Grid_T g) {
	if(sudoku_has_empty_chells(g) == 1 || sudoku_has_double_symbols(g) == 1) return 0;
	return 1;
}

/*  Check if there are error in the grid of the sudoku.
	Print the number of the column, row or block where 
	there is at least on error.
*/
void sudoku_print_errors(Grid_T g) {
	int i, j, column = 0, row = 0, k, C[9] = { 0 }, R[9] = { 0 };
	int m, l, n, t = 0, block = 0, B[9] = { 0 };

	/* Here we find what row have wrong. */
	for ( k = 0; k < 9; k++){
	    for( i = 0; i < 9; i++) {
		    for( j = i + 1; j < 9; j++) {
			    if((g.elts[k][i].val == g.elts[k][j].val && g.elts[k][j].val != 0)) {		    
			    	R[k] = 1;
				    row = 1;
				    break;
			    }
		    }
	    }
    }

	/* Here we find what column have wrong. */
	for ( k = 0; k < 9; k++){
	    for( i = 0; i < 9; i++) {
		    for( j = i + 1; j < 9; j++) {
			   if((g.elts[i][k].val == g.elts[j][k].val && g.elts[j][k].val != 0)) {			    
			   		C[k] = 1;
				    column = 1;
				    break;
			    }
		    }
	    }
    }

	/* 
	Here we find what row have wrong.
	The first two iterations changed blocks.
	The next two are used to choose an element of the block.
	And the other two for to we compare each element with each element of the block .
	*/
     for( m = 0; m < 9; m = m + 3) {
		 for( n = 0; n < 9; n = n + 3) {
			 
             for( k = m; k < m + 3; k++) {
		         for( l = n; l < n + 3; l++) {
					 
			         for( i = m; i < m + 3; i++) {
				         for( j = n; j < n + 3; j++) {
							 
			                if((g.elts[k][l].val == g.elts[i][j].val && g.elts[i][j].val != 0 && (k != i || l != j))){
								B[t] = t + 1;
				                block = 1;
				                break;
						    }
					     }
				     }
			     }
		     }
		     t++; 
	      }
     }

     /* It's the time to print all the errors. */
     if( row == 1) {
        printf("%sInvalid rows:     %s", KRED, KNRM);
        for( i = 0; i < 9; i++) {
            if( R[i] == 1) printf( "%d ", i + 1);
        }
        printf("\n");
     } 

     if( column == 1) {
        printf("%sInvalid columns:  %s", KRED, KNRM);
        for( i = 0; i < 9; i++) {
            if( C[i] == 1) printf( "%d ", i + 1);
        }
        printf("\n");
     }

     if( block == 1) {
        printf("%sInvalid blocks:   %s", KRED, KNRM);
        for( i = 0; i < 9; i++) {
            if( B[i] != 0) printf( "%d ", B[i]);
        }
    }
}

/*  Check if the sudoku has one unique solution.
	Return: 1 if exist only one
			0 if there more than one solution
*/
int sudoku_solution_is_unique(Grid_T g) {
	g = sudoku_fill_one_choice(g);
	return sudoku_is_correct(g);
}

/*  Solve the sudoku Puzzle.
	Return: The solved Puzzle
*/
Grid_T sudoku_solve(Grid_T g) {
	int i, j;
	int *Array = (int *)malloc(9 * sizeof(int *));
	Grid_T Case[9];
	int k;
	
	g = sudoku_fill_one_choice(g);

	if(sudoku_is_completed(g) == 1) {
		return g;
	}
	
	for( i = 0; i < 9; i++) {
		for ( j = 0; j < 9; j++) {
			if(g.elts[i][j].val == 0) {
				findChoices(g, i, j, Array);
				if(sudoku_count_choices_cell(Array) != 0) {
					for ( k = 0; k < 9; k++) { 
						if(Array[k] == 1) {
							g.elts[i][j].val = k + 1;
							Case[k] = sudoku_solve(g);
							if(sudoku_is_correct(Case[i]) == 1) return Case[i];
							g.elts[i][j].val = 0;
						} else {
							Case[k] = g;
						}
					}
				}
				goto exit;
			}
		}
	}
exit:
	for( i = 0; i < 9; i++) {
		if(sudoku_is_correct(Case[i]) == 1) {
			return Case[i];
		}
	}
	return g;
}

/*  Produce a new sudoku puzzle with 81 - nelts cells completed.
	Return: The uncompleted sudoku Puzzle
*/
Grid_T sudoku_generate(int nelts) {
	Grid_T g;
	int i, j;
	srand(time(NULL));
	nelts = 81 - nelts;
	g = sudoku_generate_complete();

	while(nelts > 0) {
		i = rand() % 9;
		j = rand() % 9;
		if(g.elts[i][j].val != 0) {
			g.elts[i][j].val = 0;
			nelts--;
		}
	}
	return g;
}

/*  It's the main function.
	It provide(with the suitable arguments) us new 
	sudoku Puzzles with as empty cell as we want, 
	solves the puzzle which we want or produces 
	new Puzzles and solves them itself
*/
int main(int argc, char *argv[]) {
	int i;
	Grid_T g;
	int nelts;
	i = 0;

	if(argc == 1) {
		g = sudoku_read();
		printf("New Puzzle\n");
		sudoku_print(g);

		if(sudoku_is_wrong(g) == 1) {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
				return 0;
		} else if(sudoku_solution_is_unique(g) == 1) {
			printf("Puzzle has a (unique choice) solution:\n");
		} else {
			printf("A possible solution:\n");
		}
		g = sudoku_solve(g);
		sudoku_print(g);

	} else if(argc == 2) {
		if(argv[1][0] == '-' && argv[1][1] == 'g') {
			printf("There is no input of nelts\n"); 
		} else if(argv[1][0] == '-' && argv[1][1] == 'c') {
			printf("New puzzle:\n");
			g = sudoku_read();
			sudoku_print(g);
			if(sudoku_has_empty_chells(g) == 1) {
				printf("The Puzzle is uncompleted\n");
				return 0;
			}
			if(sudoku_is_correct(g) == 1) {
				printf("The Puzzle is correct\n");
			} else {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
			}
		}

	} else if(argc == 3) {
		if(argv[1][0] == '-' && argv[1][1] == 'g') {
			nelts = atoi(argv[2]); 
			if(nelts <= 81 && nelts >= 0) {
				g = sudoku_generate(nelts);
				printf("New puzzle:\n");
				sudoku_print(g);
			} else {
				printf("Invalide input of nelts. Must be between 0 and 81\n");
			}
		}
		
	} else if(argc == 4) {
		if(argv[1][0] == '-' && argv[1][1] == 'c' && argv[2][0] == '-' && argv[2][1] == 'g' && argv) i = 3;
		if(argv[3][0] == '-' && argv[3][1] == 'c' && argv[1][0] == '-' && argv[1][1] == 'g' && argv) i = 2;
		if(i != 0) {
			nelts = atoi(argv[i]); 
			if(nelts <= 81 && nelts >= 0) {
				printf("New puzzle:\n");
				g = sudoku_generate(nelts);
			} else {
				printf("Invalide input of nelts. Must be between 0 and 81\n");
				return 0;
			}
			sudoku_print(g);
			if(sudoku_is_wrong(g) == 1) {
				printf("The Puzzle is wrong\n");
				sudoku_print_errors(g);
				return 0;
			} else if(sudoku_solution_is_unique(g) == 1) {
				printf("Puzzle has a (unique choice) solution:\n");
			} else {
				printf("A possible solution:\n");
			}
			g = sudoku_solve(g);
			sudoku_print(g);
		} 
	}
	return 1;
}